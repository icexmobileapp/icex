import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Dimensions, UIManager, LayoutAnimation, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Dropdown } from 'react-native-material-dropdown';
import { TabView, SceneMap } from 'react-native-tab-view';
import Header from './components/Header';
import AppSettings from "./subViews/AppSettings";
import AccountSettings from "./subViews/AccountSettings";

const DeviceWidth = Dimensions.get('window').width;
const ViewWidth = DeviceWidth - scale(20) - scale(20);

export default class Settings extends Component {

    state = {
        RefreshTime: '5 SEC',
        index: 0,
        routes: [
            { key: 'app', title: 'APP', navigation: this.props.navigation },
            { key: 'account', title: 'ACCOUNT', navigation: this.props.navigation },
        ],
    }

    componentWillUpdate() {
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        LayoutAnimation.spring();
    }

    _handleIndexChange = index => this.setState({ index });

    _renderTabBar = props => {
        // const inputRange = props.navigationState.routes.map((x, i) => i);
        const selectedTabAlign = this.state.index == 0 ? 'flex-start' : 'flex-end';

        // console.log(props);
        // console.log(inputRange)

        return (
            <View style={{ paddingTop: 20, flexDirection: 'column' }}>
                <View style={styles.tabBar}>
                    {props.navigationState.routes.map((route, i) => {
                        const color = this.state.index == i ? '#ffffff' : '#c9c9c9';
                        const align = i == 0 ? 'flex-start' : 'flex-end';

                        return (
                            <TouchableOpacity
                                key={route.key}
                                style={styles.tabItem}
                                onPress={() => this.setState({ index: i })}>
                                <Text style={{ color, fontSize: moderateScale(17), alignSelf: align, paddingLeft: scale(20), paddingRight: scale(20) }}>{route.title}</Text>
                                {/* <View style={{height:4,alignSelf:align ,width:ViewWidth/3, backgroundColor:'#ffffff'}}></View> */}
                            </TouchableOpacity>

                        );
                    })}
                </View>
                <View style={{ height: 3, alignSelf: selectedTabAlign, marginLeft: scale(20), marginRight: scale(20), width: ViewWidth / 3, backgroundColor: '#ffffff' }}></View>
                <View style={{ height: 2, marginLeft: scale(20), width: ViewWidth, backgroundColor: '#ffffff' }}></View>
            </View>
        );
    };

    _renderScene = SceneMap({
        app: AppSettings,
        account: AccountSettings,
    });

    render() {
        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation} />
                <LinearGradient
                    colors={['#303132', '#171717']}
                    // locations={[0,0.5,0.9]}
                    start={{ x: 0.5, y: 0.5 }} end={{ x: 1.0, y: 1.0 }}
                    style={{ flex: 1 }}
                >

                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >

                        <View style={{ marginLeft: moderateScale(20), paddingTop: moderateScale(20), paddingBottom: moderateScale(10) }}>
                            <Text style={{ fontSize: moderateScale(26), color: 'white' }}>Settings</Text>
                        </View>

                        <TabView
                            navigationState={this.state}
                            renderScene={this._renderScene}
                            renderTabBar={this._renderTabBar}
                            onIndexChange={this._handleIndexChange}
                            swipeEnabled={false}
                        />

                    </ScrollView>

                </LinearGradient>

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
        flexDirection: 'column'
    },
});
