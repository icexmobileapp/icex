import React, { Component } from 'react';
import { RefreshControl, FlatList, Text, View, TouchableOpacity, UIManager, Dimensions, Image, LayoutAnimation, TextInput } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import Overlay from 'react-native-modal-overlay';
import { saveComment, getComment } from '../actions';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Icon, Spinner } from 'native-base';
import FAIcon from "react-native-vector-icons/FontAwesome";
import SLIcon from "react-native-vector-icons/SimpleLineIcons";
import Hyperlink from 'react-native-hyperlink';
import ImagePicker from 'react-native-image-picker';
import common from '../common';
import Loader from "../components/Loader";

const linkify = require('linkify-it')();

const { height, width } = Dimensions.get('window');
class Blog extends Component {

    constructor(props) {
        super(props);
        this.state =
            {
                modalVisible: false,
                text: '',
                showCommentArea: false,
                commentsArray: [],
                fetchCommentsErr: '',
                commentErr: '',
                saveCommentsErr: '',
                commentId: '',
                loading: false,
                isRefreshing: false,
                showCommLoader: false,
                showReportLoader: false,
                commentReportMsg: '',
                needToLoadComments: false,
                canLoadMoreComments: true,
                pageSize: 10,
                pageNumber: 1,
                uploadModalVisible: false,
                attachmentBase64: '',
                attachmentName: '',
            };
    }

    onClose() {
        this.setState({
            commentReportMsg: '',
            modalVisible: false
        });
    }

    resetState() {
        this.setState({
            text: '',
            showCommentArea: false,
            commentsArray: [],
            fetchCommentsErr: '',
            saveCommentsErr: '',
            commentId: '',
            commentErr: '',
            loading: false,
            isRefreshing: false,
            showCommLoader: false,
            showReportLoader: false,
            commentReportMsg: '',
            needToLoadComments: false,
            canLoadMoreComments: true,
            pageSize: 10,
            pageNumber: 1
        });
    }

    componentWillReceiveProps(nextProps) {
        if ((this.props.selectedCommodity != nextProps.selectedCommodity) && this.props.selectedTab == 'BLOG') {
            this.resetState();
            setTimeout(() => {
                this.loadComments(false);
            }, 5);
        } else if (nextProps.selectedTab == 'BLOG' && this.state.needToLoadComments) {
            this.resetState();
            this.loadComments(false);
        } else if (nextProps.isCalled && !this.props.isCalled) {
            this.resetState();
            this.loadComments(false);
        }
        else if ((this.props.selectedCommodity != nextProps.selectedCommodity) && this.props.selectedTab != 'BLOG') {
            this.setState({
                needToLoadComments: true
            });
        }
    }

    // componentWillMount() {

    //     const req = new GetComment();
    //     // req.Commodity = this.state.Commodity;
    //     req.Commodity = this.props.selectedCommodity;
    //     console.log("AllComments", req);
    //     var serviceRequest = common.encryptData(JSON.stringify(req))

    //     var serviceBody = { request: serviceRequest };
    //     console.log('serviceBody', serviceBody);
    //     this.props.getComment(serviceBody);

    // }

    loadNextPage() {
        if (this.state.canLoadMoreComments && !this.state.loading) {
            this.setState({
                pageNumber: Number(this.state.pageNumber) + 1,
                loading: true
            });
            setTimeout(() => {
                this.loadComments(false);
            }, 5);

        }
    }

    submitClicked() {
        this.setState({
            commentErr: '',
        })
        if (this.state.text.trim() != '') {
            const req = new SubmitComment();
            req.UserId = this.props.user.Id;
            req.Commodity = this.props.selectedCommodity;
            req.Comment = this.state.text;
            req.FileName = this.state.attachmentName;
            req.Attachments = this.state.attachmentBase64;
            console.log("addCommentClicked", req);
            var serviceRequest = common.encryptData(JSON.stringify(req))

            var serviceBody = { request: serviceRequest };
            // console.log('serviceBody', serviceBody);
            this.setState({
                showCommLoader: true
            });


            common.callService('SaveBlogComments_V2', serviceBody)
                .then(SaveBlogComments => {
                    console.log(SaveBlogComments);

                    if (SaveBlogComments.ReturnCode == '1') {
                        var updatedCommentsArray = this.state.commentsArray;
                        let savedCommentData = SaveBlogComments.Data;
                        savedCommentData.ProfileImage = this.props.user.ProfileImage;
                        savedCommentData.Name = this.props.user.Name;
                        console.log(updatedCommentsArray.length % 10);
                        if (updatedCommentsArray.length % 10 == 0) {
                            updatedCommentsArray.splice(updatedCommentsArray.length, 1);
                        }
                        updatedCommentsArray.splice(0, 0, savedCommentData);
                        this.setState({
                            commentsArray: [],
                        });
                        console.log(updatedCommentsArray);
                        this.setState({
                            commentsArray: [...updatedCommentsArray],
                            showCommLoader: false,
                            text: '',
                            showCommentArea: false,
                        });
                        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
                        LayoutAnimation.easeInEaseOut();
                    } else {
                        this.setState({
                            saveCommentsErr: SaveBlogComments.ReturnMsg,
                            showCommLoader: false
                        });
                    }
                })
                .catch(err => {
                    console.log(err);
                    this.setState({
                        saveCommentsErr: err,
                        showCommLoader: false
                    });
                })
            // this.props.saveComment(serviceBody);
            // this.setState({
            //     text: '',
            // })
        } else {
            this.setState({
                commentErr: 'Please add comment'
            });
        }
    }

    ReportClicked() {
        const req = new ReportComment();
        req.Id = this.state.commentId;
        req.AbuseReporter = this.props.user.Id;
        console.log("ReportCommentClicked", req);
        var serviceRequest = common.encryptData(JSON.stringify(req))

        var serviceBody = { request: serviceRequest };
        console.log('serviceBody', serviceBody);
        this.setState({
            showReportLoader: true
        });

        common.callService('FlagCommentAbuse', serviceBody)
            .then(FlagCommentAbuse => {
                this.setState({
                    showReportLoader: false,
                });
                console.log("FlagCommentAbuse1", FlagCommentAbuse);
                if (FlagCommentAbuse.ReturnCode == '1') {
                    let tempCommArr = this.state.commentsArray;
                    let commIndex = tempCommArr.findIndex(comm =>
                        String(comm.Id) == String(this.state.commentId))
                    tempCommArr[commIndex].IsAbuse = true;
                    this.setState({
                        commentsArray: tempCommArr,
                        commentReportMsg: 'Reported as Abuse for approval'
                    });
                } else {
                    this.setState({
                        commentReportMsg: 'Unable to Report '
                    });
                    console.log(FlagCommentAbuse.ReturnMsg)
                }
                setTimeout(() => {
                    this.onClose();
                }, 2000);
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    showReportLoader: false,
                    commentReportMsg: 'Unable to Report '
                });
                setTimeout(() => {
                    this.onClose();
                }, 2000);
            })
    }

    getReportLoader() {

        if (this.state.showReportLoader) {
            return (
                <View style={{ alignItems: 'center', height: moderateScale(100), margin: 0, width: width * 0.8 }}>
                    <Spinner color='#97f8cc'
                        size={moderateScale(40)}
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    />
                    <Text style={{ fontSize: moderateScale(16), color: 'black', alignContent: 'flex-start' }}>Reporting Comment</Text>
                </View>
            );
        } else if (this.state.commentReportMsg == '') {
            return (
                <View style={{ alignItems: 'center', height: moderateScale(100), margin: 0, width: width * 0.8 }}>

                    <View>
                        <Text style={{ fontSize: moderateScale(15), color: 'black', alignContent: 'flex-start' }}>Report Comment</Text>
                    </View>

                    <View style={{ marginTop: moderateScale(15) }}>
                        <Text
                            style={{ fontSize: moderateScale(13), color: 'black', alignContent: 'flex-start' }}
                        > Do you want to report this comment as Abuse</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: moderateScale(15), marginTop: moderateScale(15) }}>

                        <TouchableOpacity
                            style={[styles.buttonStyle, { borderWidth: 1, borderColor: '#4a4eda', borderRadius: moderateScale(5), marginLeft: moderateScale(15) }]}
                            onPress={() => this.ReportClicked()}>
                            <Text style={{ fontSize: moderateScale(14), color: 'black' }}>Yes</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[styles.buttonStyle, { borderWidth: 1, borderColor: '#4a4eda', borderRadius: moderateScale(5), marginLeft: moderateScale(15) }]}
                            onPress={() => {
                                this.setState({ modalVisible: false });
                            }}
                        >
                            <Text style={{ fontSize: moderateScale(14), color: 'black' }}>No</Text>
                        </TouchableOpacity>

                    </View>

                </View>
                // <View style={{ width: width * 0.6, height: moderateScale(25) }}>
                //     <TouchableOpacity
                //         style={{ width: width * 0.6, height: moderateScale(25) }}
                //         onPress={() => this.ReportClicked()}>
                //         <Text style={{ fontSize: moderateScale(14) }}>Report listing as abuse</Text>
                //     </TouchableOpacity>
                // </View>
            )
        } else {
            return (
                <View style={{ alignItems: 'center', height: moderateScale(100), margin: 0, width: width * 0.8, justifyContent: 'center' }}>
                    <FAIcon name='warning'
                        size={moderateScale(20)}
                        style={{ alignSelf: 'center', color: 'black', marginRight: moderateScale(5) }}
                    />
                    <Text style={{ fontSize: moderateScale(16), color: 'black', alignContent: 'center' }}>{this.state.commentReportMsg}</Text>
                </View>
            );
        }
    }

    getCommentLoader() {
        if (this.state.showCommLoader) {
            return (
                <View
                    style={styles.buttonStyle}>
                    <Spinner color='#97f8cc'
                        size={moderateScale(14)} />
                </View>
            );
        }
        return (
            <TouchableOpacity
                style={styles.buttonStyle}
                onPress={() => this.submitClicked()}
            >
                <Text style={styles.textStyle}>
                    SUBMIT
                                </Text>
            </TouchableOpacity>
        );
    }

    takeUploadAction(action) {
        const options = {
            quality: 0.5,
            // maxWidth: 500,
            // maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        let date = new Date();
        let fileName = `${this.props.user.Id}_${date.getDate()}-${date.getMonth()}-${date.getFullYear()}.${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
        if (action == 0) {
            ImagePicker.launchCamera(options, (response) => {
                console.log('Response = ', response);
                if (response.didCancel) {
                    console.log('User cancelled photo picker');
                }
                else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                }
                else {
                    this.setState({
                        attachmentBase64: 'data:image/jpeg;base64,' + response.data,
                        attachmentName: `${fileName}-${response.fileName}`,
                        uploadModalVisible: false,
                    });
                    // alert('data:image/jpeg;base64,' + response.data);
                    // alert(`${fileName}.${response.type}`);
                }
            });
        } else if (action == 1) {
            ImagePicker.launchImageLibrary(options, (response) => {
                console.log('Response = ', response);
                if (response.didCancel) {
                    console.log('User cancelled photo picker');
                }
                else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                }
                else {
                    this.setState({
                        attachmentBase64: 'data:image/jpeg;base64,' + response.data,
                        attachmentName: `${fileName}-${response.fileName}`,
                        uploadModalVisible: false,
                    });
                    // alert('data:image/jpeg;base64,' + response.data);
                    // alert(`${fileName}.${response.type}`);
                }
            });
        }
    }

    getUploadFileModal() {
        return (
            <View>
                <Text style={styles.uploadModalText}>Supported Formats: png,jpeg,pdf upto 2 MB</Text>
                <View style={styles.uploadModalButtonsView}>
                    <TouchableOpacity
                        style={styles.uploadItemInnerView}
                        onPress={() => { this.takeUploadAction(0) }}
                    >
                        <Icon name='camera'
                            type={'SimpleLineIcons'}
                            style={styles.uploadModalIcons}
                        />
                        <Text style={styles.uploadModalText}>Click {'\n'}Photo</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.uploadItemInnerView}
                        onPress={() => { this.takeUploadAction(1) }}
                    >
                        <Icon name='image-filter'
                            type={'MaterialCommunityIcons'}
                            style={styles.uploadModalIcons}
                        />
                        <Text style={styles.uploadModalText}>Upload from{'\n'}Gallery</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.uploadItemInnerView, { borderRightWidth: 0 }]}>
                        <Icon name='doc'
                            type={'SimpleLineIcons'}
                            style={styles.uploadModalIcons}
                        />
                        <Text style={styles.uploadModalText}>Upload{'\n'}Documents</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    getWriteComment() {
        if (this.state.showCommentArea) {
            return (
                <>
                    <View>
                        <View style={styles.commentView}>
                            <TextInput
                                multiline={true}
                                style={{ height: verticalScale(89), marginLeft: moderateScale(10), color: '#c2c2c2', fontSize: moderateScale(12) }}
                                placeholder="Write Comment"
                                value={this.state.text}
                                placeholderTextColor='#6d6d6d'
                                textAlignVertical='top'
                                onKeyPress={(text) => { this.setState({ commentErr: '' }) }}
                                onChangeText={(text) => this.setState({ text })}
                            />

                            <View style={{ height: moderateScale(35), flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                <TouchableOpacity
                                    style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', marginRight: moderateScale(10) }}
                                    onPress={() => { this.setState({ uploadModalVisible: true }) }}
                                >
                                    <Icon name='sun' //name is written as sun because icon is not matching with exact name
                                        type={'Feather'}
                                        style={{ color: 'white', alignSelf: 'flex-end', fontSize: moderateScale(16), marginRight: moderateScale(5) }}
                                    />
                                    <Text style={{ color: 'white', alignSelf: 'flex-end' }}>Upload File</Text>
                                </TouchableOpacity>
                                <View style={{
                                    height: moderateScale(35), width: moderateScale(95), marginTop: moderateScale(4)
                                }}>
                                    <LinearGradient colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
                                        start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
                                        style={{ borderTopLeftRadius: moderateScale(5), borderBottomRightRadius: moderateScale(4), justifyContent: 'center', alignItems: 'center' }}
                                    >
                                        {this.getCommentLoader()}
                                    </LinearGradient>
                                </View>
                            </View>
                        </View>
                        {
                            !this.state.commentErr == '' ?
                                <Text style={styles.errorMsg}>{this.state.commentErr}</Text> : null
                        }
                    </View>

                    <Overlay
                        visible={this.state.uploadModalVisible}
                        onClose={() => { this.setState({ uploadModalVisible: false }) }}
                        closeOnTouchOutside={true}
                        containerStyle={{
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            justifyContent: 'center', margin: 0, padding: 0
                        }}
                        childrenWrapperStyle={{
                            alignSelf: 'center',
                            backgroundColor: '#ffff',
                            borderRadius: moderateScale(5), borderWidth: 1,
                        }}
                    >
                        {this.getUploadFileModal()}

                    </Overlay>
                </>
            );

        }
        else {
            return null
        }

    }

    addCommentClicked() {
        this.setState({
            showCommentArea: true
        });
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        LayoutAnimation.easeInEaseOut();
    }

    tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
        var formattedTime = time;

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
            formattedTime = `${time[0]}.${time[2]} ${time[5]}`
        }

        return formattedTime;
        // return time.join(''); // return adjusted time or original string
    }

    getDateToDisplay(DateObj) {
        var hours = DateObj.getHours();
        var minutes = '';
        String(DateObj.getMinutes()).length < 2 ? minutes = '0' + DateObj.getMinutes() : minutes = DateObj.getMinutes();
        var month = common.getMonthName(DateObj.getMonth()).substring(0, 3);
        var time = this.tConvert(`${hours}:${minutes}:00`);
        var date = DateObj.getDate();
        return `${time} ${month} ${date}`
    }

    errorImage(Id) {

        for (i = 0; i < this.state.commentsArray.length; i++) {
            if (Id == this.state.commentsArray[i].Id) {
                var commArr = this.state.commentsArray;
                commArr.splice(i, 1, Object.assign(this.state.commentsArray[i], { ProfileImage: '' }));
                this.setState({
                    commentsArray: commArr
                });
                break;
            }
        }

    }

    renderRepostAbuse(IsApproved, UserId, Id) {
        if (IsApproved) {
            return (
                <View style={{
                    flexDirection: 'row', alignSelf: 'flex-end', alignItems: 'center', paddingTop: moderateScale(5)
                }}>
                    <FAIcon name='warning'
                        size={moderateScale(12)}
                        style={{ alignSelf: 'center', color: '#e64545', marginRight: moderateScale(5) }}
                    />
                    <Text style={styles.reportTextStyle}>Reported as Abuse</Text>
                </View>
            );
        } else {
            return (
                <View>
                    {(UserId != this.props.user.Id) ?
                        <TouchableOpacity
                            style={{
                                alignSelf: 'flex-end',
                                paddingTop: moderateScale(5)
                            }}
                            onPress={() => this.setState({ modalVisible: true, commentId: Id })}>
                            <Text style={[styles.reportTextStyle, { color: '#6f73ff' }]}> Report Abuse </Text>
                        </TouchableOpacity>
                        : null
                    }
                </View>
            );
        }
    }

    likeClicked(commentId, isLiked) {
        console.log("isLiked", isLiked);
        var methodName = '';
        const req = new likecomment();
        req.UserId = this.props.user.Id;
        req.Id = commentId;
        console.log("likecomment", req);
        var serviceRequest = common.encryptData(JSON.stringify(req));
        var serviceBody = { request: serviceRequest };
        console.log('serviceBody', serviceBody);
        isLiked == true ? methodName = 'UnlikeBlogComment' : methodName = 'SaveBlogCommentLike';
        common.callService(methodName, serviceBody)
            .then(SaveBlogCommentLike => {
                console.log(methodName, SaveBlogCommentLike);
                if (SaveBlogCommentLike.ReturnCode == '1') {
                    let tempCommArr = this.state.commentsArray;
                    let commIndex = tempCommArr.findIndex(comm =>
                        String(comm.Id) == String(commentId))
                    console.log("commIndex", commIndex);
                    console.log(tempCommArr);

                    if (methodName == 'UnlikeBlogComment') {
                        tempCommArr[commIndex].isLiked = false;
                        tempCommArr[commIndex].LikeCount -= 1;
                    } else {
                        tempCommArr[commIndex].isLiked = true;
                        tempCommArr[commIndex].LikeCount += 1;
                    }
                    this.setState({
                        commentsArray: tempCommArr,
                    });
                }

            })
            .catch(err => {
                console.log(err);

            })
    }

    renderComments(comment) {
        // console.log('commentsLIst', comment)
        let iconColor, iconName;
        let { Comment, Name, ProfileImage, CommentDate, Id, UserId, IsApproved, LikeCount, isLiked } = comment.item;
        var DateObj = common.epochToDateObj(CommentDate.replace(/[^0-9]/g, ''));
        const CommentDateToDisplay = this.getDateToDisplay(DateObj);
        LikeCount == null ? LikeCount = 0 : LikeCount;
        isLiked == true ?
            (iconColor = '#6f73ff', iconName = 'thumbs-up')
            : (iconColor = 'white', iconName = 'thumbs-o-up');

        return (
            <View style={styles.commentsViewStyle}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={styles.profileView}>
                        {
                            (ProfileImage != '' && ProfileImage != null) ?
                                <Image style={styles.img}
                                    onError={() => this.errorImage(Id)}
                                    source={{ uri: ProfileImage }}
                                /> :
                                <FAIcon name='user-circle'
                                    size={moderateScale(65)}
                                    style={{ alignSelf: 'center', color: '#4e4e4e' }}
                                />
                        }
                        <Text style={styles.userText} >{Name}</Text>
                    </View>

                    <View style={{ flex: 1.8 }}>
                        <View style={{ minHeight: moderateScale(70) }}>
                            <Hyperlink
                                linkDefault={true}
                                linkify={linkify}
                                linkStyle={{ color: '#6f73ff', textDecorationLine: 'underline' }}
                            >
                                <Text style={styles.commentsStyle}>
                                    {Comment}
                                </Text>
                            </Hyperlink>
                        </View>
                        <View style={{ paddingRight: moderateScale(-10) }}>
                            <Text style={styles.commentDate}>
                                {CommentDateToDisplay}
                            </Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <View
                        style={{ marginLeft: moderateScale(10), flexDirection: 'row', flex: 1, alignItems: 'flex-end', alignSelf: 'flex-end' }}
                    // onPress={() => this.likeClicked(Id, isLiked)}
                    >
                        <FAIcon name='thumbs-o-up'
                            size={moderateScale(15)}
                            style={{ color: 'white', flexDirection: 'row', alignSelf: 'flex-end', alignItems: 'center', paddingTop: moderateScale(5) }}
                        />
                        <Text style={{ marginLeft: moderateScale(5), color: 'white', fontSize: moderateScale(13) }}>{LikeCount}</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                        <TouchableOpacity
                            style={{ marginRight: moderateScale(10), flexDirection: 'row', alignItems: 'flex-end', alignSelf: 'center' }}
                            onPress={() => this.likeClicked(Id, isLiked)}
                        >
                            <FAIcon name={iconName}
                                size={moderateScale(15)}
                                style={{ color: iconColor, flexDirection: 'row', alignSelf: 'flex-end', alignItems: 'center', paddingTop: moderateScale(5) }}
                            />
                            <Text style={{ marginLeft: moderateScale(5), color: iconColor, fontSize: moderateScale(13) }}>Like</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1 }}>
                        {this.renderRepostAbuse(IsApproved, UserId, Id)}
                    </View>
                </View>
                {/* <View style={{ flex: 0.2 }}>
                    {(UserId != this.props.user.Id) ?
                        <TouchableOpacity
                            onPress={() => this.setState({ modalVisible: true, commentId: Id })}>

                            <SLIcon name='options-vertical'
                                size={moderateScale(20)}
                                style={{ alignSelf: 'center', color: '#4e4e4e' }}
                            />
                        </TouchableOpacity> :
                        null
                    }
                </View> */}

                <Overlay
                    visible={this.state.modalVisible}
                    onClose={() => { this.onClose() }}

                    closeOnTouchOutside={!this.state.showReportLoader}
                    containerStyle={{
                        backgroundColor: 'rgba(0,0,0,0.1)',
                        justifyContent: 'center', margin: 0, padding: 0
                    }}
                    childrenWrapperStyle={{
                        alignSelf: 'center',
                        backgroundColor: '#ffff',
                        borderRadius: moderateScale(5), borderWidth: 1,
                    }}
                >
                    {this.getReportLoader()}


                </Overlay>

            </View>
        );
    }

    loadComments(refresh) {

        if (refresh) {
            this.setState({
                text: '',
                showCommentArea: false,
                fetchCommentsErr: '',
                saveCommentsErr: '',
                commentId: '',
                commentErr: '',
                loading: false,
                isRefreshing: false,
                showCommLoader: false,
                showReportLoader: false,
                commentReportMsg: '',
                needToLoadComments: false,
                canLoadMoreComments: true,
                pageSize: 10,
                pageNumber: 1
            });
        }

        setTimeout(() => {

            const req = new GetComment();
            req.Commodity = this.props.selectedCommodity;
            req.PageSize = this.state.pageSize;
            req.PageNumber = this.state.pageNumber;
            req.UserId = this.props.user.Id;

            console.log("AllComments", req);
            var serviceRequest = common.encryptData(JSON.stringify(req));
            var serviceBody = { request: serviceRequest };
            console.log('serviceBody', serviceBody);
            this.setState({
                loading: true,
                isRefreshing: refresh,
            });

            common.callService('BlogCommentsbyCommodity_V2', serviceBody)
                .then(BlogComments => {
                    console.log("BlogComments", BlogComments);
                    if (BlogComments.ReturnCode == '1') {
                        var commArray = [];
                        if (Number(this.state.pageNumber) == 1) {
                            this.setState({
                                commentsArray: []
                            });
                            commArray = BlogComments.Data;
                        } else {
                            commArray = this.state.commentsArray.concat(BlogComments.Data);
                            console.log(commArray);
                        }

                        if (BlogComments.Data.length % (this.state.pageSize) != 0 || BlogComments.Data.length < 1) {
                            this.setState({
                                canLoadMoreComments: false
                            });
                        }
                        this.setState({
                            commentsArray: commArray,
                            loading: false,
                            isRefreshing: false
                        });
                    } else {
                        this.setState({
                            fetchCommentsErr: BlogComments.ReturnMsg,
                            loading: false,
                            isRefreshing: false
                        });
                    }

                })
                .catch(err => {
                    console.log(err);
                    this.setState({
                        fetchCommentsErr: err,
                        loading: false,
                        isRefreshing: false
                    });
                })

        }, 10);

    }

    getCommentList() {
        if (this.state.commentsArray.length > 0) {
            return (
                <FlatList
                    data={this.state.commentsArray}
                    renderItem={(comment) => this.renderComments(comment)}
                    keyExtractor={(comment) => String(comment.Id)}
                    removeClippedSubviews={false}
                    extraData={this.state}
                    onEndReachedThreshold={0.5}
                    onEndReached={() => this.loadNextPage()}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={() => this.loadComments(true)}
                            title="Pull to refresh"
                            tintColor="#0000"
                            progressBackgroundColor="transparent"
                            titleColor="#0000"
                            colors={['#97f8cc']}
                        />
                    }
                />
            );
        } else if (this.state.loading) {
            return null;
        }
        return (
            <View style={{ margin: moderateScale(20), alignItems: 'center' }}>
                <Text style={styles.commentsStyle}>No Comments Yet!</Text>
            </View>
        );
    }

    render() {
        const { height, width } = Dimensions.get('window');
        iconSize = (height + width) / 45;

        return (
            <View style={{
                marginLeft: moderateScale(15),
                marginRight: moderateScale(15),
                flex: 1
            }}>
                <Loader loading={this.state.loading} />
                <View style={{ flexDirection: 'row', marginBottom: moderateScale(5) }}>

                    <View style={{ flex: 1 }}>

                        <TouchableOpacity
                            onPress={() => this.addCommentClicked()} >
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                <FAIcon name='plus-circle'
                                    size={iconSize}
                                    style={{ color: '#6f73ff', paddingRight: moderateScale(5) }}
                                />

                                <Text style={{ color: 'white', fontSize: verticalScale(13) }}>Add New Comment</Text>
                            </View>

                        </TouchableOpacity>
                    </View>


                </View>
                {this.getWriteComment()}

                {this.getCommentList()}

            </View >

        );
    }

}

const styles = {
    buttonStyle: {
        justifyContent: "center",
        alignItems: 'center',
        // marginRight: scale(20),
        width: moderateScale(95),
        height: moderateScale(34),
    },
    textStyle: {
        color: 'white',
        fontFamily: 'Lato',
        fontSize: moderateScale(14),
        fontWeight: 'bold'
    },
    img: {
        alignSelf: 'center',
        height: moderateScale(72),
        width: moderateScale(72),
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#6f6f6f'
    },
    commentView: {
        // marginLeft: moderateScale(20),
        // marginRight: moderateScale(20),
        marginBottom: moderateScale(25),
        borderColor: '#505266',
        borderWidth: 1,
        borderTopRightRadius: moderateScale(5),
        borderTopLeftRadius: moderateScale(5),
        borderBottomLeftRadius: moderateScale(5),
        borderBottomRightRadius: moderateScale(5),
        marginBottom: moderateScale(20)
    },
    commentsViewStyle: {
        // flexDirection: 'row',
        // marginLeft: moderateScale(20),
        // marginRight: moderateScale(20),
        paddingBottom: moderateScale(20),
        marginTop: moderateScale(20),
        borderBottomWidth: 1,
        borderBottomColor: '#393939'
    },
    profileView: {
        flex: 0.7,
        paddingRight: moderateScale(10),
        paddingLeft: moderateScale(10),
        // paddingTop: moderateScale(10)
    },
    userText: {
        alignSelf: 'center',
        fontSize: moderateScale(14),
        color: '#c2c2c2',
        paddingTop: moderateScale(10),
        textAlign: 'center',
    },
    commentsStyle: {
        fontSize: moderateScale(13),
        color: '#c2c2c2',
        // paddingRight: moderateScale(10),
        // paddingLeft: moderateScale(10)
    },
    commentDate: {
        fontSize: moderateScale(12),
        color: '#696969',
        alignSelf: 'flex-end',
        paddingTop: moderateScale(5)
    },
    reportTextStyle: {
        fontSize: moderateScale(12),
        color: '#e64545',
    },
    errorMsg: {
        color: '#e64545',
        fontSize: moderateScale(13),
        // marginLeft: moderateScale(20),
        marginTop: moderateScale(-15)
    },
    uploadModalButtonsView: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop: moderateScale(30)
    },
    uploadItemInnerView: {
        flex: 1,
        alignItems: 'center',
        borderRightWidth: 1,
        borderColor: '#c8c8cd'
    },
    uploadModalIcons: {
        color: '#494dd9',
        fontSize: moderateScale(30),
        // marginRight: moderateScale(5)
    },
    uploadModalText: {
        marginTop: moderateScale(10),
        fontSize: moderateScale(14),
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center'
    },
};

const mapStateToProps = state => {
    // const { commentsArray, loading, saveCommentDetails, errorSaveComment, errorGetComment } = state.commDetails;
    const { user } = state.auth
    return { user }
}

export default connect(mapStateToProps)(Blog);

class SubmitComment {
    UserId;
    Commodity;
    Comment;
    constructor() { }
}
class ReportComment {
    Id;
    AbuseReporter;
}
class GetComment {
    Commodity;
    UserId;
    PageNumber;
    PageSize;
    constructor() {

    }
}
class likecomment {
    UserId;
    Id;
    constructor() {

    }
}
