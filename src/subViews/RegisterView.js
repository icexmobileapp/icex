import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, UIManager, LayoutAnimation, Keyboard, Platform } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Item, Input, Label } from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux';
import { registerUser } from '../actions';
import Button from '../components/Button';
import common from '../common';
import Loader from "../components/Loader";
import CountryPicker from "../components/CountryPicker";

class RegisterView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            seconds: 60,
            time: {},
            timer: 0,
            loading: false,
            index: 0,
            name: '',
            email: '',
            mobileNo: '',
            location: '',
            systemGeneratedOtp: '',
            otp: '',
            password: '',
            cnfpassword: '',
            hidePassword: true,
            hideCnfPassword: true,
            nameErr: '',
            emailErr: '',
            isEmailValid: true,
            checkedEmailsArr: [],
            isMobileValid: true,
            // isPinCodeValid: true,
            checkedMobilesArr: [],
            mobileErr: '',
            locationErr: '',
            otpErr: '',
            passwordErr: '',
            cnfPasswordErr: '',
            registerErr: '',
            isCCPickerOpen: false,
            countryCode: '+91', //Default should be India
            countryFlag: '🇮🇳', //Default should be India
        };
    }
    // componentWillUpdate() {
    //     UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    //     LayoutAnimation.linear();
    // }
    animate() {
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        LayoutAnimation.linear();
    }

    componentWillReceiveProps(nextProps) {
        // console.log('componentWillReceiveProps');
        // console.log(this.props);
        // console.log(nextProps);
        this.setState({
            registerErr: nextProps.errorRegistration
        });
        if (this.props.user == null && nextProps.user != null) {
            this.props.route.navigation.navigate("SignedIn");
        }
    }

    resetErrors() {
        this.setState({
            nameErr: '',
            emailErr: '',
            mobileErr: '',
            locationErr: '',
            otpErr: '',
            passwordErr: '',
            cnfPasswordErr: '',
            registerErr: ''
        });
    }

    registerClicked() {
        // console.log(this.state);
        this.resetErrors();
        if (this.state.index == 0) {

            var proceed = true;

            if (this.state.name.trim() == '') {
                this.setState({
                    nameErr: 'Please enter name',
                });
                this.animate();
                proceed = false;
            }

            if (this.state.email.trim() == '') {
                this.setState({
                    emailErr: 'Please enter email Id',
                });
                this.animate();
                proceed = false;
            } else if (!common.validateEmail(this.state.email.trim())) {
                this.setState({
                    emailErr: 'Please enter valid email Id',
                });
                this.animate();
                proceed = false;
            } else if (!this.state.isEmailValid) {
                this.setState({
                    emailErr: 'Email Id already exists',
                });
                this.animate();
                proceed = false;
            }

            if (this.state.mobileNo.trim() == '') {
                this.setState({
                    mobileErr: 'Please enter mobile number',
                });
                this.animate();
                proceed = false;
            } else if (!common.validateMobileNo(this.state.mobileNo.trim(), this.state.countryCode)) {
                this.setState({
                    mobileErr: 'Please enter valid mobile number',
                });
                this.animate();
                proceed = false;
            }
            // else if (!this.state.isMobileValid) {
            //     this.setState({
            //         mobileErr: 'Mobile number already exists',
            //     });
            //     this.animate();
            //     proceed = false;
            // }

            if (this.state.location.trim() == '') {
                this.setState({
                    locationErr: 'Please enter your pincode',
                });
                this.animate();
                proceed = false;
            } else if (this.state.location.trim().length < 6) {
                this.setState({
                    locationErr: 'Please enter valid 6 digit pincode',
                });
                this.animate();
                proceed = false;
            } else if (this.state.countryCode != '+91' && this.state.location != '000000') {
                this.setState({
                    locationErr: 'For countries other than India, enter 000000 as Pincode',
                });
                this.animate();
                proceed = false;
            }
            // else if (!this.state.isPinCodeValid) {
            //     this.setState({
            //         locationErr: 'Please enter valid pincode',
            //     });
            //     this.animate();
            //     proceed = false;
            // }

            if (proceed) {
                this.setState({
                    loading: true,
                });
                this.checkMobile();

                // this.setState({
                //     index: 1,
                // });
            }

        } else if (this.state.index == 1) {
            var proceed = true;

            if (this.state.otp.trim() == '') {
                this.setState({
                    otpErr: 'Please enter OTP',
                });
                this.animate();
                proceed = false;
            } else if (this.state.otp.trim().length < 6) {
                this.setState({
                    otpErr: 'Please enter valid 6 digit OTP',
                });
                this.animate();
                proceed = false;
            }
            else if (this.state.otp != this.state.systemGeneratedOtp) {
                this.setState({
                    otpErr: 'Entered OTP is incorrect',
                });
                this.animate();
                proceed = false;
            }

            if (proceed) {
                this.animate();
                clearInterval(this.state.timer);
                this.setState({
                    index: 2,
                });
                var _this = this
                setTimeout(function () {
                    _this.animate();
                    _this.setState({
                        index: 3,
                    });
                }, 2000);
            }

            // if (proceed) {
            //     this.animate();
            //     clearInterval(this.state.timer);
            //     this.setState({
            //         index: 2,
            //     });
            //     setTimeout(() => {
            //         this.animate();
            //         this.setState({
            //             index: 3,
            //         });
            //     }, 2000);
            // }

        } else if (this.state.index == 3) {
            var proceed = true;

            if (this.state.password.trim() == '') {
                this.setState({
                    passwordErr: 'Please enter password',
                });
                this.animate();
                proceed = false;
            } else if (!common.validatePassword(this.state.password.trim())) {
                this.setState({
                    passwordErr: `Password should be between 8 to 20 characters & should contain at least one lowercase, uppercase, numeric digit and special character`,
                });
                this.animate();
                proceed = false;
            }

            if (this.state.cnfpassword.trim() == '') {
                this.setState({
                    cnfPasswordErr: 'Please enter password',
                });
                this.animate();
                proceed = false;
            } else if (this.state.password.trim() != this.state.cnfpassword.trim() && this.state.cnfpassword.trim() != '') {
                this.setState({
                    cnfPasswordErr: 'Passwords didn\'t match',
                });
                this.animate();
                proceed = false;
            }

            if (proceed) {
                //Code to register

                const request = new RegisterUser();
                request.Name = this.state.name;
                request.Email = this.state.email;
                request.Mobile = this.state.mobileNo;
                request.Location = this.state.location;
                request.Password = this.state.password;
                request.Platform = Platform.OS === 'ios' ? 2 : 1;
                request.CountryCode = this.state.countryCode;

                console.log(request);

                const serviceRequest = common.encryptData(JSON.stringify(request));
                const serviceBody = { request: serviceRequest };
                this.props.registerUser(serviceBody);
                // this.props.route.navigation.navigate("SignedIn");
            }
        }
    }

    sendOtp() {
        if (this.state.systemGeneratedOtp == '') {
            const generatedOtp = Math.floor(100000 + Math.random() * 900000);
            this.setState({
                systemGeneratedOtp: generatedOtp,
            });
        }
        var _this = this;
        setTimeout(function () {

            console.log('Otp :' + _this.state.systemGeneratedOtp);

            const request = {
                Mobile: _this.state.mobileNo,
                OTP: _this.state.systemGeneratedOtp,
                CountryCode: _this.state.countryCode
            };
            console.log(request);

            const serviceRequest = common.encryptData(JSON.stringify(request));

            const serviceBody = { request: serviceRequest };
            // _this.props.sendOtp(serviceBody);
            // _this.setState({
            //     loading: true,
            // });
            common.callService('UserRegistrationSendOTP_v2', serviceBody)
                .then(otpDetails => {
                    console.log(otpDetails);
                    if (otpDetails.ReturnCode == '1') {
                        _this.startTimer();
                        if (_this.state.index == 0) {
                            _this.setState({
                                loading: false,
                                index: 1,
                                seconds: 60
                            });
                        }

                        // this.startTimer();
                        // if (this.state.index == 0) {
                        //     this.animate();
                        //     this.setState({
                        //         index: 1,
                        //         seconds: 60
                        //     });

                        // }
                    }
                    else {
                        if (_this.state.index == 0) {
                            _this.setState({
                                loading: false,
                                locationErr: otpDetails.ReturnMsg
                            });
                        } else if (_this.state.index == 1) {
                            _this.setState({
                                loading: false,
                                otpErr: otpDetails.ReturnMsg
                            });
                        }

                    }
                })
                .catch(err => {
                    console.log(err);
                    if (_this.state.index == 0) {
                        _this.setState({
                            loading: false,
                            locationErr: 'Couldn\'t connect to server'
                        });
                    } else if (_this.state.index == 1) {
                        _this.setState({
                            loading: false,
                            otpErr: 'Couldn\'t connect to server'
                        });
                    }

                })

        }, 20);
    }

    backClicked() {

        clearInterval(this.state.timer);
        if (this.state.index == 1) {
            this.animate();
            this.setState({
                seconds: 60,
                // time: {},
                timer: 0,
                // startTimer: this.startTimer.bind(this),
                // countDown: this.countDown.bind(this),
                index: 0,
                otp: '',
                otpErr: '',
                systemGeneratedOtp: ''
            });
        }
        // if (this.state.index == 1) {
        //     this.setState({
        //         index: 0,
        //         otp: '',
        //         otpErr: '',
        //         systemGeneratedOtp: ''
        //     });
        // }
    }

    resendOtp() {
        this.setState({
            seconds: 60,
            timer: 0,
            systemGeneratedOtp: ''
        });
        setTimeout(() => {
            this.sendOtp();
        }, 10);

    }

    checkEmail() {
        if (common.validateEmail(this.state.email.trim())) {

            var req = { Email: this.state.email.trim() };
            console.log('req', req);
            var serviceRequest = common.encryptData(JSON.stringify(req))
            var serviceBody = { request: serviceRequest };
            // console.log('serviceBody', serviceBody);
            this.setState({
                loading: true,
            });
            common.callService('CheckUserEmail', serviceBody)
                .then(CheckUserEmail => {
                    console.log(CheckUserEmail);
                    this.setState({
                        emailErr: '',
                        loading: false,
                    });
                    if (CheckUserEmail.ReturnCode == '1') {
                        this.setState({
                            emailErr: 'Email Id already exists',
                            isEmailValid: false,
                        });
                    } else if (CheckUserEmail.ReturnCode == '0') {
                        this.setState({
                            isEmailValid: true,
                        });
                    }
                })
                .catch(err => {
                    this.setState({
                        loading: false,
                    });
                    console.log(err);

                });
        }
    }

    checkMobile() {
        let req = {
            Mobile: this.state.mobileNo.trim(),
            CountryCode: this.state.countryCode
        };

        console.log('req', req);
        let serviceRequest = common.encryptData(JSON.stringify(req))
        let serviceBody = { request: serviceRequest };
        // console.log('serviceBody', serviceBody);
        this.setState({
            // loading: true,
            mobileErr: ''
        });
        common.callService('CheckUserMobile_v2', serviceBody)
            .then(CheckUserMobile => {
                console.log(CheckUserMobile);
                this.checkPincode();
                if (CheckUserMobile.ReturnCode == '1') {
                    this.setState({
                        mobileErr: 'Mobile number already exists',
                        isMobileValid: false,
                    });
                } else if (CheckUserMobile.ReturnCode == '0') {
                    this.setState({
                        mobileErr:'',
                        isMobileValid: true,
                    });
                }
            })
            .catch(err => {
                this.checkPincode();
                console.log(err);
            });
    }

    checkPincode() {
        if (this.state.location.trim().length == 6) {
            if (this.state.countryCode == '+91') {
                var req = { Pincode: this.state.location.trim() };
                console.log('req', req);
                var serviceRequest = common.encryptData(JSON.stringify(req))
                var serviceBody = { request: serviceRequest };
                // console.log('serviceBody', serviceBody);
                this.setState({
                    // loading: true,
                });
                common.callService('CheckPincode', serviceBody)
                    .then(CheckPincode => {
                        console.log(CheckPincode);
                        if (CheckPincode.ReturnCode == '0') {
                            this.setState({
                                locationErr: 'Please enter valid pincode',
                                // isPinCodeValid: false,
                                loading: false,
                            });
                        } else if (CheckPincode.ReturnCode == '1') {
                            this.setState({
                                locationErr: '',
                                // isPinCodeValid: true,
                            });
                            if (this.state.isMobileValid) {
                                this.sendOtp();
                            } else {
                                this.setState({
                                    loading: false,
                                });
                            }
                        } else {
                            if (this.state.isMobileValid) {
                                this.sendOtp();
                            } else {
                                this.setState({
                                    loading: false,
                                });
                            }
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        if (this.state.isMobileValid) {
                            this.sendOtp();
                        } else {
                            this.setState({
                                loading: false,
                            });
                        }
                    });
            } else {
                if (this.state.location == '000000') {
                    this.setState({
                        loading: false,
                    });
                    this.sendOtp();
                } else {
                    this.setState({
                        locationErr: 'For countries other than India, enter 000000 as Pincode',
                        loading: false,
                    });
                }
            }
        }
    }

    togglePassword = () => {
        Keyboard.dismiss();
        this.setState({ hidePassword: !this.state.hidePassword });
    }

    toggleCnfPassword = () => {
        Keyboard.dismiss();
        this.setState({ hideCnfPassword: !this.state.hideCnfPassword });
    }

    startTimer() {
        if (this.state.timer == 0 && this.state.seconds > 0) {
            // setTimeout(() => {
            this.state.timer = setInterval(() => {
                this.countDown()
            }, 1000);
            // }, 5);

        }
    }

    countDown() {
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1;
        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });
        // Check if we're at zero.
        if (seconds == 0 || seconds < 0) {
            clearInterval(this.state.timer);
        }
    }

    secondsToTime(secs) {
        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        // if (divisor_for_seconds < 10) {
        //     var seconds = '0' + Math.ceil(divisor_for_seconds);
        // } else {
        //     var seconds = Math.ceil(divisor_for_seconds);
        // }

        let seconds = String(Math.ceil(divisor_for_seconds)).padStart(2, '0')
        let obj = {
            'h': hours,
            'm': minutes,
            's': seconds
        };
        return obj;
    }

    showTimer() {
        if (this.state.seconds == 0) {
            return (
                <TouchableOpacity
                    style={{ alignSelf: 'flex-end', marginRight: scale(20), marginTop: scale(5) }}
                    onPress={() => this.resendOtp()}>
                    <Text
                        style={{ color: 'white', fontSize: moderateScale(12) }}
                    >RESEND OTP</Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={{ alignSelf: 'flex-end', marginRight: scale(20), marginTop: scale(5) }}>
                    <Text style={{ color: 'white', fontSize: moderateScale(12) }}
                    >00:{this.state.time.s} </Text>
                </View>
            )
        }

    }

    renderPage() {
        if (this.state.index == 0) {
            return (
                <View>
                    <View style={{ fontWeight: 'bold', color: 'white', paddingLeft: scale(20), paddingRight: scale(20), }}>
                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle} >NAME</Label>
                            <Input style={styles.inputText}
                                value={this.state.name}
                                maxLength={30}
                                onChangeText={(text) => { this.setState({ name: common.charOnly(text) }) }}
                            />
                        </Item>
                        {!this.state.nameErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.nameErr}</Text> : null
                        }
                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle}>EMAIL ID</Label>
                            <Input style={styles.inputText} keyboardType='email-address'
                                value={this.state.email}
                                onBlur={() => this.checkEmail()}
                                onChangeText={(text) => { this.setState({ email: text }) }}
                            />
                        </Item>
                        {!this.state.emailErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.emailErr}</Text> : null
                        }
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View
                                style={styles.countryCode}
                            >
                                <TouchableOpacity
                                    style={{ flexDirection: 'row', }}
                                    onPress={() => this.setState({ isCCPickerOpen: true })}
                                >
                                    <Text style={styles.inputText}>{this.state.countryFlag} {this.state.countryCode}</Text>
                                    <Icon name='angle-down'
                                        size={moderateScale(15)}
                                        style={{ color: '#fff', marginLeft: moderateScale(5), paddingRight: moderateScale(5) }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <CountryPicker
                                modalVisible={this.state.isCCPickerOpen}
                                isClosed={() => {
                                    console.log('isCloseCalled');
                                    this.setState({ isCCPickerOpen: false })
                                }}
                                countryChanged={(countryCode, countryFlag) => {
                                    console.log('countryChanged', countryCode, countryFlag);
                                    this.setState({ countryCode, countryFlag })
                                }}
                            />
                            <View style={{ flex: 0.75 }}>
                                <Item floatingLabel style={styles.placeholderText}>
                                    <Label style={styles.labelStyle}>MOBILE NUMBER</Label>
                                    <Input style={styles.inputText} keyboardType='numeric'
                                        maxLength={this.state.countryCode == '+91' ? 10 : 16}
                                        value={this.state.mobileNo}
                                        onChangeText={(text) => {
                                            this.setState({ mobileNo: common.numberOnly(text) });
                                        }}
                                    />
                                </Item>
                            </View>
                        </View>
                        {!this.state.mobileErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.mobileErr}</Text> : null
                        }
                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle}>PINCODE</Label>
                            <Input style={styles.inputText} keyboardType='numeric' maxLength={6}
                                value={this.state.location}
                                onChangeText={(text) => {
                                    this.setState({ location: common.numberOnly(text) });
                                }}
                            />
                        </Item>
                        {!this.state.locationErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.locationErr}</Text> : null
                        }
                    </View>
                    <Button
                        style={{ marginTop: moderateScale(50), marginBottom: moderateScale(20) }}
                        whenPressed={() => this.registerClicked()}>
                        SUBMIT
                </Button>
                </View>
            );
        } else if (this.state.index == 1) {
            return (
                <View>
                    {/* <Text style={{color:'white', paddingLeft:scale(20),fontSize: moderateScale(16),marginTop: verticalScale(20)}}>OTP</Text> */}
                    <View style={{ fontWeight: 'bold', color: 'white', paddingLeft: scale(20), paddingRight: scale(20), }}>
                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle} >ENTER OTP</Label>
                            <Input style={styles.inputText} keyboardType='numeric' maxLength={6}
                                value={this.state.otp}
                                onChangeText={(text) => { this.setState({ otp: common.numberOnly(text) }) }}
                            />
                        </Item>
                        {!this.state.otpErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.otpErr}</Text> : null
                        }
                    </View>
                    {/* <TouchableOpacity
                        style={{ alignSelf: 'flex-end', marginRight: scale(20), marginTop: scale(5) }}
                        onPress={() => this.resendOtp()}>
                        <Text
                            style={{ color: 'white', fontSize: moderateScale(12) }}
                        >RESEND OTP</Text>
                    </TouchableOpacity> */}
                    {this.showTimer()}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginRight: scale(40), marginTop: moderateScale(50) }}>
                        <Button
                            whenPressed={() => this.registerClicked()}>
                            SUBMIT
                        </Button>
                        <TouchableOpacity
                            style={{ alignSelf: 'center', justifyContent: 'center' }}
                            onPress={() => this.backClicked()}>
                            <Text
                                style={{ color: 'white', fontSize: moderateScale(15) }}
                            >GO BACK</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        } else if (this.state.index == 2) {
            return (
                <View style={{ paddingLeft: scale(20), marginTop: scale(30), paddingRight: scale(20), }}>
                    <Text style={{ fontWeight: '400', color: 'white', fontSize: moderateScale(15) }}>
                        Hi user, the OTP entered is correct.{"\n"}
                        Kindly create your credentials.
                    </Text>
                </View>
            );
        } else if (this.state.index == 3) {
            return (
                <View>
                    <View style={{ fontWeight: 'bold', color: 'white', paddingLeft: scale(20), paddingRight: scale(20), }}>
                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle} >MOBILE NUMBER</Label>
                            <Input style={styles.inputText}
                                editable={false}
                                value={this.state.mobileNo}
                                onChangeText={(text) => { this.setState({ mobileNo: common.numberOnly(text) }) }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle}>PASSWORD</Label>
                            <Input style={styles.inputText} secureTextEntry={this.state.hidePassword}
                                value={this.state.password}
                                maxLength={20}
                                onChangeText={(text) => { this.setState({ password: text }) }}
                            />
                        </Item>
                        <TouchableOpacity
                            style={styles.eyeBtn}
                            onPress={this.togglePassword}
                        >
                            {/* <Image
                                source={(this.state.hidePassword) ? require('../assets/imgs/eyeIcon.png') : require('../assets/imgs/eyeIconOpen.png')}
                                style={styles.eyeBtnImage}
                            ></Image> */}
                            <Icon name={(this.state.hidePassword) ? 'eye-slash' : 'eye'}
                                size={moderateScale(15)}
                                style={{ color: 'white' }}
                                light
                            />
                        </TouchableOpacity>
                        {!this.state.passwordErr == '' ?
                            <Text style={[styles.errorMsg, { marginTop: 5 }]}>{this.state.passwordErr}</Text> : null
                        }

                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle}>CONFIRM PASSWORD</Label>
                            <Input style={styles.inputText} secureTextEntry={this.state.hideCnfPassword}
                                value={this.state.cnfpassword}
                                maxLength={20}
                                onChangeText={(text) => { this.setState({ cnfpassword: text }) }}
                            />
                        </Item>
                        <TouchableOpacity
                            style={styles.eyeBtn}
                            onPress={this.toggleCnfPassword}
                        >
                            {/* <Image
                                source={(this.state.hideCnfPassword) ? require('../assets/imgs/eyeIcon.png') : require('../assets/imgs/eyeIconOpen.png')}
                                style={styles.eyeBtnImage}
                            ></Image> */}
                            <Icon name={(this.state.hideCnfPassword) ? 'eye-slash' : 'eye'}
                                size={moderateScale(15)}
                                style={{ color: 'white' }}
                                light
                            />
                        </TouchableOpacity>
                        {!this.state.cnfPasswordErr == '' ?
                            <Text style={[styles.errorMsg, { marginTop: 5 }]}>{this.state.cnfPasswordErr}</Text> : null
                        }
                        {!this.state.registerErr == '' ?
                            <Text style={[styles.errorMsg, { marginTop: 5 }]}>{this.state.registerErr}</Text> : null
                        }
                    </View>
                    <Button
                        style={{ marginTop: moderateScale(50) }}
                        whenPressed={() => this.registerClicked()}>
                        SUBMIT
                </Button>
                </View>
            );
        }
    }

    render() {
        return (
            <View>
                <Loader loading={this.state.loading} />
                {/* <ScrollView> */}
                {this.renderPage()}
                {/* </ScrollView> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    placeholderText: {
        borderColor: '#91ace1',
        fontSize: moderateScale(13),
        marginTop: verticalScale(20)
    },
    labelStyle: {
        color: '#cdeff5',
        fontSize: moderateScale(14)
    },
    inputText: {
        color: '#fff',
        fontSize: moderateScale(14)
    },
    errorMsg: {
        color: '#910a0a',
        fontSize: moderateScale(13)
    },
    eyeBtn: {
        alignSelf: 'flex-end',
        height: moderateScale(20),
        width: moderateScale(20),
        marginTop: moderateScale(-25)
    },
    eyeBtnImage:
    {
        resizeMode: 'contain',
        height: '100%',
        width: '100%'
    },
    countryCode: {
        height: moderateScale(35),
        flex: 0.25,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        borderColor: '#91ace1',
        borderBottomWidth: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    }
});

// export default RegisterView;

const mapStateToProps = (state) => {
    const { errorRegistration, loading, registrationDetails } = state.auth
    return { errorRegistration, loading, registrationDetails }
}


export default connect(mapStateToProps, { registerUser })(RegisterView);

class RegisterUser {
    Name;
    Email;
    Mobile;
    Location;
    Password;
    PAN = null;
    Device = 2;
    Platform;
    CountryCode;
}