import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, UIManager, LayoutAnimation, Keyboard, AsyncStorage } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Dropdown } from 'react-native-material-dropdown';
import { withNavigationFocus } from "react-navigation";
import { Toast } from 'native-base';
import { connect } from 'react-redux';
import { refreshTimeChanged } from "../actions";


class AppSettings extends Component {

    state = {
        RefreshTime: '5 SEC',
    }

    componentWillMount() {
        AsyncStorage.getItem("refreshTime").then((value) => {
            if (value != null && value != undefined) {
                this.setState({ RefreshTime: value });
            }
        }).done();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.isFocused !== this.props.isFocused) {
            // console.log('this.props.isFocused' +this.props.isFocused);
            if (this.props.isFocused) {
                AsyncStorage.getItem("refreshTime").then((value) => {
                    if (value != null && value != undefined) {
                        if(this.state.RefreshTime != value)
                        this.setState({ RefreshTime: value });
                    }
                }).done();
            }
          }
    }

    changeRefreshTime() {
        AsyncStorage.setItem("refreshTime", this.state.RefreshTime);
        this.props.refreshTimeChanged();
        setTimeout(() => {
            Toast.show({
                text: "Refresh time changed to " + this.state.RefreshTime,
                type: "success"
            });
        }, 10)
    }

    setRefreshTime(time) {
        this.setState({
            RefreshTime: time,
        });
    }

    render() {
        return (
            <View>
                <View style={styles.topViewStyle}>
                    <Text style={{ fontSize: moderateScale(18), color: '#b8b8b8' }}>Refresh your screen after every</Text>
                </View>

                <View style={{ flexDirection: 'row', marginLeft: moderateScale(20), paddingTop: moderateScale(20), paddingBottom: moderateScale(10) }}>

                    <View style={styles.dropdownViewStyle}>
                        <Dropdown
                            // ref={this.codeRef}
                            value={this.state.RefreshTime}
                            textColor={'white'}
                            onChangeText={(time) => this.setRefreshTime(time)}
                            containerStyle={{ width: moderateScale(75), alignSelf: 'center', marginBottom: moderateScale(15) }}
                            data={refreshTimeData}
                            // propsExtractor={({ props }, index) => props}
                            fontSize={moderateScale(14)}
                            itemTextStyle={{ fontSize: moderateScale(13) }}
                            pickerStyle={{ backgroundColor: '#000000', borderRadius: moderateScale(5) }}
                            itemColor={'#727272'}
                            itemCount={8}
                            IconDetails={{ name: 'angle-down', size: moderateScale(20), color: '#777777' }}
                            dropdownPosition={-7.5}
                            dropdownMargins={{ min: 15, max: 15 }}
                        />
                    </View>

                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end', padding: moderateScale(5), marginLeft: moderateScale(20) }}
                        onPress={() => this.changeRefreshTime()}
                    >
                        <Text style={{ fontSize: moderateScale(14), color: '#6f73ff' }}>SAVE</Text>
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    const { isRefreshTimeChanged } = state.commDetails;
    return { isRefreshTimeChanged }
}

export default connect(mapStateToProps, { refreshTimeChanged })(withNavigationFocus(AppSettings));

const styles = StyleSheet.create({
    topViewStyle: {
        marginLeft: moderateScale(20),
        paddingTop: moderateScale(20),
        paddingBottom: moderateScale(10)
    },
    dropdownViewStyle: {
        width: moderateScale(100),
        height: moderateScale(40),
        borderColor: '#686969',
        borderRadius: moderateScale(5),
        borderWidth: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    }
});

const refreshTimeData = [
    { value: '5 SEC' },
    { value: '10 SEC' },
    { value: '15 SEC' },
    { value: '20 SEC' },
    { value: '25 SEC' },
    { value: '30 SEC' }
];