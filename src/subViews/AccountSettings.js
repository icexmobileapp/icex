import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, UIManager, LayoutAnimation, Keyboard } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Item, Input, Label, Toast } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux';
import common from '../common';

class AppSettings extends Component {

    state = {
        currPassword: '',
        newPassword: '',
        cnfNewPassword: '',
        hideCurrPassword: true,
        hideNewPassword: true,
        hideCnfNewPassword: true,
        currPasswordErr: '',
        newPasswordErr: '',
        cnfNewPasswordErr: '',
        showToast: false,
    }

    resetState() {
        this.setState({
            currPassword: '',
            newPassword: '',
            cnfNewPassword: '',
            hideCurrPassword: true,
            hideNewPassword: true,
            hideCnfNewPassword: true,
            currPasswordErr: '',
            newPasswordErr: '',
            cnfNewPasswordErr: '',
        });
    }

    componentWillUpdate() {
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        LayoutAnimation.linear();
    }

    toggleCurrPassword = () => {
        Keyboard.dismiss();
        this.setState({ hideCurrPassword: !this.state.hideCurrPassword });
    }

    toggleNewPassword = () => {
        Keyboard.dismiss();
        this.setState({ hideNewPassword: !this.state.hideNewPassword });
    }

    toggleCnfNewPassword = () => {
        Keyboard.dismiss();
        this.setState({ hideCnfNewPassword: !this.state.hideCnfNewPassword });
    }

    changePassword() {
        this.setState({
            currPasswordErr: '',
            newPasswordErr: '',
            cnfNewPasswordErr: '',
        });
        var proceed = true;

        if (this.state.currPassword.trim() == '') {
            this.setState({
                currPasswordErr: 'Please enter current password',
            });
            proceed = false;
        }

        if (this.state.newPassword.trim() == '') {
            this.setState({
                newPasswordErr: 'Please enter new password',
            });
            proceed = false;
        } else if (!common.validatePassword(this.state.newPassword.trim())) {
            this.setState({
                newPasswordErr: `Password should be between 8 to 20 characters & should contain at least one lowercase, uppercase, numeric digit and special character`,
            });
            proceed = false;
        }

        if (this.state.cnfNewPassword.trim() == '') {
            this.setState({
                cnfNewPasswordErr: 'Please enter new password',
            });
            proceed = false;
        } else if (this.state.newPassword.trim() != this.state.cnfNewPassword.trim() && this.state.cnfNewPassword.trim() != '') {
            this.setState({
                cnfNewPasswordErr: 'Passwords didn\'t match',
            });
            proceed = false;
        } else if (this.state.currPassword.trim() == this.state.cnfNewPassword.trim()) {
            this.setState({
                cnfNewPasswordErr: 'New password same as old password',
            });
            proceed = false;
        }


        if (proceed) {
            const request = {
                Id: this.props.user.Id,
                OldPassword: this.state.currPassword,
                NewPassword: this.state.newPassword
            }
            const serviceBody = { request: common.encryptData(JSON.stringify(request)) };

            common.callService('ResetPasswordUserDetail', serviceBody)
                .then(ResetPassword => {
                    console.log(ResetPassword);
                    if (ResetPassword.ReturnCode == '1') {
                        setTimeout(() => {
                            Toast.show({
                                text: "Password updated successfully!",
                                type: "success"
                            });
                        }, 10)

                        this.resetState();
                    } else {
                        this.setState({
                            cnfNewPasswordErr: ResetPassword.ReturnMsg,
                        });
                    }

                })
                .catch(err => {
                    console.log(err);
                    this.setState({
                        cnfNewPasswordErr: 'Sorry, Unable to update password',
                    });
                })
        }

    }

    checkCurrPasswrd(){
        if (this.state.currPassword.trim() != '') {
            const request = {
                Id: this.props.user.Id,
                Password: this.state.currPassword,
            }
            const serviceBody = { request: common.encryptData(JSON.stringify(request)) };

            common.callService('VerifyPassword', serviceBody)
                .then(VerifyPassword => {
                    console.log(VerifyPassword);
                    if (VerifyPassword.ReturnCode == '2') {
                        this.setState({
                            currPasswordErr: 'Current Password is incorrect',
                        });
                    }

                })
                .catch(err => {
                    console.log(err);
                    // this.setState({
                    //     currPasswordErr: 'Sorry, Unable to update password',
                    // });
                })
        }
    }

    render() {
        return (
            <View>
                <View style={{ fontWeight: 'bold', color: 'white', paddingLeft: scale(20), paddingRight: scale(20), }}>
                    <Item floatingLabel style={styles.placeholderText}>
                        <Label style={styles.labelStyle}>CURRENT PASSWORD</Label>
                        <Input style={styles.inputText}
                            secureTextEntry={this.state.hideCurrPassword}
                            maxLength={20}
                            value={this.state.currPassword}
                            onBlur={() => this.checkCurrPasswrd()}
                            onChangeText={(text) => { this.setState({ currPassword: text, currPasswordErr: '' }) }}
                        />
                    </Item>
                    <TouchableOpacity
                        style={styles.eyeBtn}
                        onPress={this.toggleCurrPassword}
                    >
                        <Icon name={(this.state.hideCurrPassword) ? 'eye-slash' : 'eye'}
                            size={moderateScale(15)}
                            style={{ color: 'white' }}
                            light
                        />
                    </TouchableOpacity>
                    {!this.state.currPasswordErr == '' ?
                        <Text style={[styles.errorMsg, { marginTop: 5 }]}>{this.state.currPasswordErr}</Text> : null
                    }

                    <Item floatingLabel style={styles.placeholderText}>
                        <Label style={styles.labelStyle}>NEW PASSWORD</Label>
                        <Input style={styles.inputText}
                            maxLength={20}
                            secureTextEntry={this.state.hideNewPassword}
                            value={this.state.newPassword}
                            onChangeText={(text) => { this.setState({ newPassword: text, newPasswordErr: '' }) }}
                        />
                    </Item>
                    <TouchableOpacity
                        style={styles.eyeBtn}
                        onPress={this.toggleNewPassword}
                    >
                        <Icon name={(this.state.hideNewPassword) ? 'eye-slash' : 'eye'}
                            size={moderateScale(15)}
                            style={{ color: 'white' }}
                            light
                        />
                    </TouchableOpacity>
                    {!this.state.newPasswordErr == '' ?
                        <Text style={[styles.errorMsg, { marginTop: 5 }]}>{this.state.newPasswordErr}</Text> : null
                    }

                    <Item floatingLabel style={styles.placeholderText}>
                        <Label style={styles.labelStyle}>CONFIRM NEW PASSWORD</Label>
                        <Input style={styles.inputText}
                            maxLength={20}
                            secureTextEntry={this.state.hideCnfNewPassword}
                            value={this.state.cnfNewPassword}
                            onChangeText={(text) => { this.setState({ cnfNewPassword: text, cnfNewPasswordErr:'' }) }}
                        />
                    </Item>
                    <TouchableOpacity
                        style={styles.eyeBtn}
                        onPress={this.toggleCnfNewPassword}
                    >
                        <Icon name={(this.state.hideCnfNewPassword) ? 'eye-slash' : 'eye'}
                            size={moderateScale(15)}
                            style={{ color: 'white' }}
                            light
                        />
                    </TouchableOpacity>
                    {!this.state.cnfNewPasswordErr == '' ?
                        <Text style={[styles.errorMsg, { marginTop: 5 }]}>{this.state.cnfNewPasswordErr}</Text> : null
                    }
                </View>
                <LinearGradient colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
                    start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
                    style={{
                        borderRadius: moderateScale(5), width: moderateScale(135),
                        height: moderateScale(50), margin: moderateScale(20), marginTop: moderateScale(40)
                    }}
                >
                    <TouchableOpacity
                        onPress={() => this.changePassword()}
                        style={styles.buttonStyle}>
                        <Text style={styles.textStyle}>
                            SUBMIT
                        </Text>
                    </TouchableOpacity>

                </LinearGradient>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    placeholderText: {
        borderColor: '#8e8e8e',
        fontSize: moderateScale(13),
        marginTop: verticalScale(20)
    },
    labelStyle: {
        color: '#d1d1d1',
        fontSize: moderateScale(14)
    },
    inputText: {
        color: '#fff',
        fontSize: moderateScale(14)
    },
    errorMsg: {
        color: '#e64545',
        fontSize: moderateScale(13)
    },
    eyeBtn: {
        alignSelf: 'flex-end',
        height: moderateScale(20),
        width: moderateScale(20),
        marginTop: moderateScale(-25)
    },
    buttonStyle: {
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: 'transparent',
        borderRadius: scale(5),
        flex: 1
    },
    textStyle: {
        color: '#fff',
        fontFamily: 'Lato',
        fontSize: moderateScale(14),
        fontWeight: 'bold',
        alignSelf: 'center'
    }
});

const mapStateToProps = state => {
    const { user } = state.auth
    return { user }
}

export default connect(mapStateToProps)(AppSettings);
