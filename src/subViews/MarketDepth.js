import React, { Component } from 'react';
import { RefreshControl, ScrollView, StyleSheet, Text, View, TouchableHighlight, Dimensions, UIManager, LayoutAnimation, FlatList } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import common from '../common';
import { setSelectedExpiryIndex } from '../actions';

class MarketDepth extends Component {

    state = {
        isRefreshing: false,
        commodityData: this.props.commodityData[this.props.selectedCommodityId],
        expiryData: [],
        selectedExpiry: '',
        selectedExpiryIndex: this.props.selectedExpiryIndex == null ? 0 : this.props.selectedExpiryIndex,
        marketDepthData: [],
    };
    onExpiryChange(expiry) {
        this.state.expiryData.find((exp, index) => {
            var x = String(exp.value) == String(expiry);
            if (x) {
                this.setState({
                    selectedExpiryIndex: index
                })
                this.props.setSelectedExpiryIndex(index)
            }
        });
        this.generateMarketDepthArray();
        // console.log('changed')
    }

    generateMarketDepthArray() {
        const { selectedExpiryIndex, commodityData } = this.state;
        let marketDepthArr = [];
        let LastTradedTime = commodityData[selectedExpiryIndex].LastTradedTime;
        let showLTPDetail = false;
        if (LastTradedTime != null && LastTradedTime != undefined) {
            showLTPDetail = this.canShowLTP(LastTradedTime);
        }
        console.log(LastTradedTime, showLTPDetail)
        for (let i = 0; i < commodityData[selectedExpiryIndex].BuyPrice.length; i++) {
            let obj = {};
            obj = {
                id: i,
                BuyQuantity: commodityData[selectedExpiryIndex].BuyQuantity[i],
                BuyPrice: commodityData[selectedExpiryIndex].BuyPrice[i],
                SellPrice: commodityData[selectedExpiryIndex].SellPrice[i],
                SellQuantity: commodityData[selectedExpiryIndex].SellQuantity[i]
            }

            marketDepthArr.push(obj);
            if (i == commodityData[selectedExpiryIndex].BuyPrice.length - 1) {
                this.setState({
                    marketDepthData: marketDepthArr
                });
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        // console.log('componentWillReceiveProps MarketDepth');
        // console.log(this.props);
        // console.log(nextProps);


        // if (nextProps.isCalled && !this.props.isCalled) {
        //     this.loadExpiry();
        //     this.generateMarketDepthArray();
        // }
        // console.log('nextProps.commodityList: ', nextProps.commodityList);
        // console.log('nextProps.selectedCommodity: ', nextProps.selectedCommodity);
        let selectedCommodityId= nextProps.commodityList.findIndex(comm =>
            String(comm.value) == String(nextProps.selectedCommodity))

            // console.log('selectedCommodityId:',selectedCommodityId);
        if (this.props.selectedCommodity != nextProps.selectedCommodity) {
            console.log('commodityData Changed')
            this.setState({
                selectedExpiryIndex: 0,
                commodityData: nextProps.commodityData[selectedCommodityId],
            });
            setTimeout(() => {
                this.loadExpiry();
                this.generateMarketDepthArray();
            }, 5)

        } else {
            this.setState({
                commodityData: nextProps.commodityData[selectedCommodityId],
                selectedExpiryIndex: nextProps.selectedExpiryIndex == null ? 0 : nextProps.selectedExpiryIndex,
            });
            setTimeout(() => {
                this.loadExpiry();
                this.generateMarketDepthArray();
            }, 10)
        }
    }

    canShowLTP(LastTradedTime) {
        let lttDate = new Date(LastTradedTime.substr(0, 10));
        lttDate.setHours(0, 0, 0, 0);
        let currDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        if (lttDate.getTime() !== currDate.getTime()) {
            return false;
        } else {
            return true;
        }
    }

    renderMarketDepthList(MarketDepthDetails) {
        const { BuyQuantity, BuyPrice, SellPrice, SellQuantity } = MarketDepthDetails.item;
        return (
            <View style={styles.dataView}>

                <View style={[styles.commonViewStyle, { borderRightWidth: 1, borderColor: '#2e2e2e' }]}>
                    {
                        Number(BuyQuantity) != 0 ?
                            <Text style={styles.dataViewText}>{BuyQuantity}</Text>
                            : <Text style={styles.blankDataViewText}> - </Text>
                    }
                </View>

                <View style={[styles.commonViewStyle, { borderRightWidth: 1, borderColor: '#7e7e80' }]} >
                    {
                        Number(BuyPrice) != 0 ?
                            <Text style={styles.dataViewText} >{common.addDecimals(BuyPrice)}</Text>
                            : <Text style={styles.blankDataViewText}> - </Text>
                    }
                </View>

                <View style={[styles.commonViewStyle, { borderRightWidth: 1, borderColor: '#2e2e2e' }]} >
                    {
                        Number(SellPrice) != 0 ?
                            <Text style={styles.dataViewText}>{common.addDecimals(SellPrice)}</Text>
                            : <Text style={styles.blankDataViewText}> - </Text>
                    }
                </View>

                <View style={styles.commonViewStyle} >
                    {
                        Number(SellQuantity) != 0 ?
                            <Text style={styles.dataViewText} >{SellQuantity}</Text>
                            : <Text style={styles.blankDataViewText}> - </Text>
                    }
                </View>

            </View>

        );
    }

    loadExpiry() {
        var expArray = [];
        this.state.commodityData.forEach((commodity, index) => {
            let obj = { value: common.expiryDateToDisplay(commodity.ExpiryDate) };
            expArray.push(obj);
            if (index == this.state.commodityData.length - 1) {
                console.log('this.props.selectedExpiryIndex: ', this.props.selectedExpiryIndex);
                let selectedIndex = this.props.selectedExpiryIndex == null ? 0 : this.props.selectedExpiryIndex;
                console.log(expArray[selectedIndex].value);
                this.setState({
                    expiryData: expArray,
                    selectedExpiry: expArray[selectedIndex].value
                });
            }
        });
    }

    render() {
        const { height, width } = Dimensions.get('window');
        console.log('REREndering')
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', marginBottom: scale(25), height: moderateScale(41) }}>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ fontSize: moderateScale(13), fontStyle: 'italic', color: '#898989' }}>Expiry Date</Text>
                    </View>

                    <View style={{ flex: 2.5, borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-around', alignItems: 'center' }}>
                        <Dropdown
                            // ref={this.codeRef}
                            value={this.state.selectedExpiry}
                            textColor={'white'}
                            onChangeText={(expiry) => this.onExpiryChange(expiry)}
                            containerStyle={{ width: width * 0.45, alignSelf: 'center', marginBottom: moderateScale(15) }}
                            data={this.state.expiryData}
                            // propsExtractor={({ props }, index) => props}
                            fontSize={moderateScale(14)}
                            itemTextStyle={{ fontSize: moderateScale(13) }}
                            pickerStyle={{ backgroundColor: '#000000', borderRadius: moderateScale(5) }}
                            itemColor={'#727272'}
                            itemCount={this.state.expiryData.length}
                            IconDetails={{ name: 'angle-down', size: moderateScale(20), color: '#777777' }}
                            dropdownPosition={-1.5 - this.state.expiryData.length}
                            dropdownMargins={{ min: 15, max: 15 }}
                        />
                    </View>

                    <View style={{ flex: 1 }}>
                        {/* <Text style={{ fontFamily: 'italic',color:'#898989' }}>Location</Text> */}
                    </View>
                </View>
                <View style={styles.headerView}>
                    <View style={styles.headerTopView}>
                        <View style={styles.commonViewStyle}>
                            <Text style={styles.headerTopViewText}>Buy</Text>
                        </View>

                        <View style={[styles.commonViewStyle, { borderLeftWidth: 1, borderColor: '#656565' }]}>
                            <Text style={styles.headerTopViewText}>Sell</Text>
                        </View>
                    </View>


                    <View style={styles.headerBottomView}>
                        <View style={{ flexDirection: 'row', flex: 1, }}>
                            <View style={styles.commonViewStyle}>
                                <Text style={[{ color: '#15a225' }, styles.headerBottomViewText]}>Quantity</Text>
                            </View>

                            <View style={[styles.commonViewStyle, { borderRightWidth: 1, borderColor: '#656565' }]}>
                                <Text style={[{ color: '#15a225' }, styles.headerBottomViewText]}>Price</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', flex: 1, }}>
                            <View style={[styles.commonViewStyle, { borderLeftWidth: 1, borderColor: '#656565' }]}>
                                <Text style={[{ color: '#c52121' }, styles.headerBottomViewText]}>Price</Text>
                            </View>

                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={[{ color: '#c52121' }, styles.headerBottomViewText]}>Quantity</Text>
                            </View>
                        </View>

                    </View>

                </View>
                <FlatList
                    data={this.state.marketDepthData}
                    extraData={this.state}
                    renderItem={(MarketDepthDetails) => this.renderMarketDepthList(MarketDepthDetails)}
                    keyExtractor={(MarketDepthDetails) => String(MarketDepthDetails.id)}
                    removeClippedSubviews={false}
                />

                {/* {this.renderMarketDepthList()} */}
            </View>
        );
    }

}

const mapStateToProps = state => {
    const { loading, commodityData, selectedExpiryIndex, commodityList } = state.commDetails;
    return { loading, commodityData, selectedExpiryIndex, commodityList }
}

export default connect(mapStateToProps, { setSelectedExpiryIndex })(MarketDepth);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // style={{
        marginLeft: scale(15),
        marginRight: scale(15)

    }, headerView: {
        // marginLeft: scale(20),
        // marginRight: scale(20),
        borderRadius: moderateScale(5),
        borderWidth: 1,
        borderColor: '#656565',
        height: verticalScale(65),
        marginBottom: moderateScale(10)
    },
    headerTopView: {
        flexDirection: 'row',
        height: verticalScale(36),
        backgroundColor: '#4d4d4d',
        borderTopLeftRadius: moderateScale(5),
        borderTopRightRadius: moderateScale(5),
    },
    commonViewStyle: {
        flex: 1,
        justifyContent: 'center'
    },
    headerTopViewText: {
        alignSelf: 'center',
        color: 'white',
        fontSize: moderateScale(14)
    },
    headerBottomView: {
        flexDirection: 'row',
        height: verticalScale(27),
        backgroundColor: '#303030',
        borderBottomRightRadius: moderateScale(5),
        borderBottomLeftRadius: moderateScale(5),
    },
    headerBottomViewText: {
        alignSelf: 'flex-end',
        paddingRight: scale(10),
        fontSize: moderateScale(14)
    },
    dataView: {
        // marginLeft: scale(20),
        // marginRight: scale(20),
        height: verticalScale(42),
        backgroundColor: '#171717',
        flexDirection: 'row',
        borderRadius: moderateScale(5),
        marginBottom: moderateScale(10)
    },
    dataViewText: {
        color: 'white',
        alignSelf: 'flex-end',
        paddingRight: scale(3),
        // paddingLeft: scale(10),
        fontSize: moderateScale(12)
    },
    blankDataViewText: {
        color: 'white',
        alignSelf: 'center',
        // paddingRight: scale(10),
        // paddingLeft: scale(10),
        fontSize: moderateScale(14)
    }


});