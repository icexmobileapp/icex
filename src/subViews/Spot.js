import React, { Component } from 'react';
import { RefreshControl, ScrollView, StyleSheet, Text, View, FlatList, TouchableHighlight, Dimensions, UIManager, LayoutAnimation } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Dropdown } from 'react-native-material-dropdown';
import { getSpotPrice } from '../actions';
import common from '../common';
import { connect } from 'react-redux';
import Loader from "../components/Loader";

class Spot extends Component {

    state = {
        location: 'All',
        isRefreshing: false,
        needToLoadSpot: false,
        locationData: [],
        loading: false
    };

    resetState() {
        this.setState({
            location: 'All',
            isRefreshing: false,
            needToLoadSpot: false,
        });
    }

    componentWillReceiveProps(nextProps) {
        if ((this.props.selectedCommodity != nextProps.selectedCommodity) && this.props.selectedTab == 'SPOT') {

            this.resetState();
            setTimeout(() => {
                // this.getSpotDetails();
                this.loadLocationData();
            }, 5);

        } else if (nextProps.selectedTab == 'SPOT' && this.state.needToLoadSpot) {

            this.resetState();
            this.loadLocationData();
            // this.getSpotDetails();

        } else if (nextProps.isCalled && !this.props.isCalled) {

            this.loadLocationData();

        } else if ((this.props.selectedCommodity != nextProps.selectedCommodity) && this.props.selectedTab != 'SPOT') {

            this.setState({
                needToLoadSpot: true
            });

        }

        // if(!nextProps.loading){
        //     this.setState({
        //         isRefreshing: false
        //     });
        // }
    }

    loadLocationData() {
        const req = { "Commodity": this.props.selectedCommodity }
        console.log(req);
        const serviceBody = { request: common.encryptData(JSON.stringify(req)) };
        console.log(serviceBody);
        this.setState({
            loading: true
        });
        common.callService('SpotMarketPriceLocation', serviceBody)
            .then(SpotMarketPriceLocation => {
                console.log(SpotMarketPriceLocation);
                if (SpotMarketPriceLocation.ReturnCode == '1') {
                    var tempArr = [{ location: 'All' }];
                    var location = [...tempArr, ...SpotMarketPriceLocation.Data];
                    // location.splice(0, 0, SpotMarketPriceLocation.Data);
                    console.log(location);
                    const transformedLoctn = location.map(({ location }) => ({ value: location }));
                    this.setState({
                        locationData: transformedLoctn,
                        loading: false
                    });
                    this.getSpotDetails();
                } else {
                    this.setState({
                        loading: false
                    });
                    this.getSpotDetails();
                }

            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loading: false
                });
                this.getSpotDetails();
            })
    }

    getSpotDetails() {
        var deliveryCenter;
        this.state.location == 'All' ? deliveryCenter = '' : deliveryCenter = this.state.location;
        var request = { Commodity: this.props.selectedCommodity, DeliveryCenter: deliveryCenter }
        console.log(request);
        var serviceRequest = common.encryptData(JSON.stringify(request))

        var serviceBody = { request: serviceRequest };
        this.props.getSpotPrice(serviceBody);
    }

    onChangeLocation(location) {
        if (this.state.location != location) {
            this.setState({
                location: location,
            });
            this.getSpotDetails();
        }
    }

    renderItem(spotDetails) {
        console.log(spotDetails);
        const { Commodity, DeliveryCenter, RateRupees, RateDollar } = spotDetails.item;
        return (

            <View style={[styles.dataView, { backgroundColor: '#171717' }]}>
                <View style={[styles.headerTabs, { flex: 1.2, borderRightWidth: 1, borderRightColor: '#2e2e2e' }]}>
                    <Text style={[styles.dataText, { marginLeft: moderateScale(10), paddingRight: moderateScale(3) }]}>{Commodity}</Text>
                </View>

                <View style={[styles.headerTabs, { flex: 0.8, borderRightWidth: 1, borderRightColor: '#2e2e2e' }]}>
                    <Text style={[styles.dataText, { marginLeft: moderateScale(10), }]}>{DeliveryCenter}</Text>
                </View>

                <View style={[styles.headerTabs, { alignItems: 'flex-end', flex: 1, borderRightWidth: 1, borderRightColor: '#2e2e2e' }]}>
                    <Text style={[styles.dataText, { marginRight: scale(10) }]}>{common.addDecimals(RateRupees)}</Text>
                </View>

                <View style={[styles.headerTabs, { alignItems: 'flex-end', flex: 1 }]}>
                    <Text style={[styles.dataText, { marginRight: scale(10), }]}>{common.addDecimals(RateDollar)}</Text>
                </View>
            </View>

        );
    }

    refreshData() {
        this.getSpotDetails();
        this.setState({
            isRefreshing: false
        });
    }

    renderList() {
        if (this.props.spotDetails != null) {
            if (this.props.errorSpot == '' && this.props.spotDetails.length < 1 && !this.props.loading) {
                return (
                    <View style={{ alignItems: 'center', margin: moderateScale(20) }}>
                        <Text style={{ color: 'white', fontSize: moderateScale(15) }}>No Spot Data Found For This Location</Text>
                    </View>
                );
            } else if (this.props.loading) {
                return null;
            }
            else {
                return (
                    <View>
                        {/* Header */}
                        <View style={[styles.header, { backgroundColor: '#4d4d4d' }]}>

                            <View style={[styles.headerTabs, { flex: 1.2, borderRightWidth: 1, borderRightColor: '#646464' }]}>
                                <Text style={[styles.headerText, { marginLeft: moderateScale(10) }]}>Commodity</Text>
                            </View>

                            <View style={[styles.headerTabs, { flex: 0.8, borderRightWidth: 1, borderRightColor: '#646464' }]}>
                                <Text style={[styles.headerText, { marginLeft: moderateScale(10) }]}>Location</Text>
                            </View>

                            <View style={[styles.headerTabs, { alignItems: 'flex-end', flex: 1, borderRightWidth: 1, borderRightColor: '#646464' }]}>
                                <Text style={[styles.headerText, { marginRight: scale(5) }]}>Spot{"\n"}Price(&#8377;)</Text>
                            </View>

                            <View style={[styles.headerTabs, { alignItems: 'flex-end', flex: 1 }]}>
                                <Text style={[styles.headerText, { marginRight: scale(5) }]}>Spot{"\n"}Price($)</Text>
                            </View>

                        </View>

                        {/* Data */}
                        <FlatList
                            data={this.props.spotDetails}
                            renderItem={(spotDetails) => this.renderItem(spotDetails)}
                            keyExtractor={(index) => JSON.stringify(index)}
                            removeClippedSubviews={false}
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.isRefreshing}
                                    onRefresh={() => this.refreshData()}
                                    title="Pull to refresh"
                                    tintColor="#0000"
                                    progressBackgroundColor="transparent"
                                    titleColor="#0000"
                                    colors={['#97f8cc']}
                                />
                            }
                        />
                    </View>
                );
            }
        } else if (this.props.errorSpot != '') {
            return (
                <View style={{ alignItems: 'center', margin: moderateScale(20) }}>
                    <Text style={{ color: 'white', fontSize: moderateScale(15) }}>{this.props.errorSpot}</Text>
                </View>
            );
        } else {
            return null;
        }
    }

    render() {
        const { height, width } = Dimensions.get('window');

        return (
            <View style={styles.container}>

                <Loader loading={this.props.loading} />


                {/* Dropdown */}
                <View style={{ flexDirection: 'row', marginBottom: scale(25), height: moderateScale(41) }}>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ fontSize: moderateScale(13), fontStyle: 'italic', color: '#898989' }}>Location</Text>
                    </View>

                    <View style={{ flex: 2.5, borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-around', alignItems: 'center' }}>
                        <Dropdown
                            // ref={this.codeRef}
                            value={this.state.location}
                            textColor={'white'}
                            onChangeText={(location) => this.onChangeLocation(location)}
                            containerStyle={{ width: width * 0.45, alignSelf: 'center', marginBottom: moderateScale(15) }}
                            data={this.state.locationData}
                            // propsExtractor={({ props }, index) => props}
                            fontSize={moderateScale(14)}
                            itemTextStyle={{ fontSize: moderateScale(13) }}
                            pickerStyle={{ backgroundColor: '#000000', borderRadius: moderateScale(5) }}
                            itemColor={'#727272'}
                            itemCount={6}
                            IconDetails={{ name: 'angle-down', size: moderateScale(20), color: '#777777' }}
                            dropdownPosition={-1.5 - this.state.locationData.length}
                            dropdownMargins={{ min: 15, max: 15 }}
                        />
                    </View>

                    <View style={{ flex: 1 }}>
                        {/* <Text style={{ fontFamily: 'italic',color:'#898989' }}>Location</Text> */}
                    </View>
                </View>

                {this.renderList()}


            </View>

        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: scale(15),
        marginRight: scale(15)
    },
    header: {
        // flex: 1,
        flexDirection: 'row',
        height: verticalScale(43),
        // marginLeft: scale(15),
        // marginRight: scale(15),
        // marginTop: verticalScale(15),
        marginBottom: verticalScale(10),
        borderRadius: moderateScale(5),
        borderWidth: 1,
        borderColor: '#59595a'
    },
    headerTabs: {
        justifyContent: 'center',
    },
    headerText: {
        color: 'white',
        fontSize: moderateScale(14),
        //   fontWeight: '500'
    },
    dataText: {
        color: 'white',
        fontSize: moderateScale(11),
        //   fontWeight: '500'
    },
    dataView: {
        // flex: 1,
        flexDirection: 'row',
        height: verticalScale(55),
        // marginLeft: scale(20),
        // marginRight: scale(20),
        // marginTop: verticalScale(15),
        marginBottom: verticalScale(10),
        borderRadius: moderateScale(5),
    },

});

const mapStateToProps = state => {
    console.log(state)
    const { spotDetails, errorSpot, loading } = state.commDetails
    return { spotDetails, errorSpot, loading }
}

export default connect(mapStateToProps, { getSpotPrice })(Spot);
