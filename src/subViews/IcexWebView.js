import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, Dimensions, UIManager, LayoutAnimation } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { WebView } from "react-native-webview";
import Icon from "react-native-vector-icons/FontAwesome";
import Header from '../components/Header';

export default class IcexWebView extends Component {

    render() {

        return (
            <View style={styles.container}>
            {/* <Header navigation={this.props.navigation} backBtn={true} /> */}
            <View style={{width:'100%',backgroundColor:'black' ,height:moderateScale(30) ,justifyContent:'center', alignItems:'flex-start'}}>
            <TouchableOpacity
                    onPress={() => { this.props.navigation.goBack() }}
                    style={styles.iconPadding}
                >
                    <Icon name='angle-left'
                        size={moderateScale(20)}
                        style={{ color: 'white'}}
                        light
                    />
                </TouchableOpacity>
            </View>
                <WebView
                    source={{ uri: 'https://www.icexindia.com/' }}
                />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    iconPadding: {
        paddingTop: moderateScale(5),
        paddingBottom: moderateScale(5),
        paddingRight: moderateScale(5),
        marginLeft:moderateScale(20)
    },
});