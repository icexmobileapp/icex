import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, Dimensions, UIManager, LayoutAnimation } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { VictoryChart, VictoryCursorContainer, VictoryLine, VictoryArea, VictoryVoronoiContainer, VictoryTooltip, VictoryAxis } from "victory-native";
import Icon from "react-native-vector-icons/FontAwesome";
import Orientation from 'react-native-orientation';
import common from '../common';
import { setChartData } from '../actions';
import { connect } from 'react-redux';

const { height, width } = Dimensions.get('window');
// console.log(Dimensions.get('window'))
iconSize = (height + width) / 55;
middle = '';
class ZoomChart extends Component {

    state = {
        chartData: this.props.navigation.getParam('chartData'),
        commodity: this.props.navigation.getParam('commodity'),
        rawChartsDataArr: this.props.navigation.getParam('rawChartsDataArr'),

    };

    componentWillReceiveProps(nextProps) {
        console.log("componentWillReceiveProps", nextProps);
    }

    _onOrientationDidChange = (orientation) => {
        console.log(orientation)
        if (orientation == 'PORTRAIT') {
            Orientation.lockToLandscapeLeft();
        }
    };

    componentDidMount() {
        this.props.setChartData(null);
        console.log("componentDidMount", this.props)
        console.log("chartData", this.state.chartData)
        Orientation.lockToLandscapeLeft();
        Orientation.addOrientationListener(this._onOrientationDidChange);
        var arr = this.state.chartData;
        // console.log("arr", arr)
        // console.log("arr[0]", arr[0])
        var arrLength = arr.length - 1;
        // console.log("arr[last]", arr[arrLength]);
        // console.log("Math.floor((arr.length - 1) / 2)", Math.floor((arr.length - 1) / 2))
        var middle = arr[Math.floor((arr.length - 1) / 2)];
        var temp = (arr[0].x.getTime());
        // console.log('temp', temp);
        var temp1 = (arr[arrLength].x.getTime());
        // console.log('temp1', temp1);
        var avg = Math.floor((temp + temp1) / 2);
        // console.log('avg', avg);
        this.middle = avg;
        // console.log('middle', this.middle.getTime());
    }

    componentWillUnmount() {
        this.props.setChartData(null);
        Orientation.lockToPortrait();
        Orientation.removeOrientationListener(this._onOrientationDidChange);
    }

    goBack() {
        console.log("click", this.props);
        this.props.navigation.goBack()
    }

    formatAMPM(date, display) {
        if (date == null || date == undefined) {
            return '';
        } else {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            if (display == 'data') {
                var strTime = hours + ':' + minutes + ' ' + ampm;
                return strTime;
            }
            var strTime = date.getHours() + ':' + minutes;
            return strTime;
        }
    }

    showChartData(d, commodityIndex) {
        // this.setState({
        //   chartValue: d.y
        // });
        var priceChange = '';
        var percentChange = '';
        this.state.rawChartsDataArr[commodityIndex].find(x => {
            if (x.LastTradedPrice == d.y) {
                priceChange = x.NetPriceChangeFromClosingPrice;
                percentChange = x.Change;
            }
        });
        if (d != null) {
            const obj = {
                x: this.formatAMPM(d.x, 'data'),
                y: d.y,
                priceChange,
                percentChange
            }
            if (this.props.chartData != null) {
                if (String(this.props.chartData.x) != String(obj.x)) {
                    this.props.setChartData(obj);
                }

            } else {
                this.props.setChartData(obj);
            }
        }


    }

    priceLabelToDisplay(price, domainMin) {
        if(Number(domainMin) < 10000){
          return common.addOnlyDecimals(price);
        }
        return Math.round(price);
      }

    dateTimeToDisplay(dateObj) {
        var date = dateObj.getDate();
        var month = common.getMonthName(dateObj.getMonth()).substring(0, 3);
        var time = this.formatAMPM(dateObj, 'data');
        return `${date} ${month}, ${time}`
    }

    render() {

        // const { navigation } = this.props;
        // const chartData = navigation.getParam('chartData');
        // const commodity = navigation.getParam('commodity');
        console.log("commodity", this.state.commodity)
        console.log("chartData", this.state.chartData)

        var domainMinMax = common.findMinMax(this.state.chartData, 'y');
        if (this.state.chartData.length > 1) {
            var bgColor = '#4d4d4d';
            var iconName = '';
            var axisLabelPadding = 0;
            if (this.props.chartData != null) {
                if (Number(this.props.chartData.priceChange) > 0) {
                    bgColor = '#01800f';
                    iconName = 'long-arrow-up';
                } else if (Number(this.props.chartData.priceChange) < 0) {
                    bgColor = '#de0808';
                    iconName = 'long-arrow-down';
                }
            }
            if (String(domainMinMax.max).length <= 4) {
                axisLabelPadding = (String(domainMinMax.max).length - 2) * 10;
              }else{
                axisLabelPadding = (String(domainMinMax.max).length - 4) * 10;
              }
            return (
                <View style={styles.container}>
                    {/* <Header navigation={this.props.navigation} backBtn={true} /> */}
                    <View style={{ width: '100%', backgroundColor: 'black', height: moderateScale(30), flexDirection:'row',justifyContent: 'flex-start', alignItems: 'center' }}>
                        <TouchableOpacity
                            onPress={() => { this.goBack() }}
                            style={styles.iconPadding}
                        >
                            <Icon name='angle-left'
                                size={moderateScale(30)}
                                style={{ color: 'white' }}
                                light
                            />
                        </TouchableOpacity>
                        <Text style={{ color: 'white', fontSize: moderateScale(18), paddingLeft: moderateScale(30) }}>Commodity: {this.state.commodity.item.Underlying}</Text>
                        <Text style={{ color: 'white', fontSize: moderateScale(18), paddingLeft: moderateScale(30) }}>Expiry: {common.expiryDateToDisplay(this.state.commodity.item.ExpiryDate)}</Text>
                    </View>
                    {/* <View style={styles.cursorDetailsContainer}>
                        <View style={{ flexDirection: 'row', flex: 1.2, justifyContent: 'center' }}>
                            {this.props.chartData == null ?
                                <Text style={[styles.headerText, { alignSelf: 'center' }]}></Text> :
                                <Text style={[styles.headerText, { alignSelf: 'center' }]}>{this.props.chartData.x}</Text>
                            }

                        </View>
                        <View style={{ flexDirection: 'row', flex: 1, width: '50%' }}>
                            <View style={{
                                flexDirection: 'row',
                                backgroundColor: bgColor, marginLeft: scale(20), flex: 1,
                                marginTop: verticalScale(10),
                                marginBottom: verticalScale(10),
                                justifyContent: 'space-evenly',
                            }}>
                                {iconName != '' ?
                                    <Icon name={iconName}
                                        size={iconSize * 0.7}
                                        style={styles.cursorDetailsText}
                                        light
                                    /> : null
                                }

                                {this.props.chartData == null ?
                                    <Text style={[styles.headerText, { alignSelf: 'center' }]}></Text> :
                                    <Text style={[styles.headerText, { alignSelf: 'center' }]}>{this.props.chartData.y}</Text>
                                }
                            </View>
                            <View style={{
                                backgroundColor: '#1f1f1f', marginRight: scale(20), flex: 1,
                                width: '90%',
                                marginTop: verticalScale(10),
                                marginBottom: verticalScale(10),
                                // borderRadius: moderateScale(5),
                                justifyContent: 'center',

                            }}>
                                {this.props.chartData == null ?
                                    <Text style={styles.cursorDetailsText}></Text> :
                                    <Text style={styles.cursorDetailsText}>{common.addDecimals(this.props.chartData.priceChange)}({this.props.chartData.percentChange}%)</Text>
                                }
                            </View>
                        </View>
                    </View> */}
                    <View style={{ marginTop: 0 }}>
                        <VictoryChart width={height - moderateScale(20 + Number(axisLabelPadding))} height={width - moderateScale(60)}
                            domain={{ y: [domainMinMax.min, domainMinMax.max] }}
                            domainPadding={{ y: 10 }}
                            padding={{ left: 60 + moderateScale(Number(axisLabelPadding)), top: 50, right: 40, bottom: 50 }}
                            theme={theme}
                            scale={{ x: "time", y: "linear" }}
                            // containerComponent={
                            //     <VictoryCursorContainer
                            //         style={{ stroke: 'white' }}
                            //         cursorDimension="x"
                            //         onCursorChange={(d) => {
                            // console.log(d)
                            // var str=String(d).substr(17,8);
                            // console.log(chartData.find(obj => {
                            //   console.log("54"+obj.x)
                            //   var x = String(obj.x).substr(0,22) == String(d).substr(0,22);
                            // console.log("X:"+x)}))

                            // this.setState({
                            //   chartTime:str,
                            //   chartValue:val
                            // });
                            //         }}
                            //     />
                            // }

                            containerComponent={
                                <VictoryVoronoiContainer
                                    theme={theme}
                                    voronoiDimension="x"
                                    labels={(d) => `${this.dateTimeToDisplay(d.x)}\n₹ ${common.addDecimals(d.y)}`}
                                    // labels={(d) => this.state.commodity.item.Underlying + `\n₹ ${common.addDecimals(d.y)}`}
                                    // labels={(d) => this.showChartData(d, this.state.commodity.index)}
                                    labelComponent={
                                        <VictoryTooltip
                                            theme={theme}
                                            cornerRadius={5}
                                            flyoutStyle={{ paddingLeft: moderateScale(20) }}
                                            height={moderateScale(40)}
                                            width={moderateScale(190)}
                                            // pointerWidth={6}
                                            // paddingLeft={10}
                                            horizontal={false}
                                            orientation={(d) => d.x.getTime() > this.middle ? 'left' : 'right'}
                                            renderInPortal={false}
                                            style={{ overflow: 'visible', fontSize: moderateScale(14) }}
                                        />}
                                />
                            }
                        >
                            <VictoryAxis
                                fixLabelOverlap={true}
                                tickFormat={(x) => this.formatAMPM(x, 'chart')}
                                label="Time"
                                tickCount={5}
                                style={{
                                    axisLabel: { padding: 30 }
                                }}
                            />
                            <VictoryAxis dependentAxis
                            tickCount={5}
                                tickFormat={(y) => this.priceLabelToDisplay(Math.round(y * 100) / 100, domainMinMax.min)}
                                style={{
                                    axisLabel: { padding: 50 }
                                }}
                            />
                            <VictoryArea
                                data={this.state.chartData}
                                style={{
                                    data: {
                                        stroke: "#c2c3fc",
                                        fill: '#1d1718',
                                        // strokeWidth: (d, active) => { return active ? 4 : 2; }
                                    },
                                    labels: { fill: "tomato" }
                                }}
                                animate={{
                                    // duration: 2000,
                                    onLoad: { duration: 1000 },
                                    // easing : 'back'
                                }}
                            />
                        </VictoryChart>

                    </View>
                </View>
            );
        }
        return null;
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black'
    },
    iconPadding: {
        paddingTop: moderateScale(15),
        paddingBottom: moderateScale(15),
        paddingRight: moderateScale(15),
        marginLeft: moderateScale(20)
    },
    cursorDetailsContainer: {
        flexDirection: 'row',
        paddingTop: verticalScale(10),
        borderBottomWidth: 1,
        borderColor: '#1f1f1f',
        backgroundColor: '#0b0b0b',
        zIndex: 10000
    },
    cursorDetailsText: {
        color: 'white',
        fontSize: moderateScale(12),
        alignSelf: 'center'
    },
    headerText: {
        color: 'white',
        fontSize: moderateScale(14),
        //   fontWeight: '500'
    },
});

const charcoal = "#252525";
const grey = "#969696";

// Typography
const sansSerif =
    "'Gill Sans', 'Gill Sans MT', 'Ser­avek', 'Trebuchet MS', sans-serif";
const letterSpacing = "normal";
const fontSize = 14;

// Layout
const baseProps = {
    width: 450,
    height: 300,
    padding: 50,
    colorScale: colors
};

const colors = [
    "#252525",
    "#525252",
    "#737373",
    "#969696",
    "#bdbdbd",
    "#d9d9d9",
    "#f0f0f0"
];
// Labels
const baseLabelStyles = {
    fontFamily: sansSerif,
    fontSize,
    letterSpacing,
    padding: 10,
    fill: '#fff',
    stroke: "transparent"
};
const centeredLabelStyles = Object.assign({ textAnchor: "middle" }, baseLabelStyles);

// Strokes
const strokeLinecap = "round";
const strokeLinejoin = "round";
const theme = {
    area: Object.assign({
        style: {
            data: {
                fill: charcoal
            },
            labels: centeredLabelStyles
        }
    }, baseProps),
    axis: Object.assign({
        style: {
            axis: {
                fill: "transparent",
                stroke: charcoal,
                strokeWidth: 1,
                strokeLinecap,
                strokeLinejoin
            },
            axisLabel: Object.assign({}, centeredLabelStyles, {
                padding: 25
            }),
            grid: {
                fill: "none",
                stroke: "none",
                pointerEvents: "painted"
            },
            ticks: {
                fill: "transparent",
                size: 1,
                stroke: "transparent"
            },
            tickLabels: baseLabelStyles
        }
    }, baseProps),
    bar: Object.assign({
        style: {
            data: {
                fill: charcoal,
                padding: 8,
                strokeWidth: 0
            },
            labels: baseLabelStyles
        }
    }, baseProps),
    boxplot: Object.assign({
        style: {
            max: { padding: 8, stroke: charcoal, strokeWidth: 1 },
            maxLabels: baseLabelStyles,
            median: { padding: 8, stroke: charcoal, strokeWidth: 1 },
            medianLabels: baseLabelStyles,
            min: { padding: 8, stroke: charcoal, strokeWidth: 1 },
            minLabels: baseLabelStyles,
            q1: { padding: 8, fill: grey },
            q1Labels: baseLabelStyles,
            q3: { padding: 8, fill: grey },
            q3Labels: baseLabelStyles
        },
        boxWidth: 20
    }, baseProps),
    candlestick: Object.assign({
        style: {
            data: {
                stroke: charcoal,
                strokeWidth: 1
            },
            labels: centeredLabelStyles
        },
        candleColors: {
            positive: "#ffffff",
            negative: charcoal
        }
    }, baseProps),
    chart: baseProps,
    errorbar: Object.assign({
        borderWidth: 8,
        style: {
            data: {
                fill: "transparent",
                stroke: charcoal,
                strokeWidth: 2
            },
            labels: centeredLabelStyles
        }
    }, baseProps),
    group: Object.assign({
        colorScale: colors
    }, baseProps),
    legend: {
        colorScale: colors,
        gutter: 10,
        orientation: "vertical",
        titleOrientation: "top",
        style: {
            data: {
                type: "circle"
            },
            labels: baseLabelStyles,
            title: Object.assign({}, baseLabelStyles, { padding: 5 })
        }
    },
    line: Object.assign({
        style: {
            data: {
                fill: "transparent",
                stroke: 'charcoal',
                strokeWidth: 2
            },
            labels: centeredLabelStyles
        }
    }, baseProps),
    pie: {
        style: {
            data: {
                padding: 10,
                stroke: "transparent",
                strokeWidth: 1
            },
            labels: Object.assign({}, baseLabelStyles, { padding: 20 })
        },
        colorScale: colors,
        width: 400,
        height: 400,
        padding: 50
    },
    scatter: Object.assign({
        style: {
            data: {
                fill: charcoal,
                stroke: "transparent",
                strokeWidth: 0
            },
            labels: centeredLabelStyles
        }
    }, baseProps),
    stack: Object.assign({
        colorScale: colors
    }, baseProps),
    tooltip: {
        style: {
            fontFamily: sansSerif,
            fontSize,
            letterSpacing,
            padding: 10,
            fill: '#000',
            stroke: "transparent",
            padding: 5,
            pointerEvents: "none"
        },
        flyoutStyle: {
            stroke: charcoal,
            strokeWidth: 1,
            fill: "#f0f0f0",
            pointerEvents: "none"
        },
        cornerRadius: 5,
        pointerLength: 10
    },
    voronoi: Object.assign({
        style: {
            data: {
                fill: "transparent",
                stroke: "transparent",
                strokeWidth: 0
            },
            labels: Object.assign({}, centeredLabelStyles, { padding: 5, pointerEvents: "none" }),
            flyout: {
                stroke: charcoal,
                strokeWidth: 1,
                fill: "#f0f0f0",
                pointerEvents: "none"
            }
        }
    }, baseProps)
};


const mapStateToProps = state => {
    const { loading, commodityData, watchListArr, chartData } = state.commDetails;
    const { user } = state.auth;
    return { loading, commodityData, user, watchListArr, chartData }
}

export default connect(mapStateToProps, { setChartData })(ZoomChart);
