import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, Dimensions, UIManager, LayoutAnimation, Image } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Item, Input, Label, Form, Toast } from 'native-base';
import SLIcon from "react-native-vector-icons/SimpleLineIcons";
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux';
import { openEditProfile, closeEditProfile, updateUser } from '../actions';
import common from '../common';
import Header from '../components/Header';
import Loader from "../components/Loader";
import CountryPicker from "../components/CountryPicker";
import data from '../components/Countries';
import ImagePicker from 'react-native-image-picker';

const { height, width } = Dimensions.get('window');
// console.log(Dimensions.get('window'))
iconSize = (height + width) / 55;

// const defaultFlag = data.filter(
//     obj => obj.dial_code === this.props.user.CountryCode
// )[0].flag

class EditProfile extends Component {

    constructor(props) {
        super(props);

        if (props.user.CountryCode != null && props.user.CountryCode.trim() != '' && props.user.CountryCode != undefined) {
            this.defaultCC = props.user.CountryCode;
            this.defaultFlag = data.filter(
                obj => obj.dial_code === props.user.CountryCode
            )[0].flag
        } else {
            this.defaultFlag = '🇮🇳';
            this.defaultCC = '+91';
        }


        this.state = {
            seconds: 60,
            time: {},
            timer: 0,
            ImageSource: this.props.user.ProfileImage,
            username: this.props.user.Name,
            emailId: this.props.user.Email,
            mobileNo: this.props.user.Mobile,
            pincode: this.props.user.Location,
            prevUsername: this.props.user.Name,
            prevMobileNo: this.props.user.Mobile,
            prevPincode: this.props.user.Location,
            otp: '',
            systemGeneratedOtp: '',
            showOTPBox: false,
            loading: false,
            isMobileChecked: true,
            isPincodeChecked: true,
            isPincodeValid: true,
            otpErr: '',
            nameErr: '',
            mobileErr: '',
            locationErr: '',
            updateMsg: '',
            reqSource: '',
            arrMobileNo: [{ mobNo: this.props.user.Mobile, otp: '', isNewMobNo: false }],
            isCCPickerOpen: false,
            countryCode: this.defaultCC,
            countryFlag: this.defaultFlag,
        }

    }

    componentWillMount() {
        console.log('componentWillMount', this.props.user.ProfileImage)
        if (this.props.user.ProfileImage != '' && this.props.user.ProfileImage != null) {
            let source = { uri: this.props.user.ProfileImage };

            this.setState({

                ImageSource: source

            });
        }
        console.log('Imagesource', this.state.ImageSource)
    }

    selectPhotoTapped() {

        const options = {
            quality: 0.5,
            maxWidth: 500,
            maxHeight: 500,
            // storageOptions: {
            //     skipBackup: true
            // }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                // let source = { uri: response.uri };

                // You can also display the image using data:
                let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    reqSource: response.data,
                    ImageSource: source

                });
                console.log('user', response.data);
            }
        });
    }

    showPhoto() {
        if (this.state.ImageSource != '' && this.state.ImageSource != null) {
            return (
                <View style={{ height: moderateScale(120), width: moderateScale(115) }}>
                    <Image style={{ height: moderateScale(120), width: moderateScale(115), }} source={this.state.ImageSource} />
                </View>
            );
        } else {
            <View style={{ height: moderateScale(120), width: moderateScale(115), marginLeft: moderateScale(20) }}>
            </View>
        }

    }

    resetErrors() {
        this.setState({
            nameErr: '',
            mobileErr: '',
            locationErr: '',
            updateMsg: '',
            otpErr: '',
        });
    }

    sendOtp() {
        if (this.state.systemGeneratedOtp == '') {
            const generatedOtp = Math.floor(100000 + Math.random() * 900000);
            this.setState({
                systemGeneratedOtp: generatedOtp,
            });
        }
        var _this = this;
        setTimeout(function () {

            console.log('Otp :' + _this.state.systemGeneratedOtp);

            const request = {
                Mobile: _this.state.mobileNo,
                OTP: _this.state.systemGeneratedOtp,
                CountryCode: _this.state.countryCode
            };
            console.log(request);

            const serviceRequest = common.encryptData(JSON.stringify(request));

            const serviceBody = { request: serviceRequest };
            // _this.props.sendOtp(serviceBody);

            common.callService('UserRegistrationSendOTP_v2', serviceBody)
                .then(otpDetails => {
                    console.log(otpDetails);
                    if (otpDetails.ReturnCode == '1') {
                        _this.startTimer()
                        _this.setState({
                            showOTPBox: true,
                            loading: false,
                            seconds: 60
                        });
                    }
                    else {
                        _this.setState({
                            loading: false,
                            otpErr: otpDetails.ReturnMsg
                        });

                    }
                })
                .catch(err => {
                    console.log(err);
                    _this.setState({
                        loading: false,
                        otpErr: 'Could not connect to server'
                    });

                })

        }, 20);
    }

    checkMobile() {
        console.log(this.state.mobileNo.length)
        if ((this.state.mobileNo != this.props.user.Mobile || this.props.user.CountryCode != this.state.countryCode) && common.validateMobileNo(this.state.mobileNo.trim(), this.state.countryCode)) {
            console.log('mobileNoChanged');
            var req = {
                Mobile: this.state.mobileNo.trim(),
                CountryCode: this.state.countryCode
            };
            console.log('req', req);
            var serviceRequest = common.encryptData(JSON.stringify(req))
            var serviceBody = { request: serviceRequest };
            this.setState({
                loading: true,
                showOTPBox: false
            });
            // console.log('serviceBody', serviceBody);
            common.callService('CheckUserMobile_v2', serviceBody)
                .then(CheckUserMobile => {
                    console.log(CheckUserMobile);
                    if (CheckUserMobile.ReturnCode == '1') {
                        this.setState({
                            loading: false,
                            mobileErr: 'Mobile number already exists',
                            isMobileChecked: true,
                        });
                    } else if (CheckUserMobile.ReturnCode == '0') {
                        this.setState({
                            isMobileChecked: true,
                        });
                        this.sendOtp();
                    }
                })
                .catch(err => {
                    this.setState({
                        loading: false,
                        mobileErr: 'Could not connect to server'
                    });
                    console.log(err);

                });
        } else {
            this.setState({
                showOTPBox: false,
            });
        }
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        LayoutAnimation.easeInEaseOut();

    }

    editProfile() {
        console.log(this.state)
        this.resetErrors();
        var proceed = true;
        if (this.state.username.trim() == '') {
            this.setState({
                nameErr: 'Please enter name',
            });
            proceed = false;
        }

        if (this.state.mobileNo.trim() == '') {
            this.setState({
                mobileErr: 'Please enter mobile number',
            });
            proceed = false;
        } else if (!common.validateMobileNo(this.state.mobileNo.trim(), this.state.countryCode)) {
            this.setState({
                mobileErr: 'Please enter valid mobile number',
            });
            proceed = false;
        }

        if (this.state.pincode.trim() == '') {
            this.setState({
                locationErr: 'Please enter your pincode',
            });
            proceed = false;
        } else if (this.state.pincode.trim().length < 6) {
            this.setState({
                locationErr: 'Please enter valid 6 digit pincode',
            });
            proceed = false;
        } else if (this.state.countryCode != '+91' && this.state.pincode != '000000') {
            this.setState({
                locationErr: 'For countries other than India, enter 000000 as Pincode',
            });
            proceed = false;
        } else if (!this.state.isPincodeValid || (this.state.countryCode == '+91' && this.state.pincode == '000000')) {
            this.setState({
                locationErr: 'Please enter valid pincode',
            });
            proceed = false;
        }
        

        if (this.state.mobileNo == this.props.user.Mobile && this.props.user.CountryCode == this.state.countryCode) {
            this.setState({ showOTPBox: false })
        }

        if (this.state.showOTPBox) {
            if (this.state.otp.trim() == '') {
                // console.log("if")
                this.setState({
                    otpErr: 'Please enter OTP',
                });
                proceed = false;
            } else if (this.state.otp.trim().length < 6) {
                // console.log("else if")
                this.setState({
                    otpErr: 'Please enter valid 6 digit OTP',
                });
                proceed = false;
            }
            else if (this.state.otp != this.state.systemGeneratedOtp) {
                // console.log("else2")
                this.setState({
                    otpErr: 'Entered OTP is incorrect',
                });
                proceed = false;
            }
        }

        if (this.state.username == this.props.user.Name && this.state.mobileNo == this.props.user.Mobile &&
            this.state.pincode == this.props.user.Location && this.state.reqSource == '' && this.props.user.CountryCode == this.state.countryCode) {
            proceed = false;
            Toast.show({
                text: "Details updated successfully",
                type: "success",
                // buttonText: 'Okay'
                // duration: 5000,
            });
            setTimeout(() => {
                this.props.navigation.goBack();

            }, 2000)

        }

        if (proceed) {

            if ((this.state.mobileNo != this.props.user.Mobile || this.props.user.CountryCode != this.state.countryCode) && !this.state.isMobileChecked) {
                this.checkMobile();
            } else {
                this.updateDetails();
            }

        }
    }

    updateDetails() {
        const req = new UpdateUserDetail();
        req.Id = this.props.user.Id;
        req.Name = this.state.username;
        req.Location = this.state.pincode;
        req.CountryCode = this.state.countryCode;
        req.Mobile = this.state.mobileNo;
        if (this.state.reqSource != '') {
            req.ProfileImage = this.state.reqSource;
        }

        console.log((JSON.stringify(req)));

        var serviceRequest = common.encryptData(JSON.stringify(req))
        this.setState({
            loading: true,

        });
        var serviceBody = { request: serviceRequest };
        common.callService('UpdateUserDetail_v2', serviceBody)
            .then(UpdateUserDetail => {
                console.log(UpdateUserDetail);
                if (UpdateUserDetail.ReturnCode == '1') {
                    this.setState({
                        loading: false,
                    });
                    Toast.show({
                        text: "Details updated successfully",
                        type: "success",
                        // buttonText: 'Okay'
                        // duration: 5000,
                    });
                    this.props.updateUser(UpdateUserDetail.Data);
                    setTimeout(() => {
                        this.props.navigation.goBack();

                    }, 1000)
                } else {
                    this.setState({
                        loading: false
                    });
                    setTimeout(() => {
                        Toast.show({
                            text: UpdateUserDetail.ReturnMsg,
                            type: "danger"
                        });
                    }, 10)
                }

            })
            .catch(err => {
                console.log(err);
                this.setState({
                    showOTPBox: false,
                    loading: false,
                });
                setTimeout(() => {
                    Toast.show({
                        text: "Sorry, Unable to update your details. Please try again later",
                        type: "danger"
                    });
                }, 10)
            });
    }

    resendOtp() {
        // clearInterval(this.state.timer);
        this.setState({
            seconds: 60,
            timer: 0,
            // otp: '',
            // otpErr: '',
            systemGeneratedOtp: ''
        });
        setTimeout(() => {
            this.sendOtp();
        }, 10)

    }

    startTimer() {
        if (this.state.timer == 0 && this.state.seconds > 0) {
            // setTimeout(() => {
            this.state.timer = setInterval(() => {
                this.countDown()
            }, 1000);
            // }, 5);

        }
    }

    countDown() {
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1;
        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });
        // Check if we're at zero.
        if (seconds == 0 || seconds < 0) {
            clearInterval(this.state.timer);
        }
    }

    secondsToTime(secs) {
        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = String(Math.ceil(divisor_for_seconds)).padStart(2, '0')

        let obj = {
            'h': hours,
            'm': minutes,
            's': seconds
        };
        return obj;
    }

    showTimer() {
        if (this.state.seconds == 0) {
            return (
                <TouchableOpacity
                    style={{ alignSelf: 'flex-end' }}
                    onPress={() => this.resendOtp()}>
                    <Text
                        style={{ color: 'white', fontSize: moderateScale(12) }}
                    >RESEND OTP</Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={{ alignSelf: 'flex-end' }}>
                    <Text style={{ color: 'white', fontSize: moderateScale(12) }}
                    >00 : {this.state.time.s} </Text>
                </View>
            )
        }

    }

    renderOTPTextBox() {
        if (this.state.showOTPBox) {

            return (
                <View>
                    <Item floatingLabel style={[styles.placeholderText, { marginTop: moderateScale(20) }]}>
                        <Label style={styles.labelStyle} >Enter OTP sent on above mobile no.</Label>
                        <Input
                            keyboardType='numeric'
                            style={styles.inputText}
                            value={this.state.otp}
                            maxLength={6}
                            onChangeText={(text) => { this.setState({ otp: common.numberOnly(text) }) }}
                        />
                    </Item>
                    {this.showTimer()}
                    {!this.state.otpErr == '' ?
                        <Text style={{
                            color: '#e64545',
                            fontSize: moderateScale(13),
                            marginLeft: moderateScale(20),
                            marginTop: moderateScale(15),
                            marginTop: moderateScale(-10)
                        }}>{this.state.otpErr}</Text> : null
                    }
                </View>
            );
        }
        return null;
    }

    pincodeKeyPress() {
        if (this.state.pincode == this.props.user.Location) {
            this.setState({
                loading: false,
                locationErr: '',
                isPincodeChecked: true,
                isPincodeValid: true,
            });
        }
        else if (this.state.pincode.trim().length == 6 && !this.state.isPincodeChecked && this.state.countryCode == '+91') {
            const req = new pincodeDetails();
            req.Pincode = this.state.pincode;

            // console.log('req', req);
            var serviceRequest = common.encryptData(JSON.stringify(req))
            var serviceBody = { request: serviceRequest };
            this.setState({
                loading: true,
                locationErr: ''
            });
            // console.log('serviceBody', serviceBody);
            common.callService('CheckPincode', serviceBody)
                .then(CheckPincode => {
                    console.log(CheckPincode);
                    if (CheckPincode.ReturnCode == '1') {
                        this.setState({
                            loading: false,
                            locationErr: '',
                            isPincodeChecked: true,
                            isPincodeValid: true,
                        });
                    } else {
                        this.setState({
                            loading: false,
                            locationErr: 'Please enter valid pincode',
                            isPincodeChecked: true,
                            isPincodeValid: false,
                        });
                    }
                })
                .catch(err => {
                    this.setState({
                        loading: false,
                        locationErr: 'Unable to verify pincode, Please try again later',
                        isPincodeValid: false,
                    });
                    console.log(err);
                })
        }
    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: '#202020' }}>
                <Header navigation={this.props.navigation} backBtn={true} />
                <Loader loading={this.state.loading} />
                <ScrollView
                keyboardShouldPersistTaps={'handled'}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: moderateScale(20) }}>
                        <Text style={{ marginLeft: moderateScale(20), fontSize: moderateScale(26), color: 'white' }}>Edit Profile</Text>
                    </View>


                    <Form style={{ marginRight: moderateScale(20) }}>
                        <Item floatingLabel style={[styles.placeholderText]}>
                            <Label style={styles.labelStyle} >USERNAME</Label>
                            <Input
                                getRef={(input) => { this.userNameInput = input; }}
                                style={styles.inputText}
                                value={this.state.username}
                                maxLength={30}
                                onChangeText={(text) => { this.setState({ username: common.charOnly(text) }) }}
                            />
                        </Item>
                        <TouchableOpacity
                            style={styles.editBtn}
                            onPress={() => { this.userNameInput._root.focus() }}
                        >
                            <SLIcon name='pencil'
                                size={moderateScale(15)}
                                style={{ color: 'white' }}
                                light
                            />
                        </TouchableOpacity>
                        {!this.state.nameErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.nameErr}</Text> : null
                        }

                        <Item floatingLabel style={[styles.placeholderText, { marginTop: moderateScale(20) }]}>
                            <Label style={styles.labelStyle} >EMAIL ID</Label>
                            <Input
                                getRef={(input) => { this.emailInput = input; }}
                                style={styles.inputText}
                                editable={false}
                                keyboardType='email-address'
                                value={this.state.emailId}
                                onChangeText={(text) => { this.setState({ emailId: text }) }}
                            />
                        </Item>


                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View
                                style={styles.countryCode}
                            >
                                <TouchableOpacity
                                    style={{ flexDirection: 'row', }}
                                    onPress={() => this.setState({ isCCPickerOpen: true })}
                                >
                                    <Text style={styles.inputText}>{this.state.countryFlag} {this.state.countryCode}</Text>
                                    <Icon name='angle-down'
                                        size={moderateScale(15)}
                                        style={{ color: '#fff', marginLeft: moderateScale(5), paddingRight: moderateScale(5) }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <CountryPicker
                                modalVisible={this.state.isCCPickerOpen}
                                isClosed={() => {
                                    console.log('isCloseCalled');
                                    this.setState({ isCCPickerOpen: false })
                                }}
                                countryChanged={(countryCode, countryFlag) => {
                                    console.log('countryChanged', countryCode, countryFlag);
                                    this.setState({ countryCode, countryFlag, isMobileChecked: false })
                                }}
                            />
                            <View style={{ flex: 0.75 }}>
                                <Item floatingLabel style={[styles.placeholderText, { marginTop: moderateScale(0), marginLeft: moderateScale(5) }]}>
                                    <Label style={styles.labelStyle} >MOBILE NUMBER</Label>
                                    <Input
                                        keyboardType='numeric'
                                        getRef={(input) => { this.mobileInput = input; }}
                                        maxLength={this.state.countryCode == '+91' ? 10 : 16}
                                        style={styles.inputText}
                                        value={this.state.mobileNo}
                                        onBlur={() =>
                                            setTimeout(() => {
                                                if (this.state.mobileNo != this.props.user.Mobile || this.props.user.CountryCode != this.state.countryCode) {
                                                    if (!this.state.isMobileChecked)
                                                        this.checkMobile()
                                                }
                                            }, 10)
                                        }
                                        onChangeText={(text) => {
                                            this.setState({ mobileNo: common.numberOnly(text), isMobileChecked: false });
                                        }}
                                    />
                                </Item>
                            </View>
                        </View>

                        {/* <Item floatingLabel style={[styles.placeholderText, { marginTop: moderateScale(0) }]}>
                            <Label style={styles.labelStyle} >MOBILE NUMBER</Label>
                            <Input
                                keyboardType='numeric'
                                getRef={(input) => { this.mobileInput = input; }}
                                maxLength={10}
                                style={styles.inputText}
                                value={this.state.mobileNo}
                                // onBlur={() => this.checkMobile()}
                                // onKeyPress={(text) => { this.keyPress() }}
                                onChangeText={(text) => {
                                    this.setState({ mobileNo: common.numberOnly(text) });
                                    setTimeout(() => { this.keyPress() }, 10)
                                }}
                            />
                        </Item> */}

                        <TouchableOpacity
                            style={styles.editBtn}
                            onPress={() => { this.mobileInput._root.focus() }}
                        >
                            <SLIcon name='pencil'
                                size={moderateScale(15)}
                                style={{ color: 'white' }}
                                light
                            />
                        </TouchableOpacity>
                        {!this.state.mobileErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.mobileErr}</Text> : null
                        }

                        {this.renderOTPTextBox()}

                        <Item floatingLabel style={[styles.placeholderText, { marginTop: moderateScale(20) }]}>
                            <Label style={styles.labelStyle}>PINCODE</Label>
                            <Input
                                getRef={(input) => { this.pincodeInput = input; }}
                                style={styles.inputText}
                                value={this.state.pincode}
                                keyboardType='numeric'
                                maxLength={6}
                                onChangeText={(text) => {
                                    this.setState({ pincode: common.numberOnly(text), isPincodeChecked: false });
                                    setTimeout(() => { this.pincodeKeyPress() }, 10)
                                }}
                            />
                        </Item>
                        <TouchableOpacity
                            style={styles.editBtn}
                            onPress={() => { this.pincodeInput._root.focus() }}
                        >
                            <SLIcon name='pencil'
                                size={moderateScale(15)}
                                style={{ color: 'white' }}
                                light
                            />
                        </TouchableOpacity>
                        {!this.state.locationErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.locationErr}</Text> : null
                        }
                    </Form>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-end', marginTop: moderateScale(20), marginBottom: moderateScale(20), marginLeft: moderateScale(30) }}>

                        <View style={{ height: moderateScale(120), width: moderateScale(115), backgroundColor: '#2c2c2c', }}>
                            {this.showPhoto()}
                        </View>

                        <TouchableOpacity style={{
                            borderWidth: 1, borderColor: '#4a4eda',
                            borderRadius: moderateScale(5),
                            marginLeft: moderateScale(15),
                            justifyContent: "center",
                            alignItems: 'center',
                            width: moderateScale(110),
                            height: moderateScale(40),
                        }}
                            onPress={this.selectPhotoTapped.bind(this)}
                        >
                            <Text style={{
                                color: 'white',
                                fontFamily: 'Lato',
                                fontSize: moderateScale(14),
                                fontWeight: 'bold'
                            }}>
                                ADD PHOTO
                                </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ alignSelf: 'flex-end', padding: moderateScale(5), marginLeft: moderateScale(20) }}
                            onPress={() => this.editProfile()}
                        >
                            <Text style={{ fontSize: moderateScale(14), color: '#6f73ff' }}>SAVE</Text>
                        </TouchableOpacity>


                    </View>
                    {!this.state.updateMsg == '' ?
                        <Text style={{
                            color: 'red',
                            fontSize: moderateScale(13),
                            marginLeft: moderateScale(20),
                            marginTop: moderateScale(15)
                        }}>{this.state.updateMsg}</Text> : null
                    }
                </ScrollView>

            </View>


        );
    }
}

const styles = StyleSheet.create({
    placeholderText: {
        borderColor: '#393939',
        fontSize: moderateScale(13),
        marginTop: moderateScale(10),
        marginBottom: moderateScale(10)
    },
    labelStyle: {
        color: '#a3a3a3',
        fontSize: moderateScale(12)
    },
    inputText: {
        color: '#d3d3d3',
        fontSize: moderateScale(14)
    },
    errorMsg: {
        color: '#e64545',
        fontSize: moderateScale(13),
        marginLeft: moderateScale(20),
        marginTop: moderateScale(15)
    },
    editBtn: {
        alignSelf: 'flex-end',
        marginTop: moderateScale(-35)
    },
    countryCode: {
        height: moderateScale(35),
        flex: 0.25,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        borderColor: '#393939',
        borderBottomWidth: 0.8,
        justifyContent: 'space-around',
        alignItems: 'center',
        marginBottom: moderateScale(10),
        marginLeft: moderateScale(20),
    }
});


const mapStateToProps = state => {
    // console.log('state', state)
    const { user, loading } = state.auth;
    return { user, loading }
}

export default connect(mapStateToProps, { openEditProfile, closeEditProfile, updateUser })(EditProfile);


class UpdateUserDetail {
    Id;
    Name;
    Location;
    ProfileImage;
    CountryCode;
    Mobile

    constructor() { }
}

class pincodeDetails {
    Pincode;
    constructor() { }
}
