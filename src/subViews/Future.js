import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, FlatList, View, TouchableOpacity, Dimensions, UIManager, LayoutAnimation } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { VictoryChart, VictoryCursorContainer, VictoryArea, VictoryVoronoiContainer, VictoryTooltip, VictoryAxis } from "victory-native";
import Icon from "react-native-vector-icons/FontAwesome";
import SLIcon from "react-native-vector-icons/SimpleLineIcons";
import { connect } from 'react-redux';
import Loader from "../components/Loader";
import common from '../common';
import { getWatchListArr, setChartData, setSelectedExpiryIndex } from '../actions';

const { height, width } = Dimensions.get('window');
// console.log(Dimensions.get('window'))
iconSize = (height + width) / 55;

class Future extends Component {

  state = {
    needToLoadFuture: false,
    chartTime: null,
    chartValue: null,
    expanded: null,
    loading: false,
    commodityData: this.props.commodityData[this.props.selectedCommodityId],
    watchListArr: this.props.watchListArr,
    chartsDataArr: [],
    rawChartsDataArr: []
  }

  componentWillMount() {
    // console.log(this.props.selectedExpiry);
    // this.props.setChartData(null);
    this.loadCharts(true);
  }

  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps');
    let selectedCommodityId = nextProps.commodityList.findIndex(comm =>
      String(comm.value) == String(nextProps.selectedCommodity))
    console.log(this.props);
    // console.log(nextProps);
    if ((this.props.selectedCommodity != nextProps.selectedCommodity) && this.props.selectedTab == 'FUTURE') {
      this.setState({
        commodityData: nextProps.commodityData[selectedCommodityId],
        expanded: null,
        chartsDataArr: [],
        rawChartsDataArr: [],
      });
      setTimeout(() => {
        this.loadCharts();
        this.props.setSelectedExpiryIndex(null);
        // this.props.setChartData(null);
      }, 10)
    }
    else if (nextProps.selectedTab == 'FUTURE' && this.state.needToLoadFuture) {
      this.setState({
        commodityData: nextProps.commodityData[selectedCommodityId],
        expanded: null,
        chartsDataArr: [],
        rawChartsDataArr: [],
        needToLoadFuture: false
      });
      setTimeout(() => {
        this.loadCharts();
        this.props.setSelectedExpiryIndex(null);
        // this.props.setChartData(null);
      }, 10)
    }
    else if ((this.props.selectedCommodity != nextProps.selectedCommodity) && this.props.selectedTab != 'FUTURE') {
      this.setState({
        needToLoadFuture: true
      });
    }
    else {
      this.setState({
        commodityData: nextProps.commodityData[selectedCommodityId],
      });
    }
  }

  loadCharts(expandTab) {
    for (let i = 0; i < this.state.commodityData.length; i++) {
      let chartsDataArr = this.state.chartsDataArr;
      let rawChartsDataArr = this.state.rawChartsDataArr;
      chartsDataArr.push([{}]);
      rawChartsDataArr.push([{}]);
      this.setState({
        loading: true,
        chartsDataArr: chartsDataArr,
        rawChartsDataArr: rawChartsDataArr
      });
      // if(Number(this.state.commodityData[i].LastTradedPrice) != 0){
      const req = {
        "Commodity": this.props.selectedCommodity,
        "ExpiryDate": this.state.commodityData[i].ExpiryDate
      }
      console.log((JSON.stringify(req)));
      let serviceRequest = common.encryptData(JSON.stringify(req));
      let serviceBody = { request: serviceRequest };
      console.log(serviceBody)
      common.callService('MarketFeedWithInterval', serviceBody)
        .then(MarketFeedWithInterval => {
          if (i == this.state.commodityData.length - 1) {
            setTimeout(() => {
              this.setState({
                loading: false,
              });
              if (expandTab)
                this.expandTab(this.props.selectedExpiry);
            }, 500)
          }
          console.log(this.state.commodityData[i].ExpiryDate);
          console.log(MarketFeedWithInterval);
          if (MarketFeedWithInterval.ReturnCode == '1') {
            let chartsDataArr = this.state.chartsDataArr;
            let processedArr = [];
            let rawChartsDataArr = this.state.rawChartsDataArr;
            let rawArr = [];

            if (MarketFeedWithInterval.Data.length > 0) {
              MarketFeedWithInterval.Data.forEach((data, index) => {
                let chartObj = { x: common.epochToDateObj(data.LastTradedTime.replace(/[^0-9]/g, '')), y: data.LastTradedPrice };
                rawArr.push(data);
                processedArr.push(chartObj);
                if (index == MarketFeedWithInterval.Data.length - 1) {
                  rawChartsDataArr.splice(i, 1, rawArr);
                  chartsDataArr.splice(i, 1, processedArr);
                  this.setState({
                    rawChartsDataArr: rawChartsDataArr,
                    chartsDataArr: chartsDataArr
                  });
                }
              });
            } else {
              rawChartsDataArr.splice(i, 1, []);
              chartsDataArr.splice(i, 1, []);
              this.setState({
                rawChartsDataArr: rawChartsDataArr,
                chartsDataArr: chartsDataArr
              });
            }


          }
        })
        .catch(err => {
          console.log(err);
        });
      // }
      // else{
      //   if (i == this.state.commodityData.length - 1) {
      //     setTimeout(()=>{
      //       this.setState({
      //         loading: false,
      //       });
      //       console.log(expandTab);
      //       if(expandTab)
      //       this.expandTab(this.props.selectedExpiry);
      //     },500)
      //   }
      // }

    }
  }

  addToWatchList(CommodityId, ContractCode) {

    const req = new addWatchlist();
    req.UserId = this.props.user.Id;
    req.ContractCode = ContractCode;

    console.log((JSON.stringify(req)));

    let serviceRequest = common.encryptData(JSON.stringify(req))
    this.setState({
      loading: true,
    });
    let serviceBody = { request: serviceRequest };
    console.log(serviceBody)
    common.callService('AddToWatchlist', serviceBody)
      .then(AddToWatchlist => {
        console.log(AddToWatchlist);
        if (AddToWatchlist.ReturnCode == '1') {
          this.setState({
            loading: false,
          });
          let tempArr = this.props.watchListArr;
          const obj = {
            CommodityId,
            ContractCode
          }
          tempArr.push(obj);
          this.setState({
            watchListArr: tempArr,
          });
          this.props.getWatchListArr(tempArr);
        } else {
          this.setState({
            loading: false,
          });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({
          loading: false,
        });
      });
  }

  showWatchList(showAddToWatchlist, Underlying, SymbolUniqueContractCode) {
    if (showAddToWatchlist) {
      return (
        <View
          style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}

        >
          <Icon name='eye'
            size={iconSize * 0.7}
            style={{ color: 'white' }}
            light
          />
          <Text style={{ color: 'white', fontSize: moderateScale(13), marginLeft: scale(5) }}>ADDED TO WATCHLIST</Text>
        </View>
      )
    } else {
      return (<TouchableOpacity
        style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}
        onPress={() => this.addToWatchList(Underlying, SymbolUniqueContractCode)}
      >
        <Icon name='eye'
          size={iconSize * 0.7}
          style={{ color: 'white' }}
          light
        />
        <Text style={{ color: 'white', fontSize: moderateScale(13), marginLeft: scale(5) }}>ADD TO WATCHLIST</Text>
      </TouchableOpacity>
      )
    }

  }

  formatAMPM(date, display) {
    if (date == null || date == undefined) {
      return '';
    } else {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let seconds = `${date.getSeconds()}`.padStart(2, '0');
      let ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      if (display == 'data') {
        let strTime = `${hours}:${minutes}:${seconds} ${ampm}`;
        return strTime;
      }
      let strTime = `${date.getHours()}:${minutes}`;
      return strTime;
    }
  }

  LttToDisplay(ltt) {
    let hours = ltt.split(":")[0];
    let minutes = ltt.split(":")[1];
    let seconds = ltt.split(":")[2];
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    let strTime = `${hours}:${minutes}:${seconds} ${ampm}`;
    return strTime;
  }

  priceLabelToDisplay(price, domainMin) {
    if (Number(domainMin) < 10000) {
      return common.addOnlyDecimals(price);
    }
    return Math.round(price);
  }

  // showChartData(d, commodityIndex) {
  //   // this.setState({
  //   //   chartValue: d.y
  //   // });
  //   let priceChange = '';
  //   let percentChange = '';
  //   this.state.rawChartsDataArr[commodityIndex].find(x => {
  //     if (x.LastTradedPrice == d.y) {
  //       priceChange = x.NetPriceChangeFromClosingPrice;
  //       percentChange = x.Change;
  //     }
  //   });
  //   if (d != null) {
  //     const obj = {
  //       x: this.formatAMPM(d.x, 'data'),
  //       y: d.y,
  //       priceChange,
  //       percentChange
  //     }
  //     if (this.props.chartData != null) {
  //       if (String(this.props.chartData.x) != String(obj.x)) {
  //         this.props.setChartData(obj);
  //       }

  //     } else {
  //       this.props.setChartData(obj);
  //     }
  //   }
  // }

  renderChart(domainMinMax, commodity) {
    if (this.state.chartsDataArr[commodity.index] != null && this.state.chartsDataArr[commodity.index] != undefined) {
      if (this.state.chartsDataArr[commodity.index].length > 1) {
        let axisLabelPadding = 0;
        if (String(domainMinMax.max).length <= 4) {
          axisLabelPadding = (String(domainMinMax.max).length - 2) * 10;
        } else {
          axisLabelPadding = (String(domainMinMax.max).length - 4) * 10;
        }
        return (
          <View style={{ backgroundColor: '#0b0b0b' }}>

            {/* Chart */}
            <View
              pointerEvents="none"
              style={{ marginTop: -30 }}>
              <VictoryChart height={width - verticalScale(120)} width={width - scale(30 + Number(axisLabelPadding))}
                domain={{ y: [domainMinMax.min, domainMinMax.max] }}
                domainPadding={{ y: 10 }}
                padding={{ left: 50 + moderateScale(Number(axisLabelPadding)), top: 50, right: 10, bottom: 50 }}
                theme={theme}
                responsive={false}
                scale={{ x: "time", y: "linear" }}
              // containerComponent={
              // <VictoryVoronoiContainer
              //   voronoiDimension="x"
              //   // labels={(d) => `y: ${common.addcommas(d.y)}`}
              //   labels={(d) => this.showChartData(d, commodity.index)}
              //   labelComponent={
              //     <VictoryTooltip
              //       cornerRadius={0}
              //       flyoutStyle={{ fill: "#c3c3eb" }}
              //       height={width - verticalScale(120)}
              //       width={2}
              //       pointerWidth={6}
              //     />}
              // />
              // }
              //   <VictoryCursorContainer
              //     style={{ stroke: 'white' }}
              //     cursorDimension="x"
              //     onCursorChange={(d) => {
              //       console.log(d)
              //       // let str=String(d).substr(17,8);
              //       // console.log(chartData.find(obj => {
              //       //   console.log("54"+obj.x)
              //       //   let x = String(obj.x).substr(0,22) == String(d).substr(0,22);
              //       // console.log("X:"+x)}))

              //       // this.setState({
              //       //   chartTime:str,
              //       //   chartValue:val
              //       // });
              //     }}
              //   />
              // }
              >
                <VictoryAxis
                  fixLabelOverlap={true}
                  tickFormat={(x) => this.formatAMPM(x, 'chart')}
                  label="Time"
                  tickCount={5}
                  style={{
                    axisLabel: { padding: 30 }
                  }}
                />
                <VictoryAxis dependentAxis
                  tickCount={5}
                  tickFormat={(y) => this.priceLabelToDisplay(Math.round(y * 100) / 100, domainMinMax.min)}
                  style={{
                    axisLabel: { padding: 50 }
                  }}
                />
                <VictoryArea
                  data={this.state.chartsDataArr[commodity.index]}
                  style={{
                    data: {
                      stroke: "#c2c3fc",
                      fill: '#1d1718',
                      // strokeWidth: (d, active) => { return active ? 4 : 2; }
                    },
                    labels: { fill: "tomato" }
                  }}
                  animate={{
                    // duration: 2000,
                    onLoad: { duration: 1000 },
                    // easing : 'back'
                  }}
                />
              </VictoryChart>

            </View>
            <View style={styles.watchlistStyle}>
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}
                onPress={() =>
                  this.props.navigation.navigate('ZoomChart', { chartData: this.state.chartsDataArr[commodity.index], commodity: commodity, rawChartsDataArr: this.state.rawChartsDataArr })}
              >
                <SLIcon name='magnifier-add'
                  size={iconSize * 0.7}
                  style={{ color: 'white' }}
                  light
                />
                <Text style={{ color: 'white', fontSize: moderateScale(13), marginLeft: scale(5), paddingRight: moderateScale(10) }}>ZOOM</Text>
              </TouchableOpacity>

            </View>

          </View>
        );
      }
      return null;
    }
    return null;
  }

  canShowLTP(LastTradedTime) {
    let lttDate = new Date(LastTradedTime.substr(0, 10));
    lttDate.setHours(0, 0, 0, 0);
    let currDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    if (lttDate.getTime() !== currDate.getTime()) {
      return false;
    } else {
      return true;
    }
  }

  renderDetails(commodity) {
    if (this.state.expanded == commodity.index) {
      const {
        AverageTradePrice, BuyPrice, BuyQuantity, SellPrice, SellQuantity, Underlying, SymbolUniqueContractCode,
        OpenInterest, HighPrice, LowPrice, ClosePrice, OpenPrice, VolumeTradedToday, ValueTradedToday,
        NetPriceChangeFromClosingPrice, NetPercentChangeFromClosingPrice, LastTradedPrice, LastTradedTime, PreviousClosePrice
      } = commodity.item;
      let showAddToWatchlist = false;
      if (this.state.watchListArr != null) {
        if ((this.state.watchListArr).find(x => x.ContractCode == SymbolUniqueContractCode)) {
          showAddToWatchlist = true;
        }
      }
      // console.log(this.state.chartsDataArr);
      let domainMinMax = '';
      if (this.state.chartsDataArr[commodity.index] != null && this.state.chartsDataArr[commodity.index] != undefined) {
        if (this.state.chartsDataArr[commodity.index].length > 1) {
          domainMinMax = common.findMinMax(this.state.chartsDataArr[commodity.index], 'y');
        } else {
          domainMinMax = { min: this.state.chartsDataArr[commodity.index].y - 10, max: this.state.chartsDataArr[commodity.index].y + 10 }
        }
      }

      let bgColor = '#4d4d4d';
      let iconName = '';

      let timeToDisplay = '';
      LastTradedTime != null && LastTradedTime != undefined && String(LastTradedTime).trim() != ''
        ? timeToDisplay = this.LttToDisplay(LastTradedTime.substring(11, 19)) : timeToDisplay;

      // console.log('ValueTradedToday', ValueTradedToday)
      // console.log('VolumeTradedToday', VolumeTradedToday)
      // console.log('timeToDisplay', timeToDisplay);
      // console.log('LastTradedTime', LastTradedTime);

      // let timeToDisplay = this.formatAMPM(time, 'data');
      // console.log(timeToDisplay)
      if (Number(NetPriceChangeFromClosingPrice) > 0) {
        bgColor = '#01800f';
        iconName = 'long-arrow-up';
      } else if (Number(NetPriceChangeFromClosingPrice) < 0) {
        bgColor = '#de0808';
        iconName = 'long-arrow-down';
      }

      let showLTPDetail = false;
      // let lttDate = '';

      if (LastTradedTime != null && LastTradedTime != undefined) {
        showLTPDetail = this.canShowLTP(LastTradedTime);
        // let dateObj = new Date(LastTradedTime.substr(0, 10));
        // lttDate = `${common.getDayName(dateObj.getDay())}, ${dateObj.getDate()} ${common.getMonthName(dateObj.getMonth()).substring(0, 3)}`;
      }

      return (
        <View >
          <View style={styles.itemDetail}>

            {/* Details above chart */}
            {LastTradedPrice != 0 && showLTPDetail ?
              <View style={styles.cursorDetailsContainer}>
                <View style={{ flexDirection: 'row', flex: 0.6, justifyContent: 'center' }}>

                  <Text style={[styles.headerText, { alignSelf: 'center' }]}>{timeToDisplay} </Text>

                </View>
                <View style={{ flexDirection: 'row', flex: 1.4 }}>
                  <View style={{
                    flexDirection: 'row',
                    backgroundColor: bgColor, marginLeft: scale(20), flex: 0.9,
                    marginTop: verticalScale(10),
                    marginBottom: verticalScale(10),
                    justifyContent: 'space-evenly',
                  }}>
                    {iconName != '' ?
                      <Icon name={iconName}
                        size={iconSize * 0.7}
                        style={styles.cursorDetailsText}
                        light
                      /> : null
                    }

                    <Text style={[styles.headerText, { alignSelf: 'center' }]}>{common.addDecimals(LastTradedPrice)}</Text>

                  </View>
                  <View style={{
                    backgroundColor: '#1f1f1f', marginRight: scale(20), flex: 1.1,
                    width: '90%',
                    marginTop: verticalScale(10),
                    marginBottom: verticalScale(10),
                    // borderRadius: moderateScale(5),
                    justifyContent: 'center',

                  }}>
                    <Text style={styles.cursorDetailsText}>{common.addDecimals(NetPriceChangeFromClosingPrice)}({common.addDecimals(NetPercentChangeFromClosingPrice)}%)</Text>

                  </View>
                </View>
              </View> : null
            }


            {showLTPDetail ?
              this.renderChart(domainMinMax, commodity)
              : null
            }


            {/* Details below chart */}
            <View style={styles.bottomDetailsContainer}>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>

                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>Bid Price(Qty)</Text>
                  {Number(BuyQuantity[0]) == 0 ?
                    <Text style={styles.bottomDetailsValueText}>-</Text>
                    : <Text style={styles.bottomDetailsValueText}>{common.addDecimals(BuyPrice[0])}({BuyQuantity[0]})</Text>
                  }
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>Offer Price(Qty)</Text>
                  {Number(SellPrice[0]) == 0 ?
                    <Text style={styles.bottomDetailsValueText}>-</Text>
                    : <Text style={styles.bottomDetailsValueText}>{common.addDecimals(SellPrice[0])}({SellQuantity[0]})</Text>
                  }
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>Open Interest</Text>
                  <Text style={styles.bottomDetailsValueText}>{common.addcommas(OpenInterest)}</Text>
                </View>

              </View>



              <View style={styles.bottomDetailsView}>

                {/* <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>Open Interest{"\n"}Change</Text>
                  <Text style={styles.bottomDetailsValueText}>15</Text>
                </View> */}
                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>Avg. Trade Price</Text>
                  {showLTPDetail ?
                    <Text style={styles.bottomDetailsValueText}>{common.addDecimals(AverageTradePrice)}</Text>
                    : <Text style={styles.bottomDetailsValueText}>-</Text>
                  }
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>Open</Text>
                  {showLTPDetail ?
                    <Text style={styles.bottomDetailsValueText}>{common.addDecimals(OpenPrice)}</Text>
                    : <Text style={styles.bottomDetailsValueText}>-</Text>
                  }
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>High</Text>
                  {showLTPDetail ?
                    <Text style={styles.bottomDetailsValueText}>{common.addDecimals(HighPrice)}</Text>
                    : <Text style={styles.bottomDetailsValueText}>-</Text>
                  }
                </View>

              </View>

              <View style={styles.bottomDetailsView}>


                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>Low</Text>
                  {showLTPDetail ?
                    <Text style={styles.bottomDetailsValueText}>{common.addDecimals(LowPrice)}</Text>
                    : <Text style={styles.bottomDetailsValueText}>-</Text>
                  }
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>PCP</Text>
                  <Text style={styles.bottomDetailsValueText}>{common.addDecimals(PreviousClosePrice)}</Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text style={styles.bottomDetailsPropertyText}>Turnover(Lacs)</Text>
                  {showLTPDetail ?
                    <Text style={styles.bottomDetailsValueText}>{common.addDecimals(ValueTradedToday)}</Text>
                    : <Text style={styles.bottomDetailsValueText}>-</Text>
                  }
                </View>

              </View>

              <View style={styles.bottomDetailsView}>


                <View style={{ flex: 1.2 }}>
                  <Text style={styles.bottomDetailsPropertyText}>No. Of Contracts Traded</Text>
                  {showLTPDetail ?
                    <Text style={styles.bottomDetailsValueText}>{common.addcommas(VolumeTradedToday)}</Text>
                    : <Text style={styles.bottomDetailsValueText}>-</Text>
                  }
                </View>
                <View style={{ flex: 0.8 }}>
                  {/* <Text style={{ color: '#a2a2a2', alignSelf: 'center', marginBottom: 10 }}>Open Interest</Text>
                  <Text style={{ color: '#ffffff', alignSelf: 'center' }}>13,225.00</Text> */}
                </View>

              </View>
            </View>

          </View>

          <View style={styles.watchlistStyle}>
            {this.showWatchList(showAddToWatchlist, Underlying, SymbolUniqueContractCode)}
          </View>

        </View>
      );
    }
    return null;

  }

  expandTab(index) {
    let expandedTab;
    this.state.expanded == index ? expandedTab = null : expandedTab = index;
    this.state.expanded == index ? this.props.setSelectedExpiryIndex(null) : this.props.setSelectedExpiryIndex(index);
    this.setState({
      expanded: expandedTab
    });
    // this.props.setChartData(null);
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    LayoutAnimation.easeInEaseOut();

  }

  renderItemList(commodity) {
    const { Underlying, ExpiryDate, LastTradedPrice, LastTradedTime, PreviousClosePrice, NetPriceChangeFromClosingPrice, NetPercentChangeFromClosingPrice } = commodity.item;
    let commodityitemTopTabStyle = {};
    let iconName = 'minus';
    let background = '#4d4d4d';

    let showLTPDetail = false;

    if (LastTradedTime != null && LastTradedTime != undefined) {
      showLTPDetail = this.canShowLTP(LastTradedTime);
    }

    if (commodity.index != this.state.expanded) {
      commodityitemTopTabStyle = {
        marginBottom: moderateScale(5),
        borderBottomLeftRadius: moderateScale(5),
        borderBottomRightRadius: moderateScale(5)
      }
      iconName = 'plus';
    }
    if (showLTPDetail) {
      if (NetPriceChangeFromClosingPrice < 0) {
        background = '#de0808';
      } else if (NetPriceChangeFromClosingPrice > 0) {
        background = '#01800f';
      }
    }

    return (
      <View>
        <TouchableOpacity
          style={[styles.itemTopTab, commodityitemTopTabStyle]}
          onPress={() => this.expandTab(commodity.index)}
        >
          <View style={[styles.headerTabs, { paddingLeft: scale(10), paddingBottom: moderateScale(5), paddingTop: moderateScale(5) }]}>
            <Text style={styles.dataText}>{Underlying}</Text>
            <Text style={styles.itemDateText}>{common.expiryDateToDisplay(ExpiryDate)}</Text>
          </View>
          <View style={[styles.headerTabs, styles.priceTab, { paddingBottom: moderateScale(5), paddingTop: moderateScale(5) }]}>
          {showLTPDetail ?
              <Text style={styles.dataText}>{common.addDecimals(LastTradedPrice)}</Text>:
              <Text style={styles.dataText}>{common.addDecimals(PreviousClosePrice)}</Text>
            }          
            </View>
          <View style={[styles.headerTabs, styles.changeTab, { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }]}>
            <View style={[styles.changeItemContainer, { backgroundColor: background }]}>
              {showLTPDetail ?
                <Text style={styles.changeItemText}>{common.addDecimals(NetPercentChangeFromClosingPrice)}%</Text> :
                <Text style={styles.changeItemText}>0.00%</Text>
              }
            </View>
            <Icon name={iconName}
              size={moderateScale(14)}
              style={{ color: 'white', alignSelf: 'center' }}
              light
            />
          </View>
        </TouchableOpacity>
        {this.renderDetails(commodity)}
      </View>

    );
  }

  render() {
    return (

      <ScrollView style={{
        marginLeft: scale(15),
        marginRight: scale(15),
        marginBottom: moderateScale(10)
      }}>
        <Loader loading={this.state.loading} />
        <View style={styles.header}>
          <View style={[styles.headerTabs, { paddingLeft: 10 }]}>
            <Text style={styles.headerText}>Commodity{"\n"}Name</Text>
          </View>
          <View style={[styles.headerTabs, styles.priceTab]}>
            <Text style={styles.headerText}>LTP/CP</Text>
          </View>
          <View style={[styles.headerTabs, styles.changeTab]}>
            <Text style={styles.headerText}>Change%</Text>
          </View>
        </View>

        <FlatList
          data={this.state.commodityData}
          renderItem={(commodity) => this.renderItemList(commodity)}
          keyExtractor={(index) => JSON.stringify(index)}
          removeClippedSubviews={false}
          extraData={this.state}
        />

      </ScrollView >

    );
  }

}

const mapStateToProps = state => {
  const { loading, commodityData, watchListArr, chartData, commodityList } = state.commDetails;
  const { user } = state.auth;
  return { loading, commodityData, user, watchListArr, chartData, commodityList }
}

export default connect(mapStateToProps, { getWatchListArr, setChartData, setSelectedExpiryIndex })(Future);

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  header: {
    // flex: 1,
    flexDirection: 'row',
    height: verticalScale(45),
    backgroundColor: '#4d4d4d',
    // marginLeft: scale(20),
    // marginRight: scale(20),
    // marginTop: verticalScale(15),
    marginBottom: verticalScale(10),
    borderRadius: moderateScale(5),
    borderWidth: 1,
    borderColor: '#59595a'
  },
  headerTabs: {
    justifyContent: 'center',
    flex: 1
  },
  priceTab: {
    alignItems: 'flex-end',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    paddingRight: scale(10),
    borderColor: '#59595a'
  },
  changeTab: {
    alignItems: 'flex-end',
    paddingRight: scale(10),
    paddingLeft: moderateScale(10),
  },
  headerText: {
    color: 'white',
    fontSize: moderateScale(14),
    //   fontWeight: '500'
  },
  dataText: {
    color: 'white',
    fontSize: moderateScale(12),
    //   fontWeight: '500'
  },

  itemDetail: {
    // flex: 1,
    // flexDirection: 'row',
    // height: verticalScale(200),
    backgroundColor: '#171717',
    // marginLeft: scale(20),
    // marginRight: scale(20),
    // marginTop: verticalScale(10),
    marginBottom: verticalScale(10),
    borderRadius: moderateScale(5),
    // borderWidth: 1,
    // borderColor: '#59595a'
  },
  itemTopTab: {
    flexDirection: 'row',
    backgroundColor: '#171717',
    height: moderateScale(60),
    borderTopLeftRadius: moderateScale(5),
    borderTopRightRadius: moderateScale(5),
    // marginBottom: verticalScale(10),
    // borderBottomWidth: 1,
    // borderColor: '#59595a',

  },
  itemDateText: {
    fontSize: moderateScale(14),
    color: '#9a9a9a'
  },
  changeItemContainer: {
    // flex: 1,
    width: '70%',
    height: '75%',
    // marginTop: verticalScale(10),
    // marginBottom: verticalScale(10),
    borderRadius: moderateScale(5),
    justifyContent: 'center',
    alignItems: 'center'
  },
  changeItemText: {
    alignSelf: 'flex-end',
    paddingRight: 5,
    color: 'white',
    fontSize: moderateScale(12),
    fontWeight: '500',
  },
  cursorDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: verticalScale(10),
    borderBottomWidth: 1,
    borderColor: '#1f1f1f',
    backgroundColor: '#0b0b0b',
    zIndex: 10000
  },
  cursorDetailsText: {
    color: 'white',
    fontSize: moderateScale(12),
    alignSelf: 'center'
  },
  bottomDetailsPropertyText: {
    color: '#a2a2a2',
    alignSelf: 'flex-start',
    fontSize: moderateScale(13),
    marginBottom: 5
  },
  bottomDetailsValueText: {
    color: '#ffffff',
    fontSize: moderateScale(13),
    alignSelf: 'flex-start'
  },
  watchlistStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    // marginRight: scale(20),
    marginBottom: verticalScale(20)
  },
  bottomDetailsContainer: {
    flex: 1,
    backgroundColor: '#171717',
    paddingLeft: scale(10),
    paddingTop: moderateScale(20),
    paddingBottom: verticalScale(10),
    borderBottomLeftRadius: moderateScale(5),
    borderBottomRightRadius: moderateScale(5)
  },
  bottomDetailsView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: verticalScale(20)
  },

});

// Chart Theme Styles, Used for WHITE Labels


const colors = [
  "#252525",
  "#525252",
  "#737373",
  "#969696",
  "#bdbdbd",
  "#d9d9d9",
  "#f0f0f0"
];
const charcoal = "#252525";
const grey = "#969696";

// Typography
const sansSerif =
  "'Gill Sans', 'Gill Sans MT', 'Ser­avek', 'Trebuchet MS', sans-serif";
const letterSpacing = "normal";
const fontSize = 14;

// Layout
const baseProps = {
  width: 450,
  height: 300,
  padding: 50,
  colorScale: colors
};

// Labels
const baseLabelStyles = {
  fontFamily: sansSerif,
  fontSize,
  letterSpacing,
  padding: 10,
  fill: '#fff',
  stroke: "transparent"
};
const centeredLabelStyles = Object.assign({ textAnchor: "middle" }, baseLabelStyles);

// Strokes
const strokeLinecap = "round";
const strokeLinejoin = "round";

// Put it all together...
const theme = {
  area: Object.assign({
    style: {
      data: {
        fill: charcoal
      },
      labels: centeredLabelStyles
    }
  }, baseProps),
  axis: Object.assign({
    style: {
      axis: {
        fill: "transparent",
        stroke: charcoal,
        strokeWidth: 1,
        strokeLinecap,
        strokeLinejoin
      },
      axisLabel: Object.assign({}, centeredLabelStyles, {
        padding: 25
      }),
      grid: {
        fill: "none",
        stroke: "none",
        pointerEvents: "painted"
      },
      ticks: {
        fill: "transparent",
        size: 1,
        stroke: "transparent"
      },
      tickLabels: baseLabelStyles
    }
  }, baseProps),
  bar: Object.assign({
    style: {
      data: {
        fill: charcoal,
        padding: 8,
        strokeWidth: 0
      },
      labels: baseLabelStyles
    }
  }, baseProps),
  boxplot: Object.assign({
    style: {
      max: { padding: 8, stroke: charcoal, strokeWidth: 1 },
      maxLabels: baseLabelStyles,
      median: { padding: 8, stroke: charcoal, strokeWidth: 1 },
      medianLabels: baseLabelStyles,
      min: { padding: 8, stroke: charcoal, strokeWidth: 1 },
      minLabels: baseLabelStyles,
      q1: { padding: 8, fill: grey },
      q1Labels: baseLabelStyles,
      q3: { padding: 8, fill: grey },
      q3Labels: baseLabelStyles
    },
    boxWidth: 20
  }, baseProps),
  candlestick: Object.assign({
    style: {
      data: {
        stroke: charcoal,
        strokeWidth: 1
      },
      labels: centeredLabelStyles
    },
    candleColors: {
      positive: "#ffffff",
      negative: charcoal
    }
  }, baseProps),
  chart: baseProps,
  errorbar: Object.assign({
    borderWidth: 8,
    style: {
      data: {
        fill: "transparent",
        stroke: charcoal,
        strokeWidth: 2
      },
      labels: centeredLabelStyles
    }
  }, baseProps),
  group: Object.assign({
    colorScale: colors
  }, baseProps),
  legend: {
    colorScale: colors,
    gutter: 10,
    orientation: "vertical",
    titleOrientation: "top",
    style: {
      data: {
        type: "circle"
      },
      labels: baseLabelStyles,
      title: Object.assign({}, baseLabelStyles, { padding: 5 })
    }
  },
  line: Object.assign({
    style: {
      data: {
        fill: "transparent",
        stroke: 'charcoal',
        strokeWidth: 2
      },
      labels: centeredLabelStyles
    }
  }, baseProps),
  pie: {
    style: {
      data: {
        padding: 10,
        stroke: "transparent",
        strokeWidth: 1
      },
      labels: Object.assign({}, baseLabelStyles, { padding: 20 })
    },
    colorScale: colors,
    width: 400,
    height: 400,
    padding: 50
  },
  scatter: Object.assign({
    style: {
      data: {
        fill: charcoal,
        stroke: "transparent",
        strokeWidth: 0
      },
      labels: centeredLabelStyles
    }
  }, baseProps),
  stack: Object.assign({
    colorScale: colors
  }, baseProps),
  tooltip: {
    style: Object.assign({}, centeredLabelStyles, { padding: 5, pointerEvents: "none" }),
    flyoutStyle: {
      stroke: charcoal,
      strokeWidth: 1,
      fill: "#f0f0f0",
      pointerEvents: "none"
    },
    cornerRadius: 5,
    pointerLength: 10
  },
  voronoi: Object.assign({
    style: {
      data: {
        fill: "transparent",
        stroke: "transparent",
        strokeWidth: 0
      },
      labels: Object.assign({}, centeredLabelStyles, { padding: 5, pointerEvents: "none" }),
      flyout: {
        stroke: charcoal,
        strokeWidth: 1,
        fill: "#f0f0f0",
        pointerEvents: "none"
      }
    }
  }, baseProps)
};

class addWatchlist {
  UserId;
  ContractCode;

  constructor() { }
}
