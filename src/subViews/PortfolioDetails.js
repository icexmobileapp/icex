import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, TextInput, TouchableOpacity, Dimensions, UIManager, LayoutAnimation, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { connect } from 'react-redux';
import Overlay from 'react-native-modal-overlay';
import { DatePicker, Toast } from 'native-base';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import Icon from "react-native-vector-icons/FontAwesome";
import SLIcon from "react-native-vector-icons/SimpleLineIcons";
import Loader from "../components/Loader";
import Header from '../components/Header';
import common from '../common';
import { needToLoadPortfolioData } from "../actions";

const { height, width } = Dimensions.get('window');
iconSize = (height + width) / 55;

class Portfolio extends Component {

    state = {
        portfolioDetails: [],
        loading: false,
        portfolioDetailsErr: '',
        modalVisible: false,
        selectedTransId: '',
        chosenDate: new Date(),
        action: 'buy',
        quantity: '',
        price: '',
        quantityErr: '',
        priceErr: '',
        transDateErr: ''
    }

    componentWillMount() {
        var itemDetails = this.props.navigation.getParam('itemDetails');

        var DateObj = common.removeOffsetFromDate(common.epochToDateObj(itemDetails.ExpiryDate.replace(/[^0-9]/g, '')));

        const req = {
            "UserId": this.props.user.Id,
            "CommodityId": itemDetails.CommodityId,
            "ExpiryDate": DateObj
        };
        console.log((JSON.stringify(req)));

        var serviceRequest = common.encryptData(JSON.stringify(req))
        this.setState({
            loading: true,
        });
        var serviceBody = { request: serviceRequest };
        console.log(serviceBody);

        common.callService('GetCommodityTransaction', serviceBody)
            .then(CommodityTransaction => {
                console.log('CommodityTransaction', JSON.stringify(CommodityTransaction));
                if (CommodityTransaction.ReturnCode == '1') {
                    this.setState({
                        loading: false,
                        portfolioDetailsErr: '',
                        portfolioDetails: CommodityTransaction.Data
                    });
                } else {
                    this.setState({
                        loading: false,
                        portfolioDetailsErr: CommodityTransaction.ReturnMsg
                    });
                }
            })
            .catch(err => {
                console.log('CommodityTransaction', err);
                this.setState({
                    loading: false,
                    portfolioDetailsErr: 'Unable to fetch transaction details'
                });
            })

    }

    getDateToDisplay(DateObj) {
        var month = common.getMonthName(DateObj.getMonth()).substring(0, 3);
        var year = DateObj.getFullYear();
        var date = DateObj.getDate();
        return `${date} ${month} ${year}`
    }

    deleteTransaction(Id) {
        const req = { Id };
        console.log((JSON.stringify(req)));

        var serviceRequest = common.encryptData(JSON.stringify(req))
        this.setState({
            loading: true,
        });
        var serviceBody = { request: serviceRequest };
        console.log(serviceBody);

        common.callService('DeleteCommodityTransaction', serviceBody)
            .then(DelCommTransctn => {
                console.log('DelCommTransctn', DelCommTransctn);
                this.setState({
                    loading: false,
                });
                if (DelCommTransctn.ReturnCode == '1') {
                    this.props.needToLoadPortfolioData(true);
                    this.setState({
                        portfolioDetails: DelCommTransctn.Data
                    });
                } else {
                    setTimeout(() => {
                        Toast.show({
                            text: 'Unable to delete transaction',
                            type: "danger"
                        });
                    }, 10)
                }
            })
            .catch(err => {
                console.log('DelCommTransctn', err);
                setTimeout(() => {
                    Toast.show({
                        text: 'Unable to delete transaction',
                        type: "danger"
                    });
                }, 10)
                this.setState({
                    loading: false,
                });
            })
    }

    editTransaction(commDetails) {
        const { Id, TransactionDate, Quantity, Price, Action } = commDetails;
        var DateObj = common.removeOffsetFromDate(common.epochToDateObj(TransactionDate.replace(/[^0-9]/g, '')));
        var transType = 'buy'
        if (Action == 0) {
            transType = 'sell';
        }
        this.setState({
            modalVisible: true,
            selectedTransId: Id,
            chosenDate: DateObj,
            action: transType,
            quantity: String(Quantity),
            price: String(Price.toFixed(2)),
        });
    }

    saveItemClicked() {
        this.setState({
            quantityErr: '',
            priceErr: '',
            transDateErr: ''
        });
        var canProceed = true;
        var regex = /^[0-9]\d{0,9}(\.\d{1,2})?%?$/;
        if (this.state.quantity.trim() == '') {
            canProceed = false;
            this.setState({
                quantityErr: 'Please enter quantity'
            });
        } else if (this.state.quantity == 0) {
            canProceed = false;
            this.setState({
                quantityErr: 'Quantity can not be zero'
            });
        }
        if (this.state.price.trim() == '') {
            canProceed = false;
            this.setState({
                priceErr: 'Please enter price'
            });
        } else if (!regex.test(this.state.price.trim())) {
            canProceed = false;
            this.setState({
                priceErr: 'Please enter valid price'
            });
        }

        if (canProceed) {
            console.log(this.state);
            var action = '';
            this.state.action == 'buy' ? action = 1 : action = 0;

            let transDateObj = common.removeOffsetFromDate(new Date(this.state.chosenDate));
            let transDate = JSON.parse(JSON.stringify(transDateObj));

            let req = {
                'Id': this.state.selectedTransId,
                'Action': action,
                'Quantity': this.state.quantity,
                'Price': this.state.price,
                'TransactionDate': transDate
            };
            console.log(req);
            var serviceRequest = common.encryptData(JSON.stringify(req));
            this.setState({
                loading: true,

            });
            var serviceBody = { request: serviceRequest };
            console.log(serviceBody);

            common.callService('UpdateCommodityTransaction', serviceBody)
                .then(UpdateComm => {
                    console.log('UpdateComm', JSON.stringify(UpdateComm));
                    this.setState({
                        loading: false,
                    });
                    if (UpdateComm.ReturnCode == '1') {
                        this.props.needToLoadPortfolioData(true);
                        this.setState({
                            portfolioDetails: UpdateComm.Data
                        });
                    } else {
                        setTimeout(() => {
                            Toast.show({
                                text: "Unable to edit transaction",
                                type: "danger"
                            });
                        }, 10)
                    }
                })
                .catch(err => {
                    console.log(err);
                    setTimeout(() => {
                        Toast.show({
                            text: "Unable to edit transaction",
                            type: "danger"
                        });
                    }, 10)
                    this.setState({
                        loading: false,
                    });
                });
            this.onClose();
        }
    }

    renderPortfolioDetails(comm) {
        const { Id, TransactionDate, Quantity, Price, Multiplier, Action } = comm.item;

        var transType = 'Buy';
        var textColor = '#01800f';
        var sign = '+';
        if (Action == 0) {
            transType = 'Sell';
            textColor = '#de0808';
            sign = '-';
        }
        var DateObj = common.epochToDateObj(TransactionDate.replace(/[^0-9]/g, ''));
        var transDateToDisplay = this.getDateToDisplay(DateObj);

        var netValue = Number(Quantity) * Number(Multiplier) * Number(Price);

        return (
            <View>
                <View style={[styles.items]}>
                    <View style={[styles.headerTabs, { paddingLeft: 10 }]}>
                        {/* <Text style={styles.headerText}>{Multiplier}</Text> */}
                        <Text style={styles.headerText}>{transDateToDisplay}</Text>
                    </View>
                    <View style={[styles.headerTabs, styles.priceTab, { borderColor: '#2e2e2e' }]}>
                        <Text style={[styles.headerText, { color: textColor }]}>{Quantity}</Text>
                        <Text style={styles.headerText}>{transType}</Text>
                        {/* <Text style={styles.headerText}>{common.addDecimals(NetValue)}</Text> */}
                    </View>
                    <View style={[styles.headerTabs, styles.changeTab]}>
                        <Text style={[styles.headerText, { color: textColor }]}>{sign} {common.addDecimals(Price)}</Text>
                        <Text style={[styles.headerText, { color: textColor }]}>{sign} {common.addDecimals(netValue)}</Text>
                        {/* <Text style={[styles.headerText, { color: profTextColor }]}>{common.addDecimals(profitLoss)}</Text> */}
                    </View>
                </View>

                <View style={{
                    flexDirection: 'row', justifyContent: 'flex-end',
                    alignItems: 'center', marginRight: moderateScale(20),
                    //  marginBottom:moderateScale(5), marginTop:moderateScale(5)
                }}>
                    <TouchableOpacity style={{ padding: moderateScale(5), flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => this.editTransaction(comm.item)}
                    >
                        <Icon name='pencil'
                            size={moderateScale(15)}
                            style={{ color: '#fff', paddingRight: moderateScale(5) }}
                        />
                        <Text style={{ fontSize: moderateScale(13), color: '#fff' }}>
                            Edit
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ padding: moderateScale(5), marginLeft: moderateScale(10), flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => this.deleteTransaction(Id)}
                    >
                        <Icon name='remove'
                            size={moderateScale(15)}
                            style={{ color: '#fff', paddingRight: moderateScale(5) }}
                        />
                        <Text style={{ fontSize: moderateScale(13), color: '#fff' }}>
                            Delete
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>

        );
    }

    renderPortfolioList() {

        if (this.state.loading) {
            return null;
        }
        else if (this.state.portfolioDetailsErr != '') {
            return (
                <View
                    style={{
                        flex: 1, backgroundColor: '#303132', padding: moderateScale(20),
                        alignItems: 'center', justifyContent: 'center'
                    }}
                >
                    <Icon name='chain-broken'
                        size={moderateScale(35)}
                        style={{ color: 'white' }}
                    />
                    <Text style={{ fontSize: moderateScale(20), color: 'white', marginTop: moderateScale(20) }}>
                        {this.state.portfolioDetailsErr}
                    </Text>
                </View>
            );
        }
        return (
            <View>
                <View style={styles.header}>
                    <View style={[styles.headerTabs, { paddingLeft: 10 }]}>
                        {/* <Text style={styles.headerText}>Multiplier</Text> */}
                        <Text style={styles.headerText}>Trans. Date</Text>
                    </View>
                    <View style={[styles.headerTabs, styles.priceTab]}>
                        <Text style={styles.headerText}>Quantity</Text>
                        <Text style={styles.headerText}>Trans. Type</Text>
                    </View>
                    <View style={[styles.headerTabs, styles.changeTab]}>
                        <Text style={styles.headerText}>Price</Text>
                        <Text style={styles.headerText}>Net. Value</Text>
                    </View>
                </View>

                <FlatList
                    data={this.state.portfolioDetails}
                    extraData={this.state}
                    renderItem={(comm) => this.renderPortfolioDetails(comm)}
                    keyExtractor={(index) => JSON.stringify(index)}
                    removeClippedSubviews={false}
                />
            </View>
        );
    }

    onSelect(index, value) {
        this.setState({
            action: value
        });
    }

    onClose = () => this.setState({ modalVisible: false });

    render() {
        var itemDetails = this.props.navigation.getParam('itemDetails');
        var expDateObj = common.epochToDateObj(itemDetails.ExpiryDate.replace(/[^0-9]/g, ''));
        var expDateToDisplay = this.getDateToDisplay(expDateObj);
        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation} backBtn={true} />
                <Loader loading={this.state.loading} />
                <LinearGradient
                    colors={['#303132', '#171717']}
                    // locations={[0,0.5,0.9]}
                    start={{ x: 0.5, y: 0.5 }} end={{ x: 1.0, y: 1.0 }}
                    style={{ flex: 1 }}
                >
                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: moderateScale(20), marginRight: moderateScale(20), paddingTop: moderateScale(20), paddingBottom: moderateScale(10) }}>
                            <Text style={{ fontSize: moderateScale(18), color: 'white' }}>{itemDetails.CommodityId}</Text>
                            <Text style={{ fontSize: moderateScale(18), color: 'white' }}>{expDateToDisplay}</Text>
                        </View>

                        {this.renderPortfolioList()}

                        <Overlay
                            visible={this.state.modalVisible}
                            onClose={this.onClose}
                            closeOnTouchOutside={false}
                            containerStyle={{
                                backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', paddingRight: 0, paddingTop: 0
                            }}
                            childrenWrapperStyle={{
                                alignSelf: 'center',
                                backgroundColor: '#2b2b33',
                                justifyContent: 'center',
                                width: width > 600 ? 600 : width - scale(40),
                                borderRadius: moderateScale(5),
                                marginLeft: moderateScale(-20)
                            }}
                        >
                            <View>
                                <View style={{ alignItems: 'flex-end', marginRight: moderateScale(-10), marginBottom: moderateScale(5), marginTop: moderateScale(-10) }}>
                                    <TouchableOpacity
                                        style={{ padding: moderateScale(5) }}
                                        onPress={this.onClose}
                                    >
                                        <SLIcon name='close'
                                            size={moderateScale(23)}
                                            style={{ color: '#ffffff' }}
                                        />
                                    </TouchableOpacity>

                                </View>


                                <View style={{ marginBottom: moderateScale(5) }}>
                                    <RadioGroup
                                        size={moderateScale(22)}
                                        thickness={moderateScale(2)}
                                        color='#5054cb'
                                        style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}
                                        // highlightColor='#ccc8b9'
                                        selectedIndex={this.state.action == 'buy' ? 0 : 1}
                                        onSelect={(index, value) => this.onSelect(index, value)}
                                    >

                                        <RadioButton
                                            value='buy'
                                            color='#d9fff1'
                                        >
                                            <Text style={{ fontSize: moderateScale(14), color: '#ffffff' }}>BUY</Text>
                                        </RadioButton>

                                        <RadioButton
                                            value='sell'
                                            color='#d9fff1'
                                        >
                                            <Text style={{ fontSize: moderateScale(14), color: '#ffffff' }}>SELL</Text>
                                        </RadioButton>
                                    </RadioGroup>
                                </View>

                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(15) }}>

                                    <View style={{}}>
                                        <Text style={{ fontSize: moderateScale(13), color: 'white', alignContent: 'flex-start' }}>Trans. Date:</Text>
                                    </View>

                                    <View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', width: width * 0.45, height: moderateScale(40), borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-between' }}>
                                            <DatePicker
                                                defaultDate={this.state.chosenDate}
                                                minimumDate={new Date(2018, 1, 1)}
                                                maximumDate={new Date()}
                                                locale={"en"}
                                                timeZoneOffsetInMinutes={undefined}
                                                modalTransparent={false}
                                                animationType={"fade"}
                                                androidMode={"default"}
                                                // placeHolderText="Select date"
                                                textStyle={{ paddingRight: width * 0.2, color: 'white', fontSize: moderateScale(14), marginLeft: moderateScale(5) }}
                                                // placeHolderTextStyle={{ color: "#d3d3d3" }}
                                                onDateChange={(newDate) => this.setState({ chosenDate: newDate })}
                                                disabled={false}
                                            />
                                            {/* <Icon name='angle-down'
                                        size={moderateScale(20)}
                                        style={{ color: '#fff', marginLeft: moderateScale(-10), alignSelf:'center' }}
                                    /> */}
                                        </View>
                                        {!this.state.transDateErr == '' ?
                                            <Text style={styles.errorMsg}>{this.state.transDateErr}</Text> : null
                                        }
                                    </View>

                                </View>

                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(15) }}>

                                    <View style={{}}>
                                        <Text style={{ fontSize: moderateScale(13), color: 'white', alignContent: 'flex-start' }}>Quantity:</Text>
                                    </View>

                                    <View>
                                        <View style={{ width: width * 0.45, height: moderateScale(40), borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-around' }}>
                                            <TextInput
                                                style={{ color: 'white', fontSize: moderateScale(14), marginLeft: moderateScale(5) }}
                                                keyboardType='numeric'
                                                maxLength={5}
                                                value={this.state.quantity}
                                                onChangeText={(text) => { this.setState({ quantity: common.numberOnly(text) }) }}
                                            />
                                        </View>
                                        {!this.state.quantityErr == '' ?
                                            <Text style={styles.errorMsg}>{this.state.quantityErr}</Text> : null
                                        }
                                    </View>

                                </View>

                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(15) }}>

                                    <View style={{}}>
                                        <Text style={{ fontSize: moderateScale(13), color: 'white', alignContent: 'flex-start' }}>Price:</Text>
                                    </View>

                                    <View>
                                        <View style={{ width: width * 0.45, height: moderateScale(40), borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-around' }}>
                                            <TextInput
                                                style={{ color: 'white', fontSize: moderateScale(14), marginLeft: moderateScale(5) }}
                                                keyboardType='numeric'
                                                maxLength={10}
                                                value={this.state.price}
                                                onChangeText={(text) => { this.setState({ price: common.numberWithDecimal(text) }) }}
                                            />
                                        </View>
                                        {!this.state.priceErr == '' ?
                                            <Text style={styles.errorMsg}>{this.state.priceErr}</Text> : null
                                        }
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: moderateScale(15) }}>

                                    <LinearGradient colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
                                        start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
                                        style={{ borderRadius: moderateScale(5) }}
                                    >
                                        <TouchableOpacity
                                            style={styles.buttonStyle}
                                            onPress={() => this.saveItemClicked()}
                                        >
                                            <Text style={styles.textStyle}>
                                                Save
                                </Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                    {/* <TouchableOpacity style={[styles.buttonStyle, { borderWidth: 1, borderColor: '#4a4eda', borderRadius: moderateScale(5), marginLeft: moderateScale(15) }]}>
                                <Text style={styles.textStyle}>
                                    Delete
                                </Text>
                            </TouchableOpacity> */}

                                </View>

                            </View>

                        </Overlay>

                    </ScrollView>

                </LinearGradient>


            </View>
        );
    }

}


const mapStateToProps = state => {
    const { user } = state.auth;
    const { watchListArr, commodityData, commodityList } = state.commDetails;
    return { user, watchListArr, commodityData, commodityList }
}

export default connect(mapStateToProps, { needToLoadPortfolioData })(Portfolio);

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    header: {
        // flex: 1,
        flexDirection: 'row',
        height: moderateScale(55),
        backgroundColor: '#4d4d4d',
        marginLeft: moderateScale(20),
        marginRight: moderateScale(20),
        // marginTop: verticalScale(15),
        marginBottom: moderateScale(10),
        borderRadius: moderateScale(5),
        borderWidth: 1,
        borderColor: '#59595a'
    },
    headerTabs: {
        justifyContent: 'center',
        flex: 1
    },
    priceTab: {
        alignItems: 'center',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        paddingRight: moderateScale(10),
        borderColor: '#59595a'
    },
    changeTab: {
        alignItems: 'flex-end',
        paddingRight: moderateScale(10)
    },
    headerText: {
        color: 'white',
        fontSize: moderateScale(14),
        // fontWeight: '500'
    },
    items: {
        flex: 1,
        flexDirection: 'row',
        height: moderateScale(55),
        backgroundColor: '#171717',
        marginLeft: moderateScale(20),
        marginRight: moderateScale(20),
        marginTop: moderateScale(5),
        borderRadius: moderateScale(5),
    },
    buttonStyle: {
        justifyContent: "center",
        alignItems: 'center',
        width: moderateScale(130),
        height: moderateScale(50),
    },
    textStyle: {
        color: 'white',
        fontFamily: 'Lato',
        fontSize: moderateScale(14),
        fontWeight: 'bold'
    },
    errorMsg: {
        color: '#e64545',
        fontSize: moderateScale(13)
    },
});

class GetPortfolioList {
    UserId;
    constructor() {

    }
}