import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet, Text, View, Platform, LayoutAnimation, UIManager, Keyboard, AsyncStorage } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Item, Input, Label, Row } from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { loginUser } from '../actions';
import Button from '../components/Button';
import common from '../common';
import ForgotPassword from "../ForgotPassword";
import { CryptoJS } from "../assets/js/aes";
import CountryPicker from "../components/CountryPicker";

class LoginView extends Component {
    state = {
        // username: '',
        mobileNo:'',
        email:'',
        password: '',
        emailErr: '',
        mobileErr:'',
        passwordErr: '',
        loginErr: '',
        hidePassword: true,
        action: 'mobile',
        isCCPickerOpen: false,
        countryCode: '+91', //Default should be India
        countryFlag: '🇮🇳', //Default should be India
    };

    // componentWillUpdate() {
    //     UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    //     LayoutAnimation.linear();
    // }
    animate() {
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        LayoutAnimation.linear();
    }

    componentWillReceiveProps(nextProps) {
        // console.log('componentWillReceiveProps LoginView');
        // console.log(this.props);
        // console.log(nextProps);
        this.setState({
            loginErr: nextProps.errorLogin
        });
        // this.animate();
        // if (this.props.user == null && nextProps.user != null) {
        //     this.props.route.navigation.navigate("SignedIn");
        // }

        if (this.props.user == null && nextProps.user != null) {
            let objCredentials = {};
            this.state.action == 'email' ? objCredentials.Email = this.state.email : (objCredentials.Mobile = this.state.mobileNo, objCredentials.CountryCode = this.state.countryCode);
            objCredentials.Password = this.state.password;
            var seckey = 'aNdRgUkXp2s5v8y/';
            console.log('objCredentials', objCredentials);
            var encrypted = CryptoJS.AES.encrypt((JSON.stringify(objCredentials)), seckey);
            AsyncStorage.setItem("loginCredential", encrypted.toString());
            this.props.route.navigation.navigate("SignedIn");
        }
    }


    resetErrors() {
        this.setState({
            emailErr: '',
            mobileErr:'',
            passwordErr: '',
            loginErr: ''
        });
    }

    login() {
        this.animate();
        this.resetErrors();
        var proceed = true;
        let isMobileNo = null;

        if(this.state.action == 'email'){
            isMobileNo = false;
            if(this.state.email.trim() == ''){
                this.setState({
                    emailErr: 'Please enter email id',
                });
                proceed = false;
            }else{
                if (!common.validateEmail(this.state.email.trim())) {
                    this.setState({
                        emailErr: 'Please enter valid email id',
                    });
                    proceed = false;
                }
            }

        }else if(this.state.action == 'mobile'){
            isMobileNo = true;
            if(this.state.mobileNo.trim() == ''){
                this.setState({
                    mobileErr: 'Please enter mobile number',
                });
                proceed = false;
            }else{
                if (!common.validateMobileNo(this.state.mobileNo.trim(),this.state.countryCode)) {
                    this.setState({
                        mobileErr: 'Please enter valid mobile number',
                    });
                    proceed = false;
                }
            }
        }

        if (this.state.password.trim() == '') {
            this.setState({
                passwordErr: 'Please enter password',
            });
            proceed = false;
        }


        if (proceed) {
            //Code to register & login 

            const req = new UserLogin();
            isMobileNo ? (req.Mobile = this.state.mobileNo, req.CountryCode = this.state.countryCode) : req.Email = this.state.email;
            req.Password = this.state.password;
            req.Device = '2';
            req.Platform = Platform.OS === 'ios' ? 2 : 1;

            console.log((JSON.stringify(req)));

            var serviceRequest = common.encryptData(JSON.stringify(req))

            var serviceBody = { request: serviceRequest };
            this.props.loginUser(serviceBody);

            // this.props.route.navigation.navigate("SignedIn");
        }
    }

    togglePassword = () => {
        Keyboard.dismiss();
        this.setState({ hidePassword: !this.state.hidePassword });
    }

    returnData(clearErr) {
        console.log('clearErr: ', clearErr);
        if (clearErr)
            this.resetErrors();
    }

    forgotPassword() {
        console.log(this.props);
        this.props.route.navigation.navigate("ForgotPassword", { returnData: this.returnData.bind(this) });
    }

    onSelect(index, value) {
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        LayoutAnimation.easeInEaseOut();
        this.setState({
            action: value
        });
    }

    renderEmailOrMobile() {
        if (this.state.action == 'email') {
            return (
                <View>
                    <Item floatingLabel style={[styles.placeholderText, { marginTop: moderateScale(10) }]}>
                        <Label style={styles.labelStyle}>EMAIL ID</Label>
                        <Input style={styles.inputText}
                            value={this.state.email}
                            onChangeText={(text) => {
                                this.animate();
                                this.setState({ email: text, emailErr: '' })
                            }}
                        />
                    </Item>
                    {!this.state.emailErr == '' ?
                        <Text style={styles.errorMsg}>{this.state.emailErr}</Text> : null
                    }
                </View>
            );
        }
        return (
            <View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View
                        style={styles.countryCode}
                    >
                        <TouchableOpacity
                            style={{ flexDirection: 'row', }}
                            onPress={() => this.setState({ isCCPickerOpen: true })}
                        >
                            <Text style={styles.inputText}>{this.state.countryFlag} {this.state.countryCode}</Text>
                            <Icon name='angle-down'
                                size={moderateScale(15)}
                                style={{ color: '#fff', marginLeft: moderateScale(5), paddingRight: moderateScale(5) }}
                            />
                        </TouchableOpacity>
                    </View>
                    <CountryPicker
                        modalVisible={this.state.isCCPickerOpen}
                        isClosed={() => {
                            console.log('isCloseCalled');
                            this.setState({ isCCPickerOpen: false })
                        }}
                        countryChanged={(countryCode, countryFlag) => {
                            console.log('countryChanged', countryCode, countryFlag);
                            this.setState({ countryCode, countryFlag })
                        }}
                    />
                    <View style={{ flex: 0.75 }}>
                        <Item floatingLabel style={[styles.placeholderText, { marginTop: moderateScale(10) }]}>
                            <Label style={styles.labelStyle}>MOBILE NUMBER</Label>
                            <Input style={styles.inputText}
                                keyboardType='numeric' 
                                maxLength={this.state.countryCode == '+91'? 10 : 16}
                                value={this.state.mobileNo}
                                onChangeText={(text) => {
                                    this.animate();
                                    this.setState({ mobileNo: text, mobileErr: '' })
                                }}
                            />
                        </Item>
                    </View>
                </View>
                {!this.state.mobileErr == '' ?
                    <Text style={styles.errorMsg}>{this.state.mobileErr}</Text> : null
                }
            </View>
        );
    }

    render() {
        return (
            <View>
                {/* <ScrollView> */}
                <View
                    style={{ fontWeight: 'bold', color: 'white', paddingLeft: scale(20), paddingRight: scale(20), }}>

                    {/* <Text style={{color:'#fff', fontSize:moderateScale(16), marginTop:moderateScale(20), marginBottom:moderateScale(-10)}}>Login via</Text> */}

                    <RadioGroup
                        size={moderateScale(18)}
                        thickness={moderateScale(2)}
                        color='#fff'
                        style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', marginTop: moderateScale(10) }}
                        // highlightColor='#ccc8b9'
                        selectedIndex={this.state.action == 'mobile' ? 0 : 1}
                        onSelect={(index, value) => this.onSelect(index, value)}
                    >
                        <RadioButton
                            value='mobile'
                            color='#d9fff1'
                        >
                            <Text style={{ fontSize: moderateScale(14), color: '#ffffff' }}>MOBILE</Text>
                        </RadioButton>
                        <RadioButton
                            value='email'
                            color='#d9fff1'
                        >
                            <Text style={{ fontSize: moderateScale(14), color: '#ffffff' }}>EMAIL</Text>
                        </RadioButton>
                    </RadioGroup>

                    {this.renderEmailOrMobile()}

                    {/* <Item floatingLabel style={[styles.placeholderText, { marginTop: moderateScale(5) }]}>
                        <Label style={styles.labelStyle}>EMAIL ID/MOBILE NUMBER</Label>
                        <Input style={styles.inputText}
                            value={this.state.username}
                            onChangeText={(text) => {
                                this.animate();
                                this.setState({ username: text, usernameErr: '' })
                            }}
                        />
                    </Item> */}

                    <Item floatingLabel style={[styles.placeholderText]}>
                        <Label style={styles.labelStyle}>PASSWORD</Label>
                        <Input style={styles.inputText} secureTextEntry={this.state.hidePassword}
                            value={this.state.password}
                            onChangeText={(text) => {
                                this.animate();
                                this.setState({ password: text, passwordErr: '' })
                            }}
                        />

                    </Item>
                    <TouchableOpacity
                        style={styles.eyeBtn}
                        onPress={this.togglePassword}
                    >
                        {/* <Image
                            source={(this.state.hidePassword) ? require('../assets/imgs/eyeIcon.png') : require('../assets/imgs/eyeIconOpen.png')}
                            style={styles.eyeBtnImage}
                        ></Image> */}
                        <Icon name={(this.state.hidePassword) ? 'eye-slash' : 'eye'}
                            size={moderateScale(15)}
                            style={{ color: 'white' }}
                            light
                        />
                    </TouchableOpacity>
                    {!this.state.passwordErr == '' ?
                        <Text style={[styles.errorMsg, { marginTop: moderateScale(5) }]}>{this.state.passwordErr}</Text> : null
                    }
                    {!this.state.loginErr == '' ?
                        <Text style={[styles.errorMsg, { paddingTop: moderateScale(5) }]}>{this.state.loginErr}</Text> : null
                    }
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: moderateScale(50) }}>
                    <Button
                        whenPressed={() => this.login()}>
                        SUBMIT
                </Button>
                    <TouchableOpacity
                        style={{ alignSelf: 'center', paddingRight: scale(20) }}
                        onPress={() => this.forgotPassword()}
                    >
                        <Text style={{ color: 'white', fontSize: moderateScale(13) }}>Forgot Password</Text>

                    </TouchableOpacity>
                </View>


                {/* </ScrollView> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    placeholderText: {
        borderColor: '#91ace1',
        fontSize: moderateScale(13),
        marginTop: verticalScale(20)
    },
    labelStyle: {
        color: '#cdeff5',
        fontSize: moderateScale(14)
    },
    inputText: {
        color: '#fff',
        fontSize: moderateScale(14)
    },
    errorMsg: {
        color: '#910a0a',
        fontSize: moderateScale(13)
    },
    eyeBtn: {
        alignSelf: 'flex-end',
        height: moderateScale(20),
        width: moderateScale(20),
        marginTop: moderateScale(-25)
    },
    eyeBtnImage:
    {
        resizeMode: 'contain',
        height: '100%',
        width: '100%'
    },
    countryCode: {
        height: moderateScale(35),
        flex: 0.25,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        borderColor: '#91ace1',
        borderBottomWidth: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    }
});

const mapStateToProps = state => {
    // console.log(state)
    const { errorLogin, loading, user } = state.auth
    return { errorLogin, loading, user }
}

export default connect(mapStateToProps, { loginUser })(LoginView);


class UserLogin {
    Mobile;
    Password;
    CountryCode;
    Device;
    Email;
    Platform;

    constructor() { }
}