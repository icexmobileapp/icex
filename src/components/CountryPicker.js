import React from 'react'
import {
  View,
  Text,
  Modal,
  FlatList,
  StyleSheet,
  SafeAreaView,
  TextInput,
  ScrollView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Dimensions
} from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import Icon from "react-native-vector-icons/FontAwesome";
import { Container, Header, Content, Input, Item } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import data from './Countries';

const { height, width } = Dimensions.get('window');

export default class CountryPicker extends React.Component {

  state = {
    searchTerm: '',
    enableScrollViewScroll: false
  }

  async getCountry(country) {
    const countryData = await data
    try {
      const countryCode = await countryData.filter(
        obj => obj.name === country
      )[0].dial_code
      const countryFlag = await countryData.filter(
        obj => obj.name === country
      )[0].flag

      this.setState({ searchTerm: '', enableScrollViewScroll: true });

      this.props.countryChanged(countryCode, countryFlag);
      this.props.isClosed();
    }
    catch (err) {
      console.log(err)
    }
  }

  render() {
    let countryData;
    this.state.searchTerm == '' ? countryData = data : countryData = data.filter((data) => {
      if (data.name.toLowerCase().indexOf(this.state.searchTerm.toLowerCase()) > -1)
        return data.name.toLowerCase().indexOf(this.state.searchTerm.toLowerCase()) > -1;
      return data.dial_code.toLowerCase().indexOf(this.state.searchTerm.toLowerCase()) > -1;
    });
    return (
      <Modal
        transparent={true}
        animationType={'none'}
        visible={this.props.modalVisible}
        onRequestClose={this.props.isClosed}>

        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          contentContainerStyle={styles.modalBackground}
          scrollEnabled={this.state.enableScrollViewScroll}
          ref={myScroll => (this._myScroll = myScroll)}>


          <View
            onStartShouldSetResponderCapture={() => {
              this.setState({ enableScrollViewScroll: false });
              if (this._myScroll.contentOffset === 0
                && this.state.enableScrollViewScroll === false) {
                this.setState({ enableScrollViewScroll: true });
              }
            }}
            style={styles.outerViewStyle}>

            <Item
              style={styles.searchInputStyle}
              rounded>
              <Input
                placeholder='Search country'
                onChangeText={text => this.setState({ searchTerm: text })}
                value={this.state.searchTerm}
              />
              <Icon name='search'
                size={verticalScale(20)}
                style={{ color: 'black' }}
                light
              />
            </Item>

            <FlatList
              keyboardShouldPersistTaps={'handled'}
              data={countryData}
              keyExtractor={(item, index) => index.toString()}
              removeClippedSubviews={false}
              renderItem={
                ({ item }) =>
                  <TouchableOpacity
                    onPress={() => this.getCountry(item.name)}>
                    <View
                      style={styles.countryStyle}>
                      <Text style={{ fontSize: moderateScale(25), color: 'black' }}>
                        {item.flag}
                      </Text>
                      <Text style={{ fontSize: moderateScale(15), color: 'black', flexShrink: 1 }}>
                        {item.name} ({item.dial_code})
                      </Text>
                    </View>
                  </TouchableOpacity>
              }
            />
            <TouchableOpacity
              onPress={() => {
                this.setState({ searchTerm: '', enableScrollViewScroll: true });
                this.props.isClosed();
              }}
              style={styles.closeButtonStyle}>
              <LinearGradient colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
                start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
                style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: moderateScale(35), borderRadius: moderateScale(5) }}
              >
                <Text style={styles.textStyle}>
                  Cancel
                          </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>

        </ScrollView>

      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  textStyle: {
    color: '#fff',
    fontFamily: 'Lato',
    fontSize: moderateScale(14),
    fontWeight: 'bold'
  },
  modalBackground: {
    height,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)'
  },
  outerViewStyle: {
    alignSelf: 'center',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    height: height / 1.4,
    width: width - moderateScale(50),
    // borderRadius: moderateScale(50), 
  },
  searchInputStyle: {
    flexDirection: 'row',
    marginBottom: moderateScale(10),
    borderRadius: moderateScale(5),
    alignItems: 'center',
    justifyContent: "space-between",
    backgroundColor: '#fff',
    paddingLeft: moderateScale(10),
    paddingRight: moderateScale(15),
    height: moderateScale(35),
  },
  countryStyle: {
    flex: 1,
    backgroundColor: '#fff',
    borderColor: '#adadad',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // borderWidth: 0.5,
    marginBottom: moderateScale(5),
    paddingLeft: moderateScale(15),
    paddingRight: moderateScale(15),
    borderRadius: moderateScale(5)
  },
  closeButtonStyle: {
    // flex: 1,
    marginTop: moderateScale(10),
    borderRadius: moderateScale(5),
    alignItems: 'center',
    justifyContent: "center",
    backgroundColor: '#fff',
    height: moderateScale(35),
  }
})