import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import FAIcon from "react-native-vector-icons/FontAwesome";
import SLIcon from "react-native-vector-icons/SimpleLineIcons";
import { connect } from 'react-redux';
import { openEditProfile } from '../actions';
import common from "../common";

class MenuDrawer extends Component {

    navLink(nav, text) {
        return (
            <TouchableOpacity
                style={{ height: moderateScale(50) }}
                onPress={() => {
                    this.props.navigation.navigate(nav);
                    this.props.navigation.closeDrawer()
                }}
            >
                <Text style={styles.link}>{text}</Text>
            </TouchableOpacity>
        )
    }
    profileImage() {
        if (this.props.user.ProfileImage != '' && this.props.user.ProfileImage != null) {
            return (
                <Image style={{
                    alignSelf: 'center',
                    height: moderateScale(35),
                    width: moderateScale(35),
                    borderRadius: 50,
                }}
                    source={{ uri: this.props.user.ProfileImage }}
                />

            )
        } else {
            return (
                <FAIcon name='user-circle'
                    size={moderateScale(35)}
                    style={{ alignSelf: 'center', color: '#4e4e4e' }}
                />
            )
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topSection}>
                    <View style={{ flex: 1, justifyContent: 'center', }}>
                        {/* <Image style={{
                            borderWidth: 1, borderColor: 'white',
                            height: moderateScale(35,0.8),
                            width: moderateScale(35,0.8),
                            borderRadius: 50,

                        }} source={require('../assets/images/defaultPic.png')} /> */}
                        {this.profileImage()}

                    </View>

                    <View style={{ flex: 3, justifyContent: 'center' }}>
                        <Text style={{ fontSize: moderateScale(15), color: 'white', paddingLeft: moderateScale(5) }}>
                            {this.props.user.Name}
                        </Text>
                        <Text style={{ fontSize: moderateScale(12), color: '#818181', paddingLeft: moderateScale(5) }}>
                            {this.props.user.Email}
                        </Text>
                    </View>
                    <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.closeDrawer();
                                this.props.navigation.navigate('EditProfile', { navigation: this.props.navigation });
                            }}
                        >
                            {/* <Image style={{
                            height: moderateScale(25),
                            width: moderateScale(25),
                        }} source={require('../assets/images/edit.png')} /> */}
                            <SLIcon name='pencil'
                                size={moderateScale(20)}
                                style={{ alignSelf: 'center', color: '#4e4e4e' }}
                            />
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={styles.buttonsView}>
                    {this.navLink('Home', 'HOME')}
                    {this.navLink('Portfolio', 'PORTFOLIO')}
                    {this.navLink('WatchList', 'WATCHLIST')}
                    {this.navLink('UserPreference', 'PREFERENCE')}
                    {this.navLink('AboutUs', 'ABOUT US')}
                    {this.navLink('Settings', 'SETTINGS')}
                </View>

                <View style={styles.versionView}>
                    <Text style={styles.versionText}>v {common.appVersion}</Text>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
    }, link: {
        flex: 1,
        color: 'white',
        fontSize: moderateScale(14),
        paddingLeft: moderateScale(10),
        margin: 5,
        textAlign: 'left'
    }, buttonsView: {
        marginLeft: moderateScale(30),
        marginRight: moderateScale(30),
        paddingBottom: moderateScale(20),
        borderColor: '#484848',
        borderBottomWidth: 1,
        // paddingTop: 10,
    }, topSection: {
        flexDirection: 'row',
        paddingTop: moderateScale(20),
        paddingBottom: moderateScale(20),
        borderColor: '#484848',
        borderBottomWidth: 1,
        margin: moderateScale(30),
        // marginRight:20
    }, versionView: {
        marginTop: moderateScale(20),
        marginLeft: moderateScale(40),
        marginRight: moderateScale(30),
        paddingBottom: moderateScale(20),
    }, versionText: {
        fontSize:moderateScale(13),
        color:'#9e9e9e'
    }
});

const mapStateToProps = state => {
    console.log(state)
    const { user } = state.auth
    return { user }
}
export default connect(mapStateToProps, { openEditProfile })(MenuDrawer);