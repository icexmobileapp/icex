
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

const Button = ({ whenPressed, children,style }) => {
    const { buttonStyle, textStyle, button1Style } = styles;
    return (

        <TouchableOpacity onPress={whenPressed} style={[style,buttonStyle]}>
                <Text style={textStyle}>
                    {children}
                </Text>
        </TouchableOpacity>

    );
};

const styles = {
    buttonStyle: {
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: scale(5),
        marginLeft: scale(20),
        marginRight: scale(35),
        width: moderateScale(135),
        height: moderateScale(50),
    },
    textStyle: {
        color: '#3539ae',
        fontFamily:'Lato',
        fontSize: moderateScale(14),
        fontWeight: 'bold'
    }
};

export default Button;
