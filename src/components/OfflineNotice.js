import React, { PureComponent } from 'react';
import { View, Text, NetInfo, Dimensions, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { changeInternetConnectivity } from '../actions'


const { width } = Dimensions.get('window');

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}

class OfflineNotice extends PureComponent {

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
        console.log(isConnected);
      });
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }

  handleConnectivityChange = isConnected => {
      console.log(isConnected);
    if (isConnected) {
      this.props.changeInternetConnectivity(isConnected);
      // this.setState({ isConnected });
    } else {
      this.props.changeInternetConnectivity(isConnected);
      // this.setState({ isConnected });
    }
  };

  render() {
    if (!this.props.isInternetConnected) {
      return <MiniOfflineSign />;
    }
    return null;
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    // position: 'absolute',
    // zIndex: 100000000
    // top: 30
  },
  offlineText: { color: '#fff' }
});


const mapStateToProps = (state) => {
  const { isInternetConnected } = state.auth
  return { isInternetConnected }
}


export default connect(mapStateToProps, { changeInternetConnectivity })(OfflineNotice);