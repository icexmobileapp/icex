import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, Dimensions, UIManager, LayoutAnimation, Image, AsyncStorage } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import Overlay from 'react-native-modal-overlay';
import Icon from "react-native-vector-icons/FontAwesome";
import SLIcon from "react-native-vector-icons/SimpleLineIcons";
import { StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { resetState, CanCallService } from '../actions';
import common from '../common';

const { height, width } = Dimensions.get('window');
// console.log(Dimensions.get('window'))
iconSize = (height + width) / 55;

class Header extends Component {

    state = {
        ImageSource: this.props.user.ProfileImage,
        modalVisible: false,
        username: this.props.user.Name,
        emailId: this.props.user.Email,
        mobileNo: this.props.user.Mobile,
        pincode: this.props.user.Location,
    }

    onClose() {
        this.setState({
            modalVisible: false
        });
    }

    renderBackButton() {
        if (this.props.backBtn) {

            return (
                <TouchableOpacity
                    onPress={() => { this.props.navigation.goBack() }}
                    style={styles.iconPadding}
                >
                    <Icon name='angle-left'
                        size={verticalScale(25)}
                        style={styles.menuIcon}
                        light
                    />
                </TouchableOpacity>
            )

        }
        return (
            <TouchableOpacity
                onPress={() => { this.props.navigation.toggleDrawer() }}

                style={styles.iconPadding}
            >
                {/* <Icon name='bars'
                    size={iconSize}
                    style={styles.menuIcon}
                /> */}
                <Image
                    style={{ height: moderateScale(23), width: moderateScale(23) }}
                    source={require('../assets/images/menu.png')} />
            </TouchableOpacity>
        )
    }

    logout() {
        // console.log(this.props);
        this.props.resetState();
        this.props.CanCallService(false);
        AsyncStorage.setItem("loginCredential", '');
        this.props.navigation.navigate("SignedOut");
    }

    callHome() {
        this.props.navigation.dispatch(StackActions.popToTop());
        this.props.navigation.navigate("HomePage")
    }

    profileImage() {
        console.log('ProfileIcon', this.props.user.profileImage);
        if (this.props.user.ProfileImage != '' && this.props.user.ProfileImage != null) {
            return (
                <Image style={{
                    alignSelf: 'center',
                    height: moderateScale(25),
                    width: moderateScale(25),
                    borderRadius: 50,
                }}
                    source={{ uri: this.props.user.ProfileImage }}
                />

            )
        } else {
            return (
                <Icon name='user-circle'
                    size={moderateScale(25)}
                    style={{ color: '#4e4e4e' }}
                />
            )
        }

    }

    render() {

        return (
            <View style={styles.container}>

                <View style={{ flex: 1 }}>

                    {this.renderBackButton()}

                </View>
                <View style={{ flex: 6 }}>
                    <TouchableOpacity
                        onPress={() => { this.callHome() }}
                    >
                        <Image
                            style={styles.logoStyle}
                            source={require('../assets/images/Logo.png')} />
                    </TouchableOpacity>
                </View>

                <View style={styles.profileView}>

                    {/* <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: true })}
                    >
                        <Icon name='bell'
                            size={iconSize}
                            style={styles.ProfileIcon}
                        />
                    </TouchableOpacity> */}

                    {this.profileImage()}

                    <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: true })}
                        style={{ padding: moderateScale(10) }}
                    >
                        <Icon name='angle-down'
                            size={iconSize}
                            style={styles.ProfileIcon}
                        />
                    </TouchableOpacity>
                </View>

                {/* User Details on v icon Click */}
                <Overlay
                    visible={this.state.modalVisible}
                    onClose={() => { this.onClose() }}
                    closeOnTouchOutside
                    containerStyle={{
                        backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'flex-start', paddingRight: 0, paddingTop: 0
                    }}
                    childrenWrapperStyle={{
                        alignSelf: 'flex-end',
                        backgroundColor: '#202020',
                        marginTop: moderateScale(60),
                    }}
                >
                    <View style={{ width: width * 0.5 }}>
                        <View style={{ borderBottomColor: '#393939', borderBottomWidth: 1 }}>
                            <Text style={styles.userNameText}>{this.props.user.Name}</Text>
                            <Text style={styles.emailText}>{this.state.emailId}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        modalVisible: false,
                                    });
                                    this.props.navigation.navigate('EditProfile', { navigation: this.props.navigation });
                                }}
                            >
                                <Text style={styles.editProfileText}>EDIT PROFILE</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => { this.logout() }}
                            >
                                <Text style={styles.logoutText}>LOGOUT</Text>
                            </TouchableOpacity>

                        </View>


                    </View>

                </Overlay>

            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: 'black',
        height: moderateScale(60),
        flexDirection: 'row',
        paddingRight: scale(20),
        paddingLeft: scale(20),
        zIndex: 10000000000000
    },
    img: {
        alignSelf: 'center',
        height: moderateScale(25),
        width: moderateScale(25),
        borderRadius: 50,
    },
    ProfileIcon: {

        alignSelf: 'center',
        color: 'white'
    },
    menuIcon: {
        color: 'white'
    },
    logoStyle: {
        height: moderateScale(26),
        width: moderateScale(92),
    },
    iconPadding: {
        paddingTop: moderateScale(5),
        paddingBottom: moderateScale(5),
        paddingRight: moderateScale(5)
    },
    profileView: {
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1.3,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    userNameText: {
        color: '#ffff',
        fontSize: moderateScale(14),
        paddingBottom: moderateScale(16),
        paddingLeft: scale(10)
    },
    emailText: {
        color: '#727272',
        fontSize: moderateScale(11),
        paddingBottom: moderateScale(27),
        paddingLeft: scale(10)
    },
    editProfileText: {
        color: '#5a5dc4',
        fontSize: moderateScale(11),
        paddingTop: moderateScale(27),
        paddingLeft: scale(10)
    },
    logoutText: {
        color: '#5a5dc4',
        fontSize: moderateScale(11),
        fontWeight: 'bold',
        paddingTop: moderateScale(27),
        paddingRight: scale(10)
    }
});


const mapStateToProps = state => {
    const { user } = state.auth;
    return { user }
}

export default connect(mapStateToProps, { resetState, CanCallService })(Header);