import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Dimensions, UIManager, LayoutAnimation, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Toast } from 'native-base';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { connect } from 'react-redux';
import { StackActions } from 'react-navigation';
import Header from './components/Header';
import CheckBox from 'react-native-check-box';
import common from './common';
import Loader from "./components/Loader";

const DeviceWidth = Dimensions.get('window').width;
const ViewWidth = DeviceWidth - scale(20) - scale(20);

class UserPreference extends Component {

    state = {
        commodityList: [],
        isChecked: false,
        commodityListErr: '',
        loading: false,
        userPreferenceList: [],
        userPreferenceListErr: '',
        backButton: this.props.navigation.getParam("fromHomePage", false)
    }

    // componentWillUpdate() {
    //     UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    //     LayoutAnimation.spring();
    // }

    componentWillMount() {
        this.getCommodityList();
    }

    getCommodityList() {
        this.setState({
            loading: true,
        });
        common.callService('GetCommodityList_v2', '')
            .then(CommodityList => {
                console.log("CommodityList", CommodityList);
                if (CommodityList.ReturnCode == '1') {
                    let data = CommodityList.Data
                    data.forEach((obj, index) => {
                        obj.checked = false
                        if (index == data.length - 1) {
                            this.setState({
                                commodityList: data,
                                commodityListErr: '',
                            });
                        }
                    });
                    if (this.state.backButton) {
                        this.setState({
                            userPreferenceList: [],
                            userPreferenceListErr: '',
                            loading: false,
                        });
                    } else {
                        this.getUserPreferenceList();
                    }
                } else {
                    this.setState({
                        commodityList: [],
                        commodityListErr: 'Sorry, Some unexpected error occured',
                        loading: false,
                    });
                }

            })
            .catch(err => {
                console.log(err);
                this.setState({
                    commodityList: [],
                    commodityListErr: 'Sorry, Some unexpected error occured',
                    loading: false,
                });
            })
    }

    getUserPreferenceList() {
        const req = { "UserId": this.props.user.Id }

        console.log("AllComments", req);
        var serviceRequest = common.encryptData(JSON.stringify(req));
        var serviceBody = { request: serviceRequest };
        console.log('serviceBody', serviceBody);

        common.callService('GetUserPreferenceCommodity_v2', serviceBody)
            .then(UserPreferenceCommodity => {
                console.log("UserPreferenceCommodity", UserPreferenceCommodity);
                if (UserPreferenceCommodity.ReturnCode == '1') {

                    let data = this.state.commodityList;
                    let userData = UserPreferenceCommodity.Data;
                    if (userData.length > 0) {
                        this.setState({
                            loading: false,
                        });
                        for (let i = 0; i < data.length; i++) {
                            for (let j = 0; j < userData.length; j++) {
                                if (data[i].Commodity == userData[j]) {
                                    data[i].checked = true;
                                    if (i != data.length - 1)
                                        break;
                                }
                                if (i == data.length - 1) {
                                    this.setState({
                                        userPreferenceList: UserPreferenceCommodity.Data,
                                        commodityList: data,
                                        userPreferenceListErr: '',
                                        loading: false,
                                    });
                                    setTimeout(() => {
                                        console.log(this.state);
                                    }, 5);
                                }
                            }
                        }
                    } else {
                        this.setState({
                            userPreferenceList: [],
                            userPreferenceListErr: '',
                            loading: false,
                        });
                    }

                } else {
                    this.setState({
                        userPreferenceList: [],
                        userPreferenceListErr: 'Sorry, Some unexpected error occured',
                        loading: false,
                    });
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    userPreferenceList: [],
                    userPreferenceListErr: 'Sorry, Some unexpected error occured',
                    loading: false,
                });
            })
    }

    savePreference() {
        if (this.state.userPreferenceList.length != 0) {

            console.log(this.state.commodityList);
            const req = {
                "UserId": this.props.user.Id,
                "Commodity": this.state.userPreferenceList
            }

            console.log("savePreference", req);
            var serviceRequest = common.encryptData(JSON.stringify(req));
            var serviceBody = { request: serviceRequest };
            console.log('serviceBody', serviceBody);
            this.setState({
                loading: true,
            });

            common.callService('SaveUserPreferences', serviceBody)
                .then(SaveUserPreferences => {
                    console.log(SaveUserPreferences);
                    if (SaveUserPreferences.ReturnCode == '1') {
                        setTimeout(() => {
                            Toast.show({
                                text: "Preference Updated successfully",
                                type: "success"
                            });
                        }, 10)
                        setTimeout(() => {
                            this.props.navigation.dispatch(StackActions.popToTop());
                            this.props.navigation.navigate("HomePage")
                        }, 1000);
                        this.setState({
                            loading: false,
                        });

                    } else {
                        setTimeout(() => {
                            Toast.show({
                                text: "Sorry , Unable to update preference",
                                type: "danger"
                            });
                        }, 10)
                        this.setState({
                            loading: false,
                        });
                    }
                })
                .catch(err => {
                    console.log(err);
                    setTimeout(() => {
                        Toast.show({
                            text: "Sorry , Unable to update preference",
                            type: "danger"
                        });
                    }, 10)
                    this.setState({
                        loading: false,
                    });
                })
        } else {
            setTimeout(() => {
                Toast.show({
                    text: "Please select at least one commodity",
                    type: "danger"
                });
            }, 10)
        }
    }

    renderList(commList) {
        // console.log("commList", commList);
        let borderWidth = 0;
        commList.index == this.state.commodityList.length - 1 ? borderWidth = 0 : borderWidth = 0.5;
        return (
            <View style={[styles.checkBoxView, { borderBottomWidth: borderWidth }]}>
                <CheckBox
                    style={{ flex: 1 }}
                    onClick={() => {
                        let tempCommodityList = this.state.commodityList;
                        let tempUserPreferenceList = this.state.userPreferenceList;
                        let selectAllBtnValue = this.state.isChecked;
                        var index = tempUserPreferenceList.indexOf(commList.item.Commodity);
                        index == -1 ? tempUserPreferenceList.push(commList.item.Commodity) : tempUserPreferenceList.splice(index, 1);
                        console.log(commList.item.checked)
                        commList.item.checked ? selectAllBtnValue = false : selectAllBtnValue;
                        tempCommodityList[commList.index].checked = !commList.item.checked;
                        this.setState({
                            commodityList: tempCommodityList,
                            userPreferenceList: tempUserPreferenceList,
                            isChecked: selectAllBtnValue
                        })
                    }}
                    isChecked={commList.item.checked}
                    checkedCheckBoxColor={'#6f73ff'}
                    uncheckedCheckBoxColor={'#606161'}
                    imgWidth={moderateScale(35)}
                    imgHeight={moderateScale(35)}
                    rightTextView={
                        <View style={{ flex: 1, marginLeft: moderateScale(20) }}>
                            <Text style={{ fontSize: moderateScale(16), color: 'white' }}>{commList.item.Commodity}</Text>
                        </View>
                    }
                // checkedImage={<Image source={require('../../page/my/img/ic_check_box.png')} style={this.props.theme.styles.tabBarSelectedIcon} />}
                // unCheckedImage={<Image source={require('../../page/my/img/ic_check_box_outline_blank.png')} style={this.props.theme.styles.tabBarSelectedIcon} />}
                />
            </View>
        );
    }

    getLoader() {
        if (this.state.loading) {
            return (
                <Loader />
            );
        }
        return null;
    }

    selectAllClicked() {
        let data = this.state.commodityList;
        let tempUserPreferenceList = [];
        if (this.state.isChecked) {
            data.forEach((obj, index) => {
                obj.checked = false;
                if (index == data.length - 1) {
                    this.setState({
                        commodityList: data,
                        isChecked: false,
                        userPreferenceList: tempUserPreferenceList
                    });
                }
            });
        } else {
            data.forEach((obj, index) => {
                obj.checked = true;
                tempUserPreferenceList.push(obj.Commodity);
                if (index == data.length - 1) {
                    this.setState({
                        commodityList: data,
                        isChecked: true,
                        userPreferenceList: tempUserPreferenceList
                    });
                }
            });
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation} backBtn={this.state.backButton} />
                <LinearGradient
                    colors={['#303132', '#171717']}
                    // locations={[0,0.5,0.9]}
                    start={{ x: 0.5, y: 0.5 }} end={{ x: 1.0, y: 1.0 }}
                    style={{ flex: 1 }}
                >
                    {this.getLoader()}
                    <View style={{ marginLeft: moderateScale(20), paddingTop: moderateScale(20), paddingBottom: moderateScale(10) }}>
                        <Text style={{ fontSize: moderateScale(26), color: 'white' }}>What is your preference?</Text>
                    </View>

                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >


                        <View style={[styles.checkBoxView, { borderBottomWidth: 0.5 }]}>
                            <CheckBox
                                style={{ flex: 1 }}
                                onClick={() => this.selectAllClicked()}
                                isChecked={this.state.isChecked}
                                checkedCheckBoxColor={'#6f73ff'}
                                uncheckedCheckBoxColor={'#606161'}
                                imgWidth={moderateScale(35)}
                                imgHeight={moderateScale(35)}
                                rightTextView={
                                    <View style={{ flex: 1, marginLeft: moderateScale(20) }}>
                                        <Text style={{ fontSize: moderateScale(16), color: 'white' }}>SELECT ALL</Text>
                                    </View>
                                }
                            />
                        </View>

                        <FlatList
                            data={this.state.commodityList}
                            renderItem={(commList) => this.renderList(commList)}
                            keyExtractor={(commList, index) => String(commList + index)}
                            removeClippedSubviews={false}
                            extraData={this.state}
                        />

                    </ScrollView>

                    <TouchableOpacity
                        // style={styles.buttonStyle}
                        style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: moderateScale(45) }}
                        onPress={() => this.savePreference()}
                    >
                        <LinearGradient colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
                            start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
                            style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: moderateScale(45) }}
                        >
                            <Text style={styles.buttonTextStyle}>
                                SAVE
                                </Text>
                        </LinearGradient>
                    </TouchableOpacity>


                </LinearGradient>

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    checkBoxView: {
        marginLeft: moderateScale(20),
        marginRight: moderateScale(20),
        paddingBottom: moderateScale(20),
        paddingTop: moderateScale(20),
        borderBottomColor: '#606060',
    },
    // buttonStyle: {
    //     justifyContent: "center",
    //     alignItems: 'center',
    //     // marginRight: scale(20),
    //     width: moderateScale(95),
    //     height: moderateScale(50),
    // },
    buttonTextStyle: {
        color: 'white',
        fontFamily: 'Lato',
        fontSize: moderateScale(14),
        fontWeight: 'bold'
    },
});

const mapStateToProps = state => {
    // const { commentsArray, loading, saveCommentDetails, errorSaveComment, errorGetComment } = state.commDetails;
    const { user } = state.auth
    return { user }
}

export default connect(mapStateToProps)(UserPreference);
