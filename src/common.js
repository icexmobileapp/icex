import { CryptoJS } from "./assets/js/aes";
import axios from 'axios';

//---------------------------App URL---------------------------------//

const URL = 'https://uat.icexindia.com/ICEXMobileService/';
// const URL = 'https://www.icexindia.com/ICEXMobileService/';

//-----Change MobileMarketData URL in actions/index.js -> (ActionCreator for CommodityData) also when changing above URL-----//

//--------------------------------------------------------------------//

const appVersion = '1.6';

class commonClass {

  appVersion = appVersion; //For exporting appVersion. Don't change this.

  getMonthName(month) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    return monthNames[month];
  }

  getDayName(day) {
    const dayNames = ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    return dayNames[day];
  }

  numberOnly(text) {
    return transformedInput = text.replace(/[^0-9]/g, '');
  }

  numberWithDecimal(text) {
    var transformedInput = text.replace(/[^0-9.]/g, '');
    var regex = /^[0-9]\d{0,9}(\.\d{0,2})?%?$/;
    if (regex.test(transformedInput)) {
      return transformedInput;
    } else {
      return newStr = transformedInput.substring(0, transformedInput.length - 1);
    }
  }

  charOnly(text) {
    return transformedInput = text.replace(/[^a-zA-Z ]/g, '');
  }

  alphaNumeric(text) {
    return transformedInput = text.replace(/[^a-zA-Z0-9]/g, '');
  }

  validateEmail(email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return true;
    }
    return false;
  }

  validatePassword(str) {
    // 8 and 20 characters; must contain at least one lowercase letter,
    //  one uppercase letter, one numeric digit, and one special character
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&()\\-`.+,/\"]).{8,20}/;
    return re.test(str);
  }

  validateMobileNo(str,cc) {
    if(String(cc).trim() == '+91'){
      var re = /^[0]?[6789]\d{9}$/;
      return re.test(str);
    }else{
      var re = /^[0-9]{4,16}$/;
      return re.test(str);
    }
  }

  addcommas(x) {

    if (x != undefined) {
      var isNegative = false;
      x = x.toString();
      if (x.indexOf("-") != -1) {
        x = x.slice(1)
        isNegative = true
      }
      var afterPoint = '';
      if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
      x = Math.floor(x);
      x = x.toString();
      var lastThree = x.substring(x.length - 3);
      var otherNumbers = x.substring(0, x.length - 3);
      if (otherNumbers != '')
        lastThree = ',' + lastThree;
      var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
      if (isNegative) {
        res = "-" + res
      }
      return res;
    } else {
      return null;
    }
  }

  addDecimals(x) {
    if (x != undefined) {
      var no = Number.parseFloat(x).toFixed(2);
      return this.addcommas(no);
    } else {
      return null;
    }
  }

  addOnlyDecimals(x){
    if (x != undefined) {
      return Number.parseFloat(x).toFixed(2);
    } else {
      return null;
    }
  }

  expiryDateToDisplay(expiry) {
    expiry = expiry.substr(0, 10);
    var splitDate = expiry.split('-');
    if (splitDate.count == 0) {
      return null;
    }

    var year = splitDate[0];
    var month = splitDate[1];
    var day = splitDate[2];

    return day + '-' + month + '-' + year;
    // expiry = expiry.substr(0, 10);

    // return expiry;
  }

  DdMmYyToDateObj(date) {
    var splitDate = date.split('-');
    if (splitDate.count == 0) {
      return null;
    }

    const [day, month, year] = splitDate;
    return new Date(year, month - 1, day)
  }

  percentChange(x, y) {
    if (x != undefined && y != undefined) {
      const res = (((x - y) / x) * 100).toFixed(2) + '%';
      return res;
    } else {
      return null;
    }
  }


  encryptData(request) {
    // var seckey = secretkey != "" && secretkey != null && secretkey != undefined ? secretkey : '8080808080808080';
    var seckey = 'aNdRgUkXp2s5v8y/';
    var seckey16 = seckey.substring(0, 16);
    var key = CryptoJS.enc.Utf8.parse(seckey16);
    var iv = CryptoJS.enc.Utf8.parse(seckey16);
    var encryptedData = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(request), key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
    return encryptedData.toString();
  }

  callService(method, body) {
    console.log(method, body);
    return new Promise(function (resolve, reject) {

      // fetch(URL+method, {
      //   method: 'POST',
      //   headers: {
      //     Accept: 'application/json',
      //     'Content-Type': 'application/json',
      //   },
      //   body: JSON.stringify(body),
      // })
      // .then(response => response.json())
      // .then((responseJson) => {
      //   console.log(responseJson);
      //   resolve(responseJson);
      //   // return responseJson.movies;
      // })
      // .catch((error) => {
      //   console.log(error);
      //   reject(error);
      // });

      axios.post(URL + method, body)
        .then(function (response) {
          // console.log('In CallService');
          // console.log(response);
          resolve(response.data);
          // return Promise.all(response);
        })
        .catch(function (error) {
          console.log('In Catch CallService');
          console.log(error.response);
          reject(error);
          // return Promise.reject(err);
        });
    }
    );
  }

  findMinMax(arrayOfObjects, key) {
    const values = arrayOfObjects.map(value => value[key]);
    return {
      min: Math.min.apply(null, values),
      max: Math.max.apply(null, values)
    };
  };

  removeOffsetFromDate(DateObj) {
    let hoursDiff = DateObj.getHours() - DateObj.getTimezoneOffset() / 60;
    let minutesDiff = (DateObj.getHours() - DateObj.getTimezoneOffset()) % 60;
    DateObj.setHours(hoursDiff);
    DateObj.setMinutes(minutesDiff);
    return DateObj;
  }

  epochToDateObj(epochdate) {
    var date = new Date(Number(epochdate));
    return date;
  }

}
const common = new commonClass();
export default common;