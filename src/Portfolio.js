import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, TextInput, TouchableOpacity, Dimensions, UIManager, LayoutAnimation, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import Icon from "react-native-vector-icons/FontAwesome";
import { DatePicker, Toast } from 'native-base';
// import { Toast } from 'native-base';
import SLIcon from "react-native-vector-icons/SimpleLineIcons";
import Overlay from 'react-native-modal-overlay';
import { Dropdown } from 'react-native-material-dropdown';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { connect } from 'react-redux';
import { withNavigationFocus } from "react-navigation";
import Loader from "./components/Loader";
import Header from './components/Header';
import common from './common';
import { portfolioList, CanCallService, needToLoadPortfolioData } from "./actions";

const { height, width } = Dimensions.get('window');
iconSize = (height + width) / 55;

class Portfolio extends Component {

    state = {
        userPortfolio: [],
        commData: this.props.commodityData,
        selectedComm: this.props.commodityList[0].value,
        selectedCommIndex: 0,
        chosenDate: new Date(),
        expiryData: [],
        priceData: [],
        totalProfitLoss: '',
        selectedExpiry: '',
        selectedExpiryIndex: 0,
        modalVisible: false,
        loading: false,
        action: 'buy',
        quantity: '',
        price: '',
        quantityErr: '',
        priceErr: '',
        portfolioErr: '',
        transDateErr: ''
    }

    loadExpiry() {
        var expArray = [];
        var priceArr = [];
        this.props.commodityData[this.state.selectedCommIndex].forEach((commodity, index) => {
            let obj = { value: common.expiryDateToDisplay(commodity.ExpiryDate) };
            expArray.push(obj);
            priceArr.push(commodity.LastTradedPrice)
            if (index == this.props.commodityData[this.state.selectedCommIndex].length - 1) {
                console.log(expArray);
                this.setState({
                    priceData: priceArr,
                    expiryData: expArray,
                    selectedExpiry: expArray[0].value,
                    price: String(priceArr[0])
                });
            }
        });
    }

    componentWillMount() {
        this.loadExpiry();
        this.loadPortfolioData();
    }

    loadPortfolioData() {
        const req = new GetPortfolioList();
        req.UserId = this.props.user.Id;

        console.log((JSON.stringify(req)));

        var serviceRequest = common.encryptData(JSON.stringify(req))
        this.setState({
            loading: true,
            portfolioErr: ''
        });
        var serviceBody = { request: serviceRequest };
        console.log(serviceBody);

        common.callService('GetPortfolio', serviceBody)
            .then(portfolioList => {
                console.log('portfolioList', portfolioList);

                if (portfolioList.ReturnCode == '1') {
                    this.setState({
                        loading: false,
                        userPortfolio: [],
                        totalProfitLoss: '',
                    });
                    this.setPortfolioDataToDisplay(portfolioList);
                } else {
                    this.setState({
                        loading: false,
                        portfolioErr: 'Unable to fetch portfolio details'
                    });
                    // portfolioListFail(dispatch, portfolioList.ReturnMsg);
                }
            })
            .catch(err => {
                console.log('portfolioList', err);
                this.setState({
                    loading: false,
                    portfolioErr: 'Unable to fetch portfolio details'
                });
                // portfolioListFail(dispatch, 'Some unexpected error occured')
            })

        // this.props.portfolioList(serviceBody)
    }

    componentDidUpdate(prevProps) {
        console.log('componentDidUpdate CanCallService');
        console.log(prevProps.isFocused);
        console.log(this.props.isFocused);
        if (prevProps.isFocused !== this.props.isFocused) {
            this.props.CanCallService(this.props.isFocused);
        } else if (this.props.isFocused) {
            this.props.CanCallService(this.props.isFocused);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.needToLoadPortfolio) {
            this.loadPortfolioData();
            this.props.needToLoadPortfolioData(false);
        }
        this.setState({
            loading: false,
        });
    }

    setPortfolioDataToDisplay(portfolioList) {
        var portfolioData = [];
        portfolioList.Data.forEach((comm, index) => {
            const { CommodityId, ExpiryDate, NetValue, TotalQuantity, ContractMultiplier, LastTradedPrice } = comm;
            var DateObj = common.epochToDateObj(ExpiryDate.replace(/[^0-9]/g, ''));
            var expDateToDisplay = this.getDateToDisplay(DateObj);
            console.log(DateObj);

            var AveragePrice = 0;

            TotalQuantity != 0 ? AveragePrice = (Number(NetValue) / (Number(ContractMultiplier) * Number(TotalQuantity))) : AveragePrice;

            var selectedCommodityId = this.props.commodityList.findIndex(comm =>
                String(comm.value) == String(CommodityId))

            var selectedCommExpiryId = this.state.commData[selectedCommodityId].findIndex(comm =>
                String(new Date(comm.ExpiryDate)) == DateObj)

            var LTP = 0;

            selectedCommExpiryId != -1 ? LTP = this.state.commData[selectedCommodityId][selectedCommExpiryId].LastTradedPrice : LTP = LastTradedPrice;

            var currValue = Number(LTP) * Number(TotalQuantity) * Number(ContractMultiplier);
            var profitLoss = Number(currValue) - Number(NetValue);

            otherPortfolioDetails = {
                expDateToDisplay, AveragePrice, LTP, currValue, profitLoss
            }
            const newPortfolioDetails = Object.assign(comm, otherPortfolioDetails);

            portfolioData.push(newPortfolioDetails);

            if (index == portfolioList.Data.length - 1) {
                var totalPL = 0;
                portfolioData.forEach((portfolio, index) => {
                    totalPL += portfolio.profitLoss;
                    if (index == portfolioData.length - 1) {
                        console.log(totalPL);
                        this.setState({
                            totalProfitLoss: totalPL
                        });
                    }
                });
                this.setState({
                    loading: false,
                    userPortfolio: portfolioData
                });
            }
        });
    }

    onCommdityChange(commodity) {
        this.setState({
            selectedComm: commodity,
            selectedExpiryIndex: 0,
            selectedCommIndex: this.props.commodityList.findIndex(comm =>
                String(comm.value) == String(commodity))
        });
        setTimeout(() => {
            this.loadExpiry();
        }, 5)
    }

    onExpiryChange(expiry) {
        this.state.expiryData.find((exp, index) => {
            var x = String(exp.value) == String(expiry);
            if (x) {
                this.setState({
                    selectedExpiry: expiry,
                    selectedExpiryIndex: index,
                    price: String(Number(this.state.priceData[index]).toFixed(2))
                })
            }
        });
    }

    addItemClicked() {
        this.setState({
            modalVisible: true,
            action: 'buy',
            price: String(this.state.priceData[this.state.selectedExpiryIndex].toFixed(2))
        })
    }

    editItemClicked(commodity, ExpiryDate) {
        this.onCommdityChange(commodity);
        var expDateObj = common.epochToDateObj(ExpiryDate.replace(/[^0-9]/g, ''));
        var date = expDateObj.getDate();
        var year = expDateObj.getFullYear();
        var month = expDateObj.getMonth() + 1;
        String(month).length == 1 ? month = `0${month}` : month;
        String(date).length == 1 ? date = `0${date}` : date;
        var expiry = `${year}-${month}-${date}`;

        setTimeout(() => {
            this.onExpiryChange(expiry);
            this.addItemClicked();
        }, 15)
    }

    saveItemClicked() {
        this.setState({
            quantityErr: '',
            priceErr: ''
        });
        var canProceed = true;
        var regex = /^[0-9]\d{0,9}(\.\d{1,2})?%?$/;
        if (this.state.quantity.trim() == '') {
            canProceed = false;
            this.setState({
                quantityErr: 'Please enter quantity'
            });
        } else if (this.state.quantity == 0) {
            canProceed = false;
            this.setState({
                quantityErr: 'Quantity can not be zero'
            });
        }
        if (this.state.price.trim() == '') {
            canProceed = false;
            this.setState({
                priceErr: 'Please enter price'
            });
        } else if (!regex.test(this.state.price.trim())) {
            canProceed = false;
            this.setState({
                priceErr: 'Please enter valid price'
            });
        }

        if (canProceed) {
            console.log(this.state);
            var methodName = '';
            this.state.action == 'buy' ? methodName = 'BuyCommodity' : methodName = 'SellCommodity';
            console.log(this.state.selectedExpiry)

            let expDateObj = common.removeOffsetFromDate(common.DdMmYyToDateObj(this.state.selectedExpiry));
            let expDate = JSON.parse(JSON.stringify(expDateObj));
            console.log('expDate', expDate)
            let transDateObj = common.removeOffsetFromDate(new Date(this.state.chosenDate));
            let transDate = JSON.parse(JSON.stringify(transDateObj));

            let req = {
                'UserId': this.props.user.Id,
                'CommodityId': this.state.selectedComm,
                'ExpiryDate': expDate,
                'Quantity': this.state.quantity,
                'Price': this.state.price,
                'TransactionDate': transDate
            };
            console.log(req);
            // alert(JSON.stringify(req))
            var serviceRequest = common.encryptData(JSON.stringify(req));
            this.setState({
                loading: true,

            });
            var serviceBody = { request: serviceRequest };
            console.log(serviceBody);
            // alert(JSON.stringify(serviceBody))
            common.callService(methodName, serviceBody)
                .then(BuySellComm => {
                    console.log('BuySellComm', BuySellComm);
                    // alert(JSON.stringify(BuySellComm))
                    if (BuySellComm.ReturnCode == '1') {
                        this.setState({
                            loading: false,
                        });
                        this.setPortfolioDataToDisplay(BuySellComm);
                    } else {
                        setTimeout(() => {
                            Toast.show({
                                text: "Unable to complete transaction",
                                type: "danger"
                            });
                        }, 10)
                        this.setState({
                            loading: false,
                        });
                    }
                })
                .catch(err => {
                    console.log(err);
                    setTimeout(() => {
                        Toast.show({
                            text: "Unable to complete transaction",
                            type: "danger"
                        });
                    }, 10)
                    this.setState({
                        loading: false,
                    });
                });
            this.onClose();
        }
    }

    onClose = () => this.setState({ modalVisible: false });

    onSelect(index, value) {
        this.setState({
            action: value
        });
    }

    getDateToDisplay(DateObj) {
        var month = common.getMonthName(DateObj.getMonth()).substring(0, 3);
        var year = DateObj.getFullYear();
        var date = DateObj.getDate();
        return `${date} ${month} ${year}`
    }

    detailsClicked(CommodityId, ExpiryDate) {
        var itemDetailObj = {
            CommodityId, ExpiryDate
        }
        this.props.navigation.navigate('PortfolioDetails', { itemDetails: itemDetailObj });
    }

    renderPortfolioDetails(comm) {
        const { CommodityId, expDateToDisplay, NetValue, TotalQuantity, AveragePrice, LTP, currValue, profitLoss, ExpiryDate } = comm.item;

        var profTextColor = '#fff';
        if (profitLoss > 0) {
            profTextColor = '#01800f';
        } else if (profitLoss < 0) {
            profTextColor = '#de0808';
        }

        return (
            <View>
                <TouchableOpacity style={[styles.items]}
                    onPress={() => this.editItemClicked(CommodityId, ExpiryDate)}
                >
                    <View style={[styles.headerTabs, { paddingLeft: 10 }]}>
                        <Text style={styles.headerText}>{CommodityId}{"\n"}{expDateToDisplay}{"\n"}</Text>
                    </View>
                    <View style={[styles.headerTabs, styles.priceTab, { borderColor: '#2e2e2e' }]}>
                        <Text style={styles.headerText}>{common.addcommas(TotalQuantity)}</Text>
                        {/* <Text style={styles.headerText}>{common.addDecimals(AveragePrice)}</Text> */}
                        <Text style={styles.headerText}>{common.addDecimals(NetValue)}{"\n"}</Text>
                    </View>
                    <View style={[styles.headerTabs, styles.changeTab]}>
                        <Text style={styles.headerText}>{common.addDecimals(LTP)}</Text>
                        <Text style={styles.headerText}>{common.addDecimals(currValue)}</Text>
                        <Text style={[styles.headerText, { color: profTextColor }]}>{common.addDecimals(profitLoss)}</Text>
                    </View>
                </TouchableOpacity>

                <View style={{
                    alignItems: 'flex-end', marginRight: moderateScale(20),
                    //  marginBottom:moderateScale(5), marginTop:moderateScale(5)
                }}>
                    <TouchableOpacity style={{ padding: moderateScale(5), flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => this.detailsClicked(CommodityId, ExpiryDate)}
                    >
                        <Icon name='info-circle'
                            size={moderateScale(16)}
                            style={{ color: '#fff', paddingRight: moderateScale(5) }}
                        />
                        <Text style={{ fontSize: moderateScale(13), color: '#fff' }}>
                            Details
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>

        );
    }

    renderPortfolioList() {
        var totalPLColor = '#fff';
        if (this.state.totalProfitLoss > 0) {
            totalPLColor = '#01800f';
        } else if (this.state.totalProfitLoss < 0) {
            totalPLColor = '#de0808';
        }

        if (this.state.loading) {
            return null;
        }
        else if (this.state.portfolioErr != '') {
            return (
                <View
                    style={{
                        flex: 1, backgroundColor: 'transparent', padding: moderateScale(20),
                        alignItems: 'center', justifyContent: 'center'
                    }}
                >
                    <Icon name='chain-broken'
                        size={moderateScale(35)}
                        style={{ color: 'white' }}
                    />
                    <Text style={{ fontSize: moderateScale(20), color: 'white', marginTop: moderateScale(20) }}>
                        {this.state.portfolioErr}
                    </Text>
                    <TouchableOpacity
                        style={{ flexDirection: 'row', padding: 10, alignSelf: 'flex-end', alignItems: 'center' }}
                        onPress={() => this.loadPortfolioData()}
                    >
                        <Icon name='refresh'
                            size={moderateScale(15)}
                            style={{ marginTop: moderateScale(20), color: 'white' }}
                        />
                        <Text style={{ fontSize: moderateScale(15), color: 'white', marginTop: moderateScale(20), marginLeft: moderateScale(5) }}>
                            Retry
                        </Text>
                    </TouchableOpacity>
                </View>
            );
        }
        else if (this.state.userPortfolio == null || this.state.userPortfolio == undefined || this.state.userPortfolio.length < 1) {
            console.log(this.state.userPortfolio);
            return (
                <View style={{ justifyContent: 'center', margin: moderateScale(20) }}>
                    <Text style={{ fontSize: moderateScale(15), color: '#fff' }}>No commodities in your portfolio</Text>
                </View>
            );
        }
        return (
            <View>
                <View style={styles.header}>
                    <View style={[styles.headerTabs, { paddingLeft: 10 }]}>
                        <Text style={styles.headerText}>Commodity{"\n"}Expiry Date{"\n"}</Text>
                    </View>
                    <View style={[styles.headerTabs, styles.priceTab]}>
                        <Text style={styles.headerText}>Quantity</Text>
                        {/* <Text style={styles.headerText}>Price</Text> */}
                        <Text style={styles.headerText}>Net Value{"\n"}</Text>
                    </View>
                    <View style={[styles.headerTabs, styles.changeTab]}>
                        <Text style={styles.headerText}>LTP</Text>
                        <Text style={styles.headerText}>Curr. Value</Text>
                        <Text style={styles.headerText}>Profit/Loss</Text>
                    </View>
                </View>

                <FlatList
                    data={this.state.userPortfolio}
                    extraData={this.state}
                    renderItem={(comm) => this.renderPortfolioDetails(comm)}
                    keyExtractor={(index) => JSON.stringify(index)}
                    removeClippedSubviews={false}
                />

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: moderateScale(20),
                    marginRight: moderateScale(20),
                    marginTop: moderateScale(20),
                    paddingBottom: moderateScale(10),
                    borderBottomWidth: 1,
                    borderColor: '#707070'
                }}>
                    <Text style={{ fontSize: moderateScale(17), color: '#ffffff' }}>Net Profit / Loss Value (₹)</Text>
                    <Text style={{ fontSize: moderateScale(17), color: totalPLColor }}>{common.addDecimals(this.state.totalProfitLoss)}</Text>
                </View>
            </View>
        );
    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }

    render() {
        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation} />
                <Loader loading={this.state.loading} />
                <LinearGradient
                    colors={['#303132', '#171717']}
                    // locations={[0,0.5,0.9]}
                    start={{ x: 0.5, y: 0.5 }} end={{ x: 1.0, y: 1.0 }}
                    style={{ flex: 1 }}
                >
                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >
                        <View style={{ marginLeft: moderateScale(20), paddingTop: moderateScale(20), paddingBottom: moderateScale(10) }}>
                            <Text style={{ fontSize: moderateScale(26), color: 'white' }}>Portfolio</Text>
                        </View>

                        {this.renderPortfolioList()}

                        <View style={{ flex: 1, marginRight: moderateScale(20), marginTop: moderateScale(20), marginBottom: moderateScale(20) }}>

                            {this.state.portfolioErr == '' ? <TouchableOpacity
                                onPress={() => this.addItemClicked()} >
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                    <Icon name='plus-circle'
                                        size={moderateScale(20)}
                                        style={{ color: '#6f73ff', paddingRight: moderateScale(5) }}
                                    />

                                    <Text style={{ color: 'white', fontSize: moderateScale(13) }}>Add New</Text>
                                </View>

                            </TouchableOpacity> : null}

                        </View>

                    </ScrollView>

                </LinearGradient>


                <Overlay
                    visible={this.state.modalVisible}
                    onClose={this.onClose}
                    closeOnTouchOutside={false}
                    containerStyle={{
                        backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', paddingRight: 0, paddingTop: 0
                    }}
                    childrenWrapperStyle={{
                        alignSelf: 'center',
                        backgroundColor: '#2b2b33',
                        justifyContent: 'center',
                        width: width > 600 ? 600 : width - scale(40),
                        borderRadius: moderateScale(5),
                        marginLeft: moderateScale(-20)
                    }}
                >
                    <View>
                        <View style={{ alignItems: 'flex-end', marginRight: moderateScale(-10), marginBottom: moderateScale(5), marginTop: moderateScale(-10) }}>
                            <TouchableOpacity
                                style={{ padding: moderateScale(5) }}
                                onPress={this.onClose}
                            >
                                <SLIcon name='close'
                                    size={moderateScale(23)}
                                    style={{ color: '#ffffff' }}
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(10) }}>

                            <View style={{ marginRight: moderateScale(20) }}>
                                <Text style={{ fontSize: moderateScale(13), color: 'white', alignContent: 'flex-start' }}>Commodity:</Text>
                            </View>

                            <View style={{ width: width * 0.45, height: moderateScale(40), borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-around' }}>
                                <Dropdown
                                    // ref={this.codeRef}
                                    value={this.state.selectedComm}
                                    textColor={'white'}
                                    onChangeText={(comm) => this.onCommdityChange(comm)}
                                    containerStyle={{ width: width * 0.4, alignSelf: 'center', marginBottom: moderateScale(15) }}
                                    data={this.props.commodityList}
                                    // propsExtractor={({ props }, index) => props}
                                    fontSize={moderateScale(14)}
                                    itemTextStyle={{ fontSize: moderateScale(13) }}
                                    pickerStyle={{ backgroundColor: '#000000', borderRadius: moderateScale(5) }}
                                    itemColor={'#727272'}
                                    itemCount={6}
                                    IconDetails={{ name: 'angle-down', size: moderateScale(20), color: 'white' }}
                                    dropdownPosition={-7.5}
                                    dropdownMargins={{ min: 15, max: 15 }}
                                />
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(10) }}>

                            <View style={{ marginRight: moderateScale(20) }}>
                                <Text style={{ fontSize: moderateScale(13), color: 'white', alignContent: 'flex-start' }}>Expiry Date:</Text>
                            </View>

                            <View style={{ width: width * 0.45, height: moderateScale(40), borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-around' }}>
                                <Dropdown
                                    // ref={this.codeRef}
                                    value={this.state.selectedExpiry}
                                    textColor={'white'}
                                    onChangeText={(exp) => this.onExpiryChange(exp)}
                                    containerStyle={{ width: width * 0.4, alignSelf: 'center', marginBottom: moderateScale(15) }}
                                    data={this.state.expiryData}
                                    // propsExtractor={({ props }, index) => props}
                                    fontSize={moderateScale(14)}
                                    itemTextStyle={{ fontSize: moderateScale(13) }}
                                    pickerStyle={{ backgroundColor: '#000000', borderRadius: moderateScale(5) }}
                                    itemColor={'#727272'}
                                    itemCount={8}
                                    IconDetails={{ name: 'angle-down', size: moderateScale(20), color: 'white' }}
                                    dropdownPosition={-1.5 - this.state.expiryData.length}
                                    dropdownMargins={{ min: 15, max: 15 }}
                                />
                            </View>
                        </View>

                        <View style={{ marginBottom: moderateScale(5) }}>
                            <RadioGroup
                                size={moderateScale(22)}
                                thickness={moderateScale(2)}
                                color='#5054cb'
                                style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}
                                // highlightColor='#ccc8b9'
                                selectedIndex={this.state.action == 'buy' ? 0 : 1}
                                onSelect={(index, value) => this.onSelect(index, value)}
                            >

                                <RadioButton
                                    value='buy'
                                    color='#d9fff1'
                                >
                                    <Text style={{ fontSize: moderateScale(14), color: '#ffffff' }}>BUY</Text>
                                </RadioButton>

                                <RadioButton
                                    value='sell'
                                    color='#d9fff1'
                                >
                                    <Text style={{ fontSize: moderateScale(14), color: '#ffffff' }}>SELL</Text>
                                </RadioButton>
                            </RadioGroup>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(15) }}>

                            <View style={{}}>
                                <Text style={{ fontSize: moderateScale(13), color: 'white', alignContent: 'flex-start' }}>Trans. Date:</Text>
                            </View>

                            <View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', width: width * 0.45, height: moderateScale(40), borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-between' }}>
                                    <DatePicker
                                        defaultDate={new Date()}
                                        minimumDate={new Date(2018, 1, 1)}
                                        maximumDate={new Date()}
                                        locale={"en"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        // placeHolderText="Select date"
                                        textStyle={{ paddingRight: width * 0.2, color: 'white', fontSize: moderateScale(14), marginLeft: moderateScale(5) }}
                                        // placeHolderTextStyle={{ color: "#d3d3d3" }}
                                        onDateChange={(newDate) => this.setDate(newDate)}
                                        disabled={false}
                                    />
                                    {/* <Icon name='angle-down'
                                        size={moderateScale(20)}
                                        style={{ color: '#fff', marginLeft: moderateScale(-10), alignSelf:'center' }}
                                    /> */}
                                </View>
                                {!this.state.transDateErr == '' ?
                                    <Text style={styles.errorMsg}>{this.state.transDateErr}</Text> : null
                                }
                            </View>

                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(15) }}>

                            <View style={{}}>
                                <Text style={{ fontSize: moderateScale(13), color: 'white', alignContent: 'flex-start' }}>Quantity:</Text>
                            </View>

                            <View>
                                <View style={{ width: width * 0.45, height: moderateScale(40), borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-around' }}>
                                    <TextInput
                                        style={{ color: 'white', fontSize: moderateScale(14), marginLeft: moderateScale(5) }}
                                        keyboardType='numeric'
                                        maxLength={5}
                                        value={this.state.quantity}
                                        onChangeText={(text) => { this.setState({ quantity: common.numberOnly(text) }) }}
                                    />
                                </View>
                                {!this.state.quantityErr == '' ?
                                    <Text style={styles.errorMsg}>{this.state.quantityErr}</Text> : null
                                }
                            </View>

                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(15) }}>

                            <View style={{}}>
                                <Text style={{ fontSize: moderateScale(13), color: 'white', alignContent: 'flex-start' }}>Price:</Text>
                            </View>

                            <View>
                                <View style={{ width: width * 0.45, height: moderateScale(40), borderColor: '#686969', borderRadius: moderateScale(5), borderWidth: 1, justifyContent: 'space-around' }}>
                                    <TextInput
                                        style={{ color: 'white', fontSize: moderateScale(14), marginLeft: moderateScale(5) }}
                                        keyboardType='numeric'
                                        maxLength={10}
                                        value={this.state.price}
                                        onChangeText={(text) => { this.setState({ price: common.numberWithDecimal(text) }) }}
                                    />
                                </View>
                                {!this.state.priceErr == '' ?
                                    <Text style={styles.errorMsg}>{this.state.priceErr}</Text> : null
                                }
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: moderateScale(15) }}>

                            <LinearGradient colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
                                start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
                                style={{ borderRadius: moderateScale(5) }}
                            >
                                <TouchableOpacity
                                    style={styles.buttonStyle}
                                    onPress={() => this.saveItemClicked()}
                                >
                                    <Text style={styles.textStyle}>
                                        Save
                                </Text>
                                </TouchableOpacity>
                            </LinearGradient>

                            {/* <TouchableOpacity style={[styles.buttonStyle, { borderWidth: 1, borderColor: '#4a4eda', borderRadius: moderateScale(5), marginLeft: moderateScale(15) }]}>
                                <Text style={styles.textStyle}>
                                    Delete
                                </Text>
                            </TouchableOpacity> */}

                        </View>

                    </View>

                </Overlay>

            </View>
        );
    }

}


const mapStateToProps = state => {
    const { user } = state.auth;
    const { watchListArr, commodityData, commodityList, needToLoadPortfolio } = state.commDetails;
    return { user, watchListArr, commodityData, commodityList, needToLoadPortfolio }
}

export default connect(mapStateToProps, { portfolioList, CanCallService, needToLoadPortfolioData })(withNavigationFocus(Portfolio));

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    header: {
        // flex: 1,
        flexDirection: 'row',
        height: moderateScale(65),
        backgroundColor: '#4d4d4d',
        marginLeft: moderateScale(20),
        marginRight: moderateScale(20),
        // marginTop: verticalScale(15),
        // marginBottom: verticalScale(10),
        borderRadius: moderateScale(5),
        borderWidth: 1,
        borderColor: '#59595a'
    },
    headerTabs: {
        justifyContent: 'center',
        flex: 1
    },
    priceTab: {
        alignItems: 'flex-end',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        paddingRight: moderateScale(10),
        borderColor: '#59595a'
    },
    changeTab: {
        alignItems: 'flex-end',
        paddingRight: moderateScale(10)
    },
    headerText: {
        color: 'white',
        fontSize: moderateScale(14),
        // fontWeight: '500'
    },
    items: {
        flex: 1,
        flexDirection: 'row',
        height: moderateScale(70),
        backgroundColor: '#171717',
        marginLeft: moderateScale(20),
        marginRight: moderateScale(20),
        marginTop: moderateScale(8),
        borderRadius: moderateScale(5),
    },
    buttonStyle: {
        justifyContent: "center",
        alignItems: 'center',
        width: moderateScale(130),
        height: moderateScale(50),
    },
    textStyle: {
        color: 'white',
        fontFamily: 'Lato',
        fontSize: moderateScale(14),
        fontWeight: 'bold'
    },
    errorMsg: {
        color: '#e64545',
        fontSize: moderateScale(13)
    },
});

class GetPortfolioList {
    UserId;
    constructor() {

    }
}