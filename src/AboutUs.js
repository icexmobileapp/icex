import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, Linking, Dimensions, UIManager, LayoutAnimation } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import Communications from 'react-native-communications';
import Header from './components/Header';

export default class AboutUs extends Component {

    render() {

        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation} />
                <LinearGradient
                    colors={['#303132', '#171717']}
                    // locations={[0,0.5,0.9]}
                    start={{ x: 0.5, y: 0.5 }} end={{ x: 1.0, y: 1.0 }}
                    style={{ flex: 1 }}
                >
                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >
                        <View style={{ marginLeft: moderateScale(20), marginRight: moderateScale(20), paddingBottom: moderateScale(30) }}>
                            <Text style={{ fontSize: moderateScale(26), color: 'white', paddingTop: moderateScale(20), paddingBottom: moderateScale(20) }}>About Us</Text>

                            <Text style={{ fontSize: moderateScale(14), color: '#979797', paddingBottom: moderateScale(25) }}>Indian Commodity Exchange Limited (ICEX) is SEBI regulated Online Commodity Derivatives Exchange. Headquartered at Mumbai, the exchange provides nationwide trading platform through its appointed brokers.{'\n\n'}
The Key Shareholders of ICEX are Central Warehousing Corporation, IDFC Bank, Punjab National Bank, Indian Potash Limited, KRIBHCO, MMTC Ltd, Reliance, Bajaj Holdings & Investment Ltd, Indiabulls Housing Finance, GAIC, GSAMB and NAFED.
                        </Text>

                            <Text style={{ fontWeight: 'bold', fontSize: moderateScale(14), color: '#979797', paddingBottom: moderateScale(10) }}>Corporate Office:
                        </Text>
                            <Text style={{ fontSize: moderateScale(14), color: '#979797', paddingBottom: moderateScale(25) }}>Reliable Tech Park, 403-A, B-Wing, 4th Floor, Thane-Belapur Road, Airoli (E), Navi Mumbai – 400708, India
                        </Text>

                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ flex: 1, fontWeight: 'bold', fontSize: moderateScale(14), color: '#979797', paddingBottom: moderateScale(20) }}>Tel.No:</Text>
                                <TouchableOpacity
                                    style={{ flex: 2.5, paddingBottom: moderateScale(20) }}
                                    onPress={() => Communications.phonecall('02240381500', true)}
                                >
                                    <Text style={{ fontSize: moderateScale(14), color: '#6f73ff' }}>+91-22-40381500 </Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ flex: 1, fontWeight: 'bold', fontSize: moderateScale(14), color: '#979797', paddingBottom: moderateScale(20) }}>Fax No:</Text>
                                <Text style={{ flex: 2.5, fontSize: moderateScale(14), color: '#979797', paddingBottom: moderateScale(20) }}>+91-22-40381511 </Text>
                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ flex: 1, fontWeight: 'bold', fontSize: moderateScale(14), color: '#979797', paddingBottom: moderateScale(20) }}>Email:</Text>
                                <TouchableOpacity
                                    style={{ flex: 2.5, paddingBottom: moderateScale(20) }}
                                    onPress={() => Communications.email(['customercare@icexindia.com'], null, null, null, null)}
                                >
                                    <Text style={{ fontSize: moderateScale(14), color: '#6f73ff' }}>customercare@icexindia.com</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ flex: 1, fontWeight: 'bold', fontSize: moderateScale(14), color: '#979797', paddingBottom: moderateScale(20) }}>Whatsapp:</Text>
                                <TouchableOpacity
                                    style={{ flex: 2.5, paddingBottom: moderateScale(20) }}
                                    onPress={() => Linking.openURL('https://api.whatsapp.com/send?phone=918451920245&text=Hi').catch((err) => console.error('An error occurred', err))}
                                >
                                    <Text style={{ fontSize: moderateScale(14), color: '#6f73ff' }}>+91-8451920245</Text>
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style={{marginBottom:moderateScale(15)}}>
                            <LinearGradient colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
                                start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
                                style={{ borderRadius: moderateScale(5), alignSelf: 'center' }}
                            >
                                <TouchableOpacity
                                    style={styles.buttonStyle}
                                    onPress={() => this.props.navigation.navigate('IcexWebView', { navigation: this.props.navigation })}
                                >
                                    <Text style={styles.textStyle}>
                                        www.icexindia.com
                                </Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>

                    </ScrollView>
                </LinearGradient>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    buttonStyle: {
        justifyContent: "center",
        alignItems: 'center',
        width: moderateScale(275),
        height: moderateScale(31),
    },
    textStyle: {
        color: 'white',
        fontFamily: 'Lato',
        fontSize: moderateScale(14),
        fontWeight: 'bold'
    },
});