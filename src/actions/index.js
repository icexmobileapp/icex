import {
    LOGIN_USER_SUCCESS, LOGIN_USER_FAIL, LOGIN_USER,
    REGISTER_USER, REGISTER_USER_SUCCESS, REGISTER_USER_FAIL,
    EDIT_PROFILE_OPEN, EDIT_PROFILE_CLOSE,
    IS_INTERNET_CONNECTED,
    SPOT, SPOT_SUCCESS, SPOT_FAIL,
    CAN_CALL_SERVICE,
    GET_COMMODITY_DATA, GET_COMMODITY_DATA_SUCCESS, GET_COMMODITY_DATA_FAIL, SET_COMMODITY_LIST,
    GET_WATCHLIST_ARR, UPDATE_USER, SET_CHART_DATA, RESET_STATE, REFRESH_TIME_CHANGED, NEED_TO_LOAD_PORTFOLIO,
    SET_SELECTED_EXPIRY_INDEX
} from './types';
import common from "../common";
import axios from 'axios';

//---------------------ActionCreators for LogIn--------------------//

export const loginUser = (serviceBody) => {
    console.log(serviceBody)
    return (dispatch) => {
        dispatch({ type: LOGIN_USER })

        common.callService('ValidateUserLogin_v2', serviceBody)
            .then(user => {
                console.log('In ActionCreater');
                console.log(user);
                if (user.ReturnCode == '1') {
                    loginUserSuccess(dispatch, user.Data);
                } else {
                    loginUserFail(dispatch, user.ReturnMsg);
                }

            })
            .catch(err => {
                console.log('In Catch ActionCreater');
                console.log(err);
                loginUserFail(dispatch, 'Some unexpected error occured')
            })
    };
}

const loginUserFail = (dispatch, errMsg) => {
    dispatch({
        type: LOGIN_USER_FAIL,
        payload: errMsg
    })
}

const loginUserSuccess = (dispatch, user) => {
    dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: user
    })
}


//---------------------ActionCreators for Registration---------------------//

export const registerUser = (serviceBody) => {
    console.log(serviceBody)
    return (dispatch) => {
        dispatch({ type: REGISTER_USER })

        common.callService('UserRegistration_v2', serviceBody)
            .then(registrationDetails => {
                console.log('In ActionCreater');
                console.log(registrationDetails);
                if (registrationDetails.ReturnCode == '1') {
                    let response = registrationDetails.Data;
                    response.isNewUser = true;
                    registerUserSuccess(dispatch, response);
                } else {
                    registerUserFail(dispatch, registrationDetails.ReturnMsg);
                }
            })
            .catch(err => {
                console.log('In Catch ActionCreater');
                console.log(err);
                registerUserFail(dispatch, 'Some unexpected error occured')
            })
    };
}

const registerUserFail = (dispatch, errMsg) => {
    dispatch({
        type: REGISTER_USER_FAIL,
        payload: errMsg
    })
}

const registerUserSuccess = (dispatch, registrationDetails) => {
    dispatch({
        type: REGISTER_USER_SUCCESS,
        payload: registrationDetails
    })
}

//---------------------ActionCreators for Toggling Edit Profile---------------------//

export const openEditProfile = () => {
    return {
        type: EDIT_PROFILE_OPEN
    }
}

export const closeEditProfile = () => {
    return {
        type: EDIT_PROFILE_CLOSE
    }
}

//---------------------ActionCreator for Checking Internet Connectivity---------------------//

export const changeInternetConnectivity = (status) => {
    return {
        type: IS_INTERNET_CONNECTED,
        payload: status
    }
}

//---------------------ActionCreators for Spot Service---------------------//

export const getSpotPrice = (serviceBody) => {
    console.log(serviceBody)
    return (dispatch) => {
        dispatch({ type: SPOT })

        common.callService('SpotMarketPrice', serviceBody)
            .then(SpotMarketPriceDetails => {
                console.log('In ActionCreater');
                console.log(SpotMarketPriceDetails);
                if (SpotMarketPriceDetails.ReturnCode == '1') {
                    spotSuccess(dispatch, SpotMarketPriceDetails.Data);
                } else {
                    spotFail(dispatch, SpotMarketPriceDetails.ReturnMsg);
                }
            })
            .catch(err => {
                console.log('In Catch ActionCreater');
                console.log(err);
                spotFail(dispatch, 'Unable to fetch Spot Price');
            })
    };
}

const spotFail = (dispatch, err) => {
    dispatch({
        type: SPOT_FAIL,
        payload: err
    })
}

const spotSuccess = (dispatch, SpotMarketPriceDetails) => {
    dispatch({
        type: SPOT_SUCCESS,
        payload: SpotMarketPriceDetails
    })
}

//---------------------ActionCreator for checking CurrPage to callService---------------------//


export const CanCallService = (canCall) => {
    // console.log(canCall)
    return {
        type: CAN_CALL_SERVICE,
        payload: canCall
    };
}

//---------------------ActionCreator for CommodityData---------------------//

export const getCommodityData = () => {
    // console.log(serviceBody)
    return (dispatch) => {
        dispatch({ type: GET_COMMODITY_DATA })
        var url = 'https://www.icexindia.com/Mobile/MobileMarketData.txt'
        url += '?dt=' + new Date().getTime();

        axios.get(url)
            .then(function (response) {
                console.log(response.data);
                if (Array.isArray(response.data)) {
                    const CommodityData = response.data;
                    var CommodityList = []
                    getCommodityDataSuccess(dispatch, CommodityData);
                    for (let i = 0; i < CommodityData.length; i++) {
                        let obj = {
                            index: i,
                            value: CommodityData[i][0].Underlying
                        }
                        CommodityList.push(obj);
                        if (i == CommodityData.length - 1) {
                            console.log(CommodityList)
                            setCommodityList(dispatch, CommodityList);
                        }
                    }
                } else {
                    getCommodityDataFail(dispatch, 'Error fetching data')
                }
            })
            .catch(function (error) {
                console.log(error.response);
                getCommodityDataFail(dispatch, 'Error fetching data')
            });
    };
}

const getCommodityDataFail = (dispatch, errMsg) => {
    dispatch({
        type: GET_COMMODITY_DATA_FAIL,
        payload: errMsg
    })
}

const setCommodityList = (dispatch, CommodityList) => {
    dispatch({
        type: SET_COMMODITY_LIST,
        payload: CommodityList
    })
}

const getCommodityDataSuccess = (dispatch, CommodityData) => {
    dispatch({
        type: GET_COMMODITY_DATA_SUCCESS,
        payload: CommodityData
    })
}
//---------------------------WatchList Array-------------------------//
export const getWatchListArr = (watchlistArr) => {
    console.log('watchlistArr', watchlistArr);
    return {
        type: GET_WATCHLIST_ARR,
        payload: watchlistArr
    };
}

//---------------------------Chart Data---------------------------//
export const setChartData = (chartData) => {
    return {
        type: SET_CHART_DATA,
        payload: chartData
    };
}

//---------------------------Update User---------------------------//
export const updateUser = (userDetails) => {
    return {
        type: UPDATE_USER,
        payload: userDetails
    };
}

//---------------------------Reset States While LoggingOut---------------------------//
export const resetState = () => {
    return {
        type: RESET_STATE,
    };
}

//---------------------------Refresh Time Changed---------------------------//
export const refreshTimeChanged = () => {
    return {
        type: REFRESH_TIME_CHANGED,
        payload: true
    };
}

//---------------------------Need To Load Portfolio---------------------------//
export const needToLoadPortfolioData = (flag) => {
    return {
        type: NEED_TO_LOAD_PORTFOLIO,
        payload: flag
    };
}

//---------------------------Setting Future Selected Tab in MarketDepth---------------------------//
export const setSelectedExpiryIndex = (index) => {
    return {
        type: SET_SELECTED_EXPIRY_INDEX,
        payload: index
    };
}


// //---------------------ActionCreators for saving comment---------------------//

// export const saveComment = (serviceBody) => {
//     console.log(serviceBody)
//     return (dispatch) => {
//         dispatch({ type: SAVE_COMMENT })

//         common.callService('SaveBlogComments', serviceBody)
//             .then(SaveBlogComments => {
//                 console.log('In ActionCreater');
//                 console.log(SaveBlogComments);
//                 saveCommentSuccess(dispatch, SaveBlogComments)
//             })
//             .catch(err => {
//                 console.log('In Catch ActionCreater');
//                 console.log(err);
//                 saveCommentFail(dispatch)
//             })
//     };
// }

// const saveCommentFail = (dispatch) => {
//     dispatch({ type: SAVE_COMMENT_FAIL })
// }

// const saveCommentSuccess = (dispatch, SaveBlogComments) => {
//     dispatch({
//         type: SAVE_COMMENT_SUCCESS,
//         payload: SaveBlogComments
//     })
// }

// //---------------------ActionCreators for fetching comments---------------------//

// export const getComment = (serviceBody) => {
//     console.log(serviceBody)
//     return (dispatch) => {
//         dispatch({ type: GET_COMMENTS })

//         common.callService('BlogCommentsbyCommodity', serviceBody)
//             .then(Comments => {
//                 console.log('In ActionCreater');
//                 console.log(Comments);
//                 if (Comments.ReturnCode == '1') {
//                     getCommentSuccess(dispatch, Comments.Data)
//                 } else {
//                     getCommentFail(dispatch, Comments.ReturnMsg);
//                 }
//             })
//             .catch(err => {
//                 console.log('In Catch ActionCreater');
//                 console.log(err);
//                 getCommentFail(dispatch, 'Unable to fetch comments')
//             })
//     };
// }

// const getCommentFail = (dispatch, errMsg) => {
//     dispatch({
//         type: GET_COMMENTS_FAIL,
//         payload: errMsg
//     })
// }

// const getCommentSuccess = (dispatch, Comments) => {
//     dispatch({
//         type: GET_COMMENTS_SUCCESS,
//         payload: Comments
//     })
// }

