export const LOGIN_USER_SUCCESS = 'login_User_Success'
export const LOGIN_USER_FAIL = 'login_User_Fail'
export const LOGIN_USER = 'login_User'

export const REGISTER_USER = 'register_User'
export const REGISTER_USER_SUCCESS = 'register_User_Success'
export const REGISTER_USER_FAIL = 'register_User_Fail'

export const EDIT_PROFILE_OPEN = 'edit_Profile_Open'
export const EDIT_PROFILE_CLOSE = 'edit_Profile_Close'

export const IS_INTERNET_CONNECTED = 'is_Internet_Connected'

export const SPOT = 'spot'
export const SPOT_SUCCESS = 'spot_Success'
export const SPOT_FAIL = 'spot_Fail'

export const SAVE_COMMENT = 'save_Comment'
export const SAVE_COMMENT_SUCCESS = 'save_Comment_Success'
export const SAVE_COMMENT_FAIL = 'save_Comment_Fail'

export const GET_COMMENTS = 'get_Comments'
export const GET_COMMENTS_SUCCESS = 'get_Comments_Success'
export const GET_COMMENTS_FAIL = 'get_Comments_Fail'

export const CAN_CALL_SERVICE = 'can_Call_Service'

export const GET_WATCHLIST_ARR = 'get_watchlist_arr'

export const GET_COMMODITY_DATA = 'get_Commodity_Data'
export const SET_COMMODITY_LIST = 'set_Commodity_List'
export const GET_COMMODITY_DATA_SUCCESS = 'get_Commodity_Data_Success'
export const GET_COMMODITY_DATA_FAIL = 'get_Commodity_Data_Fail'

export const UPDATE_USER = 'update_User'

export const SET_CHART_DATA = 'set_Chart_Data'

export const RESET_STATE = 'resest_State'

export const REFRESH_TIME_CHANGED = 'refresh_Time_Changed'

export const NEED_TO_LOAD_PORTFOLIO = 'need_To_Load_Portfolio'

export const SET_SELECTED_EXPIRY_INDEX = 'set_Selected_Expiry_Index'

export const GET_PORTFOLIOLIST = 'get_portfolio'
export const GET_GET_PORTFOLIOLIST_SUCCESS = 'get_portfolio_Success'
export const GET_GET_PORTFOLIOLIST_FAIL = 'get_portfolio_Fail'
