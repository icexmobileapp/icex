import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Dimensions, UIManager, LayoutAnimation, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import Icon from "react-native-vector-icons/FontAwesome";
import { Toast } from 'native-base';
import Header from './components/Header';
import { connect } from 'react-redux';
import { withNavigationFocus } from "react-navigation";
import common from './common';
import { getWatchListArr, CanCallService } from './actions';
import Loader from "./components/Loader";
import SLIcon from "react-native-vector-icons/SimpleLineIcons";


class WatchList extends Component {

    state = {
        loading: false,
        watchlistedComm: [],
        showCancel: false
    }

    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps WatchList');
        this.setState({
            loading: false,
        });
        if (nextProps.watchListArr.length != this.props.watchListArr.length) {

            var watchCommArr = [];
            for (let i = 0; i < this.props.commodityData.length; i++) {
                for (j = 0; j < nextProps.watchListArr.length; j++) {
                    if (this.props.commodityData[i][0].Underlying == nextProps.watchListArr[j].CommodityId) {
                        watchCommArr.push(this.props.commodityData[i]);
                        // break;
                    }
                }
                if (i == this.props.commodityData.length - 1) {
                    console.log('componentWillReceiveProps WatchList watchCommArr');
                    console.log(watchCommArr);
                    this.setState({
                        watchlistedComm: watchCommArr
                    });
                }
            }

        } else {
            if (nextProps.isFocused) {
                var watchCommArr = [];
                for (let i = 0; i < nextProps.commodityData.length; i++) {
                    for (j = 0; j < nextProps.watchListArr.length; j++) {
                        if (nextProps.commodityData[i][0].Underlying == nextProps.watchListArr[j].CommodityId) {
                            watchCommArr.push(nextProps.commodityData[i]);
                            // break;
                        }
                    }
                    if (i == nextProps.commodityData.length - 1) {
                        console.log(watchCommArr);
                        this.setState({
                            watchlistedComm: watchCommArr
                        });
                    }
                }
            }
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.isFocused !== this.props.isFocused) {
            this.props.CanCallService(this.props.isFocused);
            if (this.state.watchlistedComm.length != this.props.watchListArr.length) {
                this.setWatchlistArr();
            }
        } else if (this.props.isFocused) {
            this.props.CanCallService(this.props.isFocused);
        }
    }

    componentWillMount() {
        if (this.props.watchListArr == null) {
            const req = new GetWatchlist();
            req.UserId = this.props.user.Id;

            console.log((JSON.stringify(req)));

            var serviceRequest = common.encryptData(JSON.stringify(req))
            this.setState({
                loading: true,
            });
            var serviceBody = { request: serviceRequest };
            console.log(serviceBody);
            common.callService('GetWatchlist', serviceBody)
                .then(GetWatchlist => {
                    console.log('GetWatchlist', GetWatchlist);
                    if (GetWatchlist.ReturnCode == '1') {
                        this.setState({
                            loading: false,
                        });
                        var watchArr = [];
                        if (GetWatchlist.Data != null && GetWatchlist.Data.length > 0) {
                            for (var i = 0; i < GetWatchlist.Data.length; i++) {
                                watchArr.push(GetWatchlist.Data[i].CommodityId);
                                if (i == GetWatchlist.Data.length - 1) {
                                    this.props.getWatchListArr(watchArr);
                                }
                            }
                        } else if (GetWatchlist.Data.length == 0) {
                            var watchArr = [];
                            this.props.getWatchListArr(watchArr);
                        }
                    } else {
                        this.setState({
                            loading: false,
                        });
                    }
                })
                .catch(err => {
                    console.log(err);
                    this.setState({
                        loading: false,
                    });
                });
        } else {
            this.setWatchlistArr();
        }

    }

    setWatchlistArr() {
        var watchCommArr = [];
        console.log('this.props.watchListArr');
        console.log(this.props.watchListArr);
        for (let i = 0; i < this.props.commodityData.length; i++) {
            for (j = 0; j < this.props.watchListArr.length; j++) {
                if (this.props.commodityData[i][0].Underlying == this.props.watchListArr[j].CommodityId) {
                    watchCommArr.push(this.props.commodityData[i]);
                    // break;
                }
            }
            if (i == this.props.commodityData.length - 1) {
                console.log('watchCommArr');
                console.log(watchCommArr);
                this.setState({
                    watchlistedComm: watchCommArr
                });
            }
        }
    }

    removeCommodity(Underlying) {

        const req = new RemoveWatchList();
        req.UserId = this.props.user.Id;
        req.ContractCode = Underlying;
        console.log((JSON.stringify(req)));

        var serviceRequest = common.encryptData(JSON.stringify(req))
        this.setState({
            loading: true,

        });
        var serviceBody = { request: serviceRequest };
        console.log(serviceBody);
        common.callService('RemoveFromWatchlist', serviceBody)
            .then(RemoveFromWatchlist => {
                console.log('RemoveFromWatchlist', RemoveFromWatchlist);
                if (RemoveFromWatchlist.ReturnCode == '1') {
                    this.setState({
                        loading: false,
                    });
                    var tempArr = this.props.watchListArr;

                    var CommIndex = tempArr.findIndex(comm =>
                        String(comm.ContractCode) == String(Underlying))

                    tempArr.splice(CommIndex, 1);
                    console.log(CommIndex)
                    console.log(tempArr);
                    this.props.getWatchListArr(tempArr);
                    this.setWatchlistArr();

                } else {
                    this.setState({
                        loading: false,
                    });
                    setTimeout(() => {
                        Toast.show({
                            text: "Sorry, couldn't remove item from watchlist",
                            type: "danger"
                        });
                    }, 10)
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loading: false,
                });
            });

    }

    deleteClicked(Underlying) {
        if (this.state.showCancel) {
            return (
                <View style={styles.watchlistStyle}>
                    <TouchableOpacity
                        style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}
                        onPress={() =>
                            this.removeCommodity(Underlying)}
                    >
                        <SLIcon name='close'
                            size={moderateScale(15)}
                            style={{ color: 'white' }}
                            light
                        />
                        {/* <Text style={{ color: 'white', fontSize: moderateScale(13), marginLeft: scale(5), paddingRight: moderateScale(20) }}>Remove</Text> */}
                    </TouchableOpacity>
                </View>
            )
        } else {
            return null
        }
    }

    canShowLTP(LastTradedTime) {
        let lttDate = new Date(LastTradedTime.substr(0, 10));
        lttDate.setHours(0, 0, 0, 0);
        let currDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        if (lttDate.getTime() !== currDate.getTime()) {
            return false;
        } else {
            return true;
        }
    }

    renderItem(commodity) {
        var commIndex = 0;
        // // console.log(commodity.item);
        // // console.log(this.props.watchListArr);
        // // console.log('-------------------------------');
        loop1:
        for (let i = 0; i < commodity.item.length; i++) {
            for (let j = commodity.index; j < this.props.watchListArr.length; j++) {
                if (this.props.watchListArr[j].ContractCode == commodity.item[i].SymbolUniqueContractCode) {
                    // // console.log('Match',i,j);
                    commIndex = i;
                    break loop1;
                }
            }

        }
        // commodity.item.forEach(comm => {

        //     commIndex = this.state.watchlistedComm.findIndex(x =>
        //         (x.ContractCode == comm.SymbolUniqueContractCode))
        // });

        const { Underlying,
            ExpiryDate, LastTradedTime,
            LastTradedPrice,
            NetPriceChangeFromClosingPrice,
            NetPercentChangeFromClosingPrice, PreviousClosePrice,
            SymbolUniqueContractCode } = commodity.item[commIndex];
        let background = '#4d4d4d';

        let showLTPDetail = false;

        if (LastTradedTime != null && LastTradedTime != undefined) {
            showLTPDetail = this.canShowLTP(LastTradedTime);
        }
        if (showLTPDetail) {
            if (NetPriceChangeFromClosingPrice < 0) {
                background = '#de0808';
            } else if (NetPriceChangeFromClosingPrice > 0) {
                background = '#01800f';
            }
        }
        return (
            <View>
                {this.deleteClicked(SymbolUniqueContractCode)}
                <TouchableOpacity
                    style={[styles.items, { flexDirection: 'row' }]}
                    onPress={() => {
                        var index;
                        this.props.commodityList.find(x => {
                            if (x.value === Underlying)
                                index = x.index
                        });
                        this.props.navigation.navigate('CommodityDetail', { index: index, commodityName: Underlying, selectedExpIndex: commIndex })
                    }}
                >
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={[styles.headerTabs, { paddingLeft: 10, paddingRight: 8 }]}>
                            <Text style={styles.dataText}>{Underlying}</Text>
                            <Text style={styles.itemDateText}>{common.expiryDateToDisplay(ExpiryDate)}</Text>
                        </View>
                        <View style={[styles.headerTabs, styles.priceTab, { borderColor: '#2e2e2e' }]}>
                            {showLTPDetail ?
                                <Text style={styles.dataText}>{common.addDecimals(LastTradedPrice)}</Text> :
                                <Text style={styles.dataText}>{common.addDecimals(PreviousClosePrice)}</Text>
                            }
                        </View>
                        <View style={[styles.headerTabs, styles.changeTab]}>
                            <View style={[styles.changeItemContainer, { backgroundColor: background }]}>
                                {showLTPDetail ?
                                    <Text style={styles.changeItemText}>{common.addDecimals(NetPriceChangeFromClosingPrice)}</Text> :
                                    <Text style={styles.changeItemText}>0.00</Text>
                                }
                                {showLTPDetail ?
                                    <Text style={styles.changeItemText}>{common.addDecimals(NetPercentChangeFromClosingPrice)}%</Text> :
                                    <Text style={styles.changeItemText}>0.00%</Text>
                                }
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }


    removeFromWatchListClicked() {
        this.setState({
            showCancel: true
        });
    }

    cancelClicked() {
        this.setState({
            showCancel: false
        });
    }

    removeFromWatchList() {
        if (!this.state.showCancel) {
            return (

                <TouchableOpacity
                    onPress={() =>
                        this.removeFromWatchListClicked()}
                >
                    <Text style={{
                        color: 'white',
                        fontSize: moderateScale(14),
                    }}> Remove from watchlist</Text>

                </TouchableOpacity>
            )
        } else {
            return (

                <TouchableOpacity
                    onPress={() =>
                        this.cancelClicked()}
                >
                    <Text style={{
                        color: 'white',
                        fontSize: moderateScale(14),
                    }}> Cancel</Text>

                </TouchableOpacity>
            )
        }

    }

    renderList() {
        if (this.state.watchlistedComm.length < 1) {
            return (
                <View style={{ justifyContent: 'center', margin: moderateScale(20) }}>
                    <Text style={{ fontSize: moderateScale(15), color: '#fff' }}>You haven't added any commodity to watchlist</Text>
                </View>
            );
        } else if (this.props.errorCommodityData.trim() != '') {
            return (
                <View
                    style={{
                        flex: 1, backgroundColor: '#303132', padding: moderateScale(20),
                        alignItems: 'center', justifyContent: 'center'
                    }}
                >
                    <Icon name='chain-broken'
                        size={moderateScale(35)}
                        style={{ color: 'white' }}
                    />
                    <Text style={{ fontSize: moderateScale(20), color: 'white', marginTop: moderateScale(20) }}>
                        {this.props.errorCommodityData}
                    </Text>
                </View>

            );
        }
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', }} >
                    <View style={styles.header}>
                        <View style={[styles.headerTabs, { paddingLeft: 10, paddingRight: 8 }]}>
                            <Text style={styles.headerText}>Commodity{"\n"}Name</Text>
                        </View>
                        <View style={[styles.headerTabs, styles.priceTab]}>
                            <Text style={styles.headerText}>Price</Text>
                        </View>
                        <View style={[styles.headerTabs, styles.changeTab]}>
                            <Text style={styles.headerText}>Change</Text>
                        </View>

                    </View>
                </View>
                <FlatList
                    data={this.state.watchlistedComm}
                    renderItem={(commodity) => this.renderItem(commodity)}
                    keyExtractor={(commodity, index) => String(index)}
                    extraData={this.state}
                    removeClippedSubviews={false}

                />
                <View style={{
                    justifyContent: 'flex-end',
                    marginTop: moderateScale(10),
                    paddingBottom: moderateScale(10),
                    alignSelf: 'flex-end',
                    marginRight: moderateScale(20)
                }}>
                    {this.removeFromWatchList()}

                </View>

            </View >
        );
    }

    render() {

        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation} />
                <Loader loading={this.state.loading} />
                <LinearGradient
                    colors={['#303132', '#171717']}
                    // locations={[0,0.5,0.9]}
                    start={{ x: 0.5, y: 0.5 }} end={{ x: 1.0, y: 1.0 }}
                    style={{ flex: 1 }}
                >
                    <View style={{ marginLeft: moderateScale(20), paddingTop: moderateScale(20), paddingBottom: moderateScale(10) }}>
                        {this.state.watchlistedComm.length > 0 ?
                            <Text style={{ fontSize: moderateScale(26), color: 'white' }}>My WatchList ({this.state.watchlistedComm.length})</Text>
                            : <Text style={{ fontSize: moderateScale(26), color: 'white' }}>My WatchList</Text>
                        }
                    </View>
                    {this.renderList()}

                </LinearGradient>
            </View>
        );
    }

}

const mapStateToProps = state => {
    const { user } = state.auth;
    const { watchListArr, commodityData, commodityList, errorCommodityData } = state.commDetails;
    return { user, watchListArr, commodityData, commodityList, errorCommodityData }
}

export default connect(mapStateToProps, { getWatchListArr, CanCallService })(withNavigationFocus(WatchList));

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    header: {
        flex: 1,
        flexDirection: 'row',
        height: moderateScale(45),
        backgroundColor: '#4d4d4d',
        marginLeft: scale(16),
        marginRight: scale(16),
        // marginTop: verticalScale(15),
        // marginBottom: verticalScale(10),
        borderRadius: moderateScale(5),
        borderWidth: 1,
        borderColor: '#59595a'
    },
    headerTabs: {
        justifyContent: 'center',
        flex: 1
    },
    priceTab: {
        alignItems: 'flex-end',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        paddingRight: scale(10),
        borderColor: '#59595a'
    },
    changeTab: {
        alignItems: 'flex-end',
        paddingRight: scale(10)
    },
    headerText: {
        color: 'white',
        fontSize: moderateScale(14),
        fontWeight: '500'
    },
    dataText: {
        color: 'white',
        fontSize: moderateScale(12),
        fontWeight: '500'
    },

    items: {
        flex: 1,
        flexDirection: 'row',
        height: moderateScale(70),
        backgroundColor: '#171717',
        marginLeft: scale(16),
        marginRight: scale(16),
        marginTop: verticalScale(8),
        borderRadius: moderateScale(5),
    },
    itemDateText: {
        fontSize: moderateScale(14),
        color: '#9a9a9a'
    },
    changeItemContainer: {
        flex: 1,
        width: '80%',
        marginTop: verticalScale(10),
        marginBottom: verticalScale(10),
        borderRadius: moderateScale(5),
        justifyContent: 'center'
    },
    changeItemText: {
        alignSelf: 'flex-end',
        paddingRight: 10,
        color: 'white',
        fontSize: moderateScale(14),
        fontWeight: '500',
    },
    watchlistStyle: {
        flex: 1,
        zIndex: 1000,
        // justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginRight: scale(13),
        // padding: moderateScale(5),
        // paddingBottom:moderateScale(5)
        marginBottom: moderateScale(-18),
        // marginRight: moderateScale(-20)

    },
});

class GetWatchlist {
    UserId;
    constructor() {

    }
}

class RemoveWatchList {
    UserId;
    ContractCode;
    constructor() {

    }
}
