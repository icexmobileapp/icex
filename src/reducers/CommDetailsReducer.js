import {
    SPOT, SPOT_SUCCESS, SPOT_FAIL,
    CAN_CALL_SERVICE,
    GET_COMMODITY_DATA, GET_COMMODITY_DATA_SUCCESS, GET_COMMODITY_DATA_FAIL, SET_COMMODITY_LIST,
    GET_WATCHLIST_ARR, SET_CHART_DATA, RESET_STATE, REFRESH_TIME_CHANGED, NEED_TO_LOAD_PORTFOLIO,
    SET_SELECTED_EXPIRY_INDEX
} from '../actions/types';

const INITIAL_STATE = {
    spotDetails: null,
    saveCommentDetails: null,
    commentsArray: [],
    errorSpot: '',
    errorSaveComment: '',
    errorGetportfolio: '',
    errorGetComment: '',
    loading: false,
    canLoadHomeData: false,
    commodityData: null,
    errorCommodityData: '',
    commodityList: [],
    watchListArr: null,
    chartData: null,
    isRefreshTimeChanged: false,
    needToLoadPortfolio: false,
    selectedExpiryIndex: null,
};

export default (state = INITIAL_STATE, action) => {
    console.log(action)
    switch (action.type) {

        //-------------------SPOT-----------------------//
        case SPOT:
            return { ...state, errorSpot: '', loading: true }
        case SPOT_SUCCESS:
            return { ...state, loading: false, errorSpot: '', spotDetails: action.payload }
        case SPOT_FAIL:
            return { ...state, loading: false, spotDetails: null, errorSpot: action.payload }

        //-------------------Check Page To Call Srvice-----------------------//
        case CAN_CALL_SERVICE:
            return { ...state, canLoadHomeData: action.payload }

        //-------------------watchlist-----------------------//
        case GET_WATCHLIST_ARR:
            return { ...state, watchListArr: action.payload }

        //------------------Homepage Data--------------------------------//

        case GET_COMMODITY_DATA:
            return { ...state, errorCommodityData: '' }
        case GET_COMMODITY_DATA_SUCCESS:
            return { ...state, loading: false, errorCommodityData: '', commodityData: action.payload }
        case GET_COMMODITY_DATA_FAIL:
            return { ...state, loading: false, errorCommodityData: action.payload }

        case SET_COMMODITY_LIST:
            return { ...state, commodityList: action.payload }

        //------------------Displaying Chart Data--------------------------------//
        case SET_CHART_DATA:
            return { ...state, chartData: action.payload }

        //------------------Reset States While LoggingOut--------------------------------//.
        case RESET_STATE:
            return { ...state, ...INITIAL_STATE }

        //------------------Reset States While LoggingOut--------------------------------//.
        case REFRESH_TIME_CHANGED:
            return { ...state, isRefreshTimeChanged: action.payload }

        //------------------Reset States While LoggingOut--------------------------------//.
        case NEED_TO_LOAD_PORTFOLIO:
            return { ...state, needToLoadPortfolio: action.payload }

        //------------------Setting Future Selected Tab in MarketDepth--------------------------------//.
        case SET_SELECTED_EXPIRY_INDEX:
            return { ...state, selectedExpiryIndex: action.payload }

        default:
            return state;
    }
}


  //-------------------BLOG (Save Comment)-----------------------//
        // case SAVE_COMMENT:
        //     return { ...state, errorSaveComment: '', loading: true }
        // case SAVE_COMMENT_SUCCESS:
        //     return { ...state, loading: false, errorSaveComment: '', saveCommentDetails: action.payload }
        // case SAVE_COMMENT_FAIL:
        //     return { ...state, loading: false, errorSaveComment: 'Unable to get details' }

        // //-------------------BLOG (Fetch Comments)-----------------------//
        // case GET_COMMENTS:
        //     return { ...state, errorGetComment: '', loading: true }
        // case GET_COMMENTS_SUCCESS:
        //     return { ...state, loading: false, errorGetComment: '', commentsArray: action.payload }
        // case GET_COMMENTS_FAIL:
        //     return { ...state, loading: false, errorGetComment: 'Unable to get details' }