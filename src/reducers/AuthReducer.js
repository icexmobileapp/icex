import {
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER,
    REGISTER_USER,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAIL,
    EDIT_PROFILE_OPEN,
    EDIT_PROFILE_CLOSE,
    IS_INTERNET_CONNECTED,
    UPDATE_USER,
    RESET_STATE,
} from '../actions/types';

const INITIAL_STATE = {
    user: null,
    errorLogin: '',
    loading: false,
    registrationDetails: null,
    errorRegistration: '',
    openEditProfileModal: false,
    isInternetConnected: true,
};

export default (state = INITIAL_STATE, action) => {
    console.log(action)
    switch (action.type) {

        //-------------------Login-----------------------//
        case LOGIN_USER:
            return { ...state, ...INITIAL_STATE, loading: true }
        case LOGIN_USER_SUCCESS:
            return { ...state, ...INITIAL_STATE, user: action.payload }
        case LOGIN_USER_FAIL:
            return { ...state, ...INITIAL_STATE, errorLogin: action.payload }

        //-------------------Registration-----------------------//
        case REGISTER_USER:
            return { ...state, ...INITIAL_STATE, loading: true }
        case REGISTER_USER_SUCCESS:
            return { ...state, ...INITIAL_STATE, user: action.payload }
        case REGISTER_USER_FAIL:
            return { ...state, ...INITIAL_STATE, errorRegistration: action.payload }

        //-------------------Edit Profile Modal---------------------//
        case EDIT_PROFILE_OPEN:
            return { ...state, openEditProfileModal: true }
        case EDIT_PROFILE_CLOSE:
            return { ...state, openEditProfileModal: false }

        //---------------Internet Connectivity Status-----------------//
        case IS_INTERNET_CONNECTED:
            return { ...state, isInternetConnected: action.payload }

        //-------------------Update-----------------------//
        case UPDATE_USER:
            return { ...state, user: action.payload, loading: false }

        //------------------Reset States While LoggingOut--------------------------------//.
        case RESET_STATE:
            return { ...state, user: null, errorLogin: '', loading: false, registrationDetails: null, errorRegistration: '', }

        default:
            return state;
    }
}