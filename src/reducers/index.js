import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer'
import CommDetailsReducer from "./CommDetailsReducer";

export default combineReducers({
    auth: AuthReducer,
    commDetails: CommDetailsReducer
});
