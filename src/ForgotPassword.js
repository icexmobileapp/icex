import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, UIManager, LayoutAnimation, Keyboard, BackHandler } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Item, Input, Label, Toast, Spinner } from 'native-base';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import Icon from "react-native-vector-icons/FontAwesome";
import Button from './components/Button';
import Loader from './components/Loader';
import common from './common';
import CountryPicker from "./components/CountryPicker";

export default class ForgotPassword extends Component {

    constructor(props) {
        super(props);
        this.state =
            {
                // state = {
                seconds: 60,
                time: {},
                timer: 0,
                // startTimer: this.startTimer.bind(this),
                // countDown: this.countDown.bind(this),
                index: 0,
                loading: false,
                mobileNo: '',
                systemGeneratedOtp: '',
                otp: '',
                password: '',
                cnfpassword: '',
                hidePassword: true,
                hideCnfPassword: true,
                mobileErr: '',
                otpErr: '',
                passwordErr: '',
                cnfPasswordErr: '',
                isCCPickerOpen: false,
                countryCode: '+91', //Default should be India
                countryFlag: '🇮🇳', //Default should be India
            };
    }

    animate() {
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        LayoutAnimation.linear();
    }

    resetErrors() {
        this.setState({
            mobileErr: '',
            otpErr: '',
            passwordErr: '',
            cnfPasswordErr: ''
        });
    }

    resetState() {
        this.setState({
            index: 0,
            loading: false,
            mobileNo: '',
            systemGeneratedOtp: '',
            otp: '',
            password: '',
            cnfpassword: '',
            hidePassword: true,
            hideCnfPassword: true,
            mobileErr: '',
            otpErr: '',
            passwordErr: '',
            cnfPasswordErr: ''
        });
    }

    registerClicked() {
        // console.log(this.state);
        this.resetErrors();
        if (this.state.index == 0) {

            var proceed = true;

            if (this.state.mobileNo.trim() == '') {
                this.setState({
                    mobileErr: 'Please enter mobile number',
                });
                proceed = false;
            } else if (!common.validateMobileNo(this.state.mobileNo.trim(),this.state.countryCode)) {
                this.setState({
                    mobileErr: 'Please enter valid mobile number',
                });
                proceed = false;
            }

            if (proceed) {
                this.sendOtp();
            }

        } else if (this.state.index == 1) {
            var proceed = true;

            if (this.state.otp.trim() == '') {
                this.setState({
                    otpErr: 'Please enter OTP',
                });
                proceed = false;
            } else if (this.state.otp.trim().length < 6) {
                this.setState({
                    otpErr: 'Please enter valid 6 digit OTP',
                });
                proceed = false;
            }
            else if (this.state.otp != this.state.systemGeneratedOtp) {
                this.setState({
                    otpErr: 'Entered OTP is incorrect',
                });
                proceed = false;
            }

            if (proceed) {
                this.animate();
                clearInterval(this.state.timer);
                this.setState({
                    index: 2,
                });
                setTimeout(() => {
                    this.animate();
                    this.setState({
                        index: 3,
                    });
                }, 2000);
            }

        } else if (this.state.index == 3) {
            var proceed = true;

            if (this.state.password.trim() == '') {
                this.setState({
                    passwordErr: 'Please enter password',
                });
                proceed = false;
            } else if (!common.validatePassword(this.state.password.trim())) {
                this.setState({
                    passwordErr: `Password should be between 8 to 20 characters & should contain at least one lowercase, uppercase, numeric digit and special character`,
                });
                proceed = false;
            }

            if (this.state.cnfpassword.trim() == '') {
                this.setState({
                    cnfPasswordErr: 'Please enter password',
                });
                proceed = false;
            } else if (this.state.password.trim() != this.state.cnfpassword.trim() && this.state.cnfpassword.trim() != '') {
                this.setState({
                    cnfPasswordErr: 'Passwords didn\'t match',
                });
                proceed = false;
            }

            if (proceed) {
                const request = new ChangePasswrd();
                request.Mobile = this.state.mobileNo;
                request.Password = this.state.password;
                request.CountryCode = this.state.countryCode;

                console.log(request);

                const serviceRequest = common.encryptData(JSON.stringify(request));
                const serviceBody = { request: serviceRequest };
                this.setState({
                    loading: true,
                });

                common.callService('ChangePasswordUserDetail_v2', serviceBody)
                    .then(changepassword => {
                        console.log(changepassword);
                        if (changepassword.ReturnCode == '1') {
                            Toast.show({
                                text: "Password updated successfully!",
                                type: "success",
                                // buttonText: 'Okay'
                                // duration: 5000,
                            });
                            setTimeout(() => {
                                this.setState({
                                    loading: false,
                                });
                            }, 100);
                            setTimeout(() => {
                                this.resetState();
                                this.props.navigation.state.params.returnData(true);
                                this.props.navigation.goBack();
                            }, 1000);

                        } else {
                            this.setState({
                                loading: false,
                                cnfPasswordErr: changepassword.ReturnMsg,
                            });
                        }

                    })
                    .catch(err => {
                        this.setState({
                            loading: false,
                            cnfPasswordErr: 'Some unexpected error occured'
                        });
                        console.log(err);
                    })
            }
        }
    }

    sendOtp() {
        console.log(this.state.systemGeneratedOtp)
        if (this.state.systemGeneratedOtp == '') {

            const generatedOtp = Math.floor(100000 + Math.random() * 900000);
            this.setState({
                systemGeneratedOtp: generatedOtp,
            });
        }

        setTimeout(() => {

            console.log('Otp :' + this.state.systemGeneratedOtp);

            const request = { 
                Mobile: this.state.mobileNo, 
                OTP: this.state.systemGeneratedOtp,
                CountryCode:this.state.countryCode 
            };
            console.log(request);

            const serviceRequest = common.encryptData(JSON.stringify(request));

            const serviceBody = { request: serviceRequest };
            // this.props.sendOtp(serviceBody);
            this.setState({
                loading: true,
            });
            common.callService('ForgotPasswordUserDetail_v2', serviceBody)
                .then(otpDetails => {
                    console.log(otpDetails);
                    this.setState({
                        loading: false,
                    });
                    if (otpDetails.ReturnCode == '1') {
                        this.startTimer();
                        if (this.state.index == 0) {
                            this.animate();
                            this.setState({
                                index: 1,
                                seconds: 60
                            });

                        }
                    }
                    else {
                        if (this.state.index == 0) {
                            this.setState({
                                loading: false,
                                mobileErr: otpDetails.ReturnMsg
                            });
                        } else if (this.state.index == 1) {
                            this.setState({
                                loading: false,
                                otpErr: otpDetails.ReturnMsg
                            });
                        }

                    }
                })
                .catch(err => {
                    console.log(err);
                    if (this.state.index == 0) {
                        this.setState({
                            loading: false,
                            mobileErr: 'Couldn\'t connect to server'
                        });
                    } else if (this.state.index == 1) {
                        this.setState({
                            loading: false,
                            otpErr: 'Couldn\'t connect to server'
                        });
                    }

                })

        }, 20);
    }

    backClicked() {
        clearInterval(this.state.timer);
        if (this.state.index == 1) {
            this.animate();
            this.setState({
                seconds: 60,
                // time: {},
                timer: 0,
                // startTimer: this.startTimer.bind(this),
                // countDown: this.countDown.bind(this),
                index: 0,
                otp: '',
                otpErr: '',
                systemGeneratedOtp: ''
            });
        }
    }

    resendOtp() {
        // clearInterval(this.state.timer);
        this.setState({
            seconds: 60,
            timer: 0,
            // otp: '',
            // otpErr: '',
            systemGeneratedOtp: ''
        });
        setTimeout(() => {
            this.sendOtp();
        }, 10)

    }

    togglePassword = () => {
        Keyboard.dismiss();
        this.setState({ hidePassword: !this.state.hidePassword });
    }

    toggleCnfPassword = () => {
        Keyboard.dismiss();
        this.setState({ hideCnfPassword: !this.state.hideCnfPassword });
    }

    startTimer() {
        if (this.state.timer == 0 && this.state.seconds > 0) {
            // setTimeout(() => {
            this.state.timer = setInterval(() => {
                this.countDown()
            }, 1000);
            // }, 5);

        }
    }

    countDown() {
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1;
        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });
        // Check if we're at zero.
        if (seconds == 0 || seconds < 0) {
            clearInterval(this.state.timer);
        }
    }

    secondsToTime(secs) {
        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = String(Math.ceil(divisor_for_seconds)).padStart(2, '0')

        let obj = {
            'h': hours,
            'm': minutes,
            's': seconds
        };
        return obj;
    }

    showTimer() {
        if (this.state.seconds == 0) {
            return (
                <TouchableOpacity
                    style={{ alignSelf: 'flex-end', marginRight: scale(20), marginTop: scale(5) }}
                    onPress={() => this.resendOtp()}>
                    <Text
                        style={{ color: 'white', fontSize: moderateScale(12) }}
                    >RESEND OTP</Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={{ alignSelf: 'flex-end', marginRight: scale(20), marginTop: scale(5) }}>
                    <Text style={{ color: 'white', fontSize: moderateScale(12) }}
                    >00:{this.state.time.s} </Text>
                </View>
            )
        }

    }

    renderPage() {
        if (this.state.index == 0) {
            return (
                <View>
                    <View style={{ fontWeight: 'bold', color: 'white', paddingLeft: scale(20), paddingRight: scale(20) }}>

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View
                                style={styles.countryCode}
                            >
                                <TouchableOpacity
                                    style={{ flexDirection: 'row', }}
                                    onPress={() => this.setState({ isCCPickerOpen: true })}
                                >
                                    <Text style={styles.inputText}>{this.state.countryFlag} {this.state.countryCode}</Text>
                                    <Icon name='angle-down'
                                        size={moderateScale(15)}
                                        style={{ color: '#fff', marginLeft: moderateScale(5), paddingRight: moderateScale(5) }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <CountryPicker
                                modalVisible={this.state.isCCPickerOpen}
                                isClosed={() => {
                                    this.setState({ isCCPickerOpen: false })
                                }}
                                countryChanged={(countryCode, countryFlag) => {
                                    this.setState({ countryCode, countryFlag })
                                }}
                            />
                            <View style={{ flex: 0.75 }}>
                                <Item floatingLabel style={styles.placeholderText}>
                                    <Label style={styles.labelStyle}>MOBILE NUMBER</Label>
                                    <Input style={styles.inputText} keyboardType='numeric'
                                        maxLength={this.state.countryCode == '+91' ? 10 : 16}
                                        value={this.state.mobileNo}
                                        onChangeText={(text) => { this.setState({ mobileNo: common.numberOnly(text) }) }}
                                    />
                                </Item>
                            </View>
                        </View>
                        {!this.state.mobileErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.mobileErr}</Text> :
                            <Text style={{ fontSize: moderateScale(13), color: 'white' }}>OTP will be sent on this number</Text>
                        }
                    </View>
                    {this.state.loading ?
                        <View
                            style={{
                                justifyContent: "center",
                                alignItems: 'center',
                                height: moderateScale(100),
                                width: moderateScale(100),
                                marginTop: moderateScale(30),
                            }}>
                            <Spinner color='#97f8cc'
                                size={moderateScale(35)} />
                        </View>
                        :
                        <Button
                            style={{ marginTop: moderateScale(50), marginBottom: moderateScale(20) }}
                            whenPressed={() => this.registerClicked()}>
                            SUBMIT
                        </Button>
                    }
                </View>
            );
        } else if (this.state.index == 1) {
            return (
                <View>
                    {/* <Text style={{color:'white', paddingLeft:scale(20),fontSize: moderateScale(16),marginTop: verticalScale(20)}}>OTP</Text> */}
                    <View style={{ fontWeight: 'bold', color: 'white', paddingLeft: scale(20), paddingRight: scale(20), }}>
                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle} >ENTER OTP</Label>
                            <Input style={styles.inputText} keyboardType='numeric' maxLength={6}
                                value={this.state.otp}
                                onChangeText={(text) => { this.setState({ otp: common.numberOnly(text) }) }}
                            />
                        </Item>
                        {!this.state.otpErr == '' ?
                            <Text style={styles.errorMsg}>{this.state.otpErr}</Text> : null
                        }
                    </View>
                    {/* <View style={{ alignSelf: 'flex-end', marginRight: scale(20), marginTop: scale(5) }}>
                        <Text style={{ color: 'white', fontSize: moderateScale(12) }}
                        > s: {this.state.time.s} </Text>
                    </View>
                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end', marginRight: scale(20), marginTop: scale(5) }}
                        onPress={() => this.resendOtp()}>
                        <Text
                            style={{ color: 'white', fontSize: moderateScale(12) }}
                        >RESEND OTP</Text>
                    </TouchableOpacity> */}
                    {this.showTimer()}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginRight: scale(40), marginTop: moderateScale(50) }}>
                        <Button
                            whenPressed={() => this.registerClicked()}>
                            SUBMIT
                        </Button>
                        <TouchableOpacity
                            style={{ alignSelf: 'center', justifyContent: 'center' }}
                            onPress={() => this.backClicked()}>
                            <Text
                                style={{ color: 'white', fontSize: moderateScale(15) }}
                            >GO BACK</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        } else if (this.state.index == 2) {
            return (
                <View style={{ paddingLeft: scale(20), marginTop: scale(30), paddingRight: scale(20), }}>
                    <Text style={{ fontWeight: '400', color: 'white', fontSize: moderateScale(15) }}>
                        Hi user, the OTP entered is correct.{"\n"}
                        Kindly create your credentials.
                    </Text>
                </View>
            );
        } else if (this.state.index == 3) {
            return (
                <View>
                    <View style={{ fontWeight: 'bold', color: 'white', paddingLeft: scale(20), paddingRight: scale(20), }}>
                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle} >MOBILE NUMBER</Label>
                            <Input style={styles.inputText}
                                editable={false}
                                value={this.state.mobileNo}
                                onChangeText={(text) => { this.setState({ mobileNo: common.numberOnly(text) }) }}
                            />
                        </Item>

                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle}>PASSWORD</Label>
                            <Input style={styles.inputText} secureTextEntry={this.state.hidePassword}
                                maxLength={20}
                                value={this.state.password}
                                onChangeText={(text) => { this.setState({ password: text }) }}
                            />
                        </Item>
                        <TouchableOpacity
                            style={styles.eyeBtn}
                            onPress={this.togglePassword}
                        >
                            {/* <Image
                                source={(this.state.hidePassword) ? require('../assets/imgs/eyeIcon.png') : require('../assets/imgs/eyeIconOpen.png')}
                                style={styles.eyeBtnImage}
                            ></Image> */}
                            <Icon name={(this.state.hidePassword) ? 'eye-slash' : 'eye'}
                                size={moderateScale(15)}
                                style={{ color: 'white' }}
                                light
                            />
                        </TouchableOpacity>
                        {!this.state.passwordErr == '' ?
                            <Text style={[styles.errorMsg, { marginTop: 5 }]}>{this.state.passwordErr}</Text> : null
                        }

                        <Item floatingLabel style={styles.placeholderText}>
                            <Label style={styles.labelStyle}>CONFIRM PASSWORD</Label>
                            <Input style={styles.inputText} secureTextEntry={this.state.hideCnfPassword}
                                maxLength={20}
                                value={this.state.cnfpassword}
                                onChangeText={(text) => { this.setState({ cnfpassword: text }) }}
                            />
                        </Item>
                        <TouchableOpacity
                            style={styles.eyeBtn}
                            onPress={this.toggleCnfPassword}
                        >
                            {/* <Image
                                source={(this.state.hideCnfPassword) ? require('../assets/imgs/eyeIcon.png') : require('../assets/imgs/eyeIconOpen.png')}
                                style={styles.eyeBtnImage}
                            ></Image> */}
                            <Icon name={(this.state.hideCnfPassword) ? 'eye-slash' : 'eye'}
                                size={moderateScale(15)}
                                style={{ color: 'white' }}
                                light
                            />
                        </TouchableOpacity>
                        {!this.state.cnfPasswordErr == '' ?
                            <Text style={[styles.errorMsg, { marginTop: 5 }]}>{this.state.cnfPasswordErr}</Text> : null
                        }
                    </View>
                    {this.state.loading ?
                        <View
                            style={{
                                justifyContent: "center",
                                alignItems: 'center',
                                height: moderateScale(100),
                                width: moderateScale(100),
                                marginTop: moderateScale(30)
                            }}>
                            <Spinner color='#97f8cc'
                                size={moderateScale(35)} />
                        </View>
                        :
                        <Button
                            style={{ marginTop: moderateScale(50) }}
                            whenPressed={() => this.registerClicked()}>
                            SUBMIT
                        </Button>
                    }

                </View>
            );
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <LinearGradient
                    colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
                    start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
                    style={{ flex: 1 }}
                >
                    {/* <Loader loading={this.state.loading} /> */}
                    <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    >
                        <View style={{ flexDirection: 'row', marginTop: moderateScale(35), marginLeft: moderateScale(10) }}>
                            <TouchableOpacity
                                onPress={() => { this.props.navigation.goBack() }}
                                style={{ padding: 5 }}
                            >
                                <Icon name='angle-left'
                                    size={verticalScale(40)}
                                    style={{ color: 'white' }}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={[styles.headerText, { fontSize: moderateScale(25) }]}>Forgot Password</Text>
                        <View>
                            {this.renderPage()}
                        </View>

                    </ScrollView>
                </LinearGradient>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
    },
    headerText: {
        color: 'white',
        paddingLeft: scale(20),
        alignSelf: 'flex-start'
    },
    placeholderText: {
        borderColor: '#91ace1',
        fontSize: moderateScale(13),
        marginTop: verticalScale(20)
    },
    labelStyle: {
        color: '#cdeff5',
        fontSize: moderateScale(14)
    },
    inputText: {
        color: '#fff',
        fontSize: moderateScale(14)
    },
    errorMsg: {
        color: '#910a0a',
        fontSize: moderateScale(13)
    },
    eyeBtn: {
        alignSelf: 'flex-end',
        height: moderateScale(20),
        width: moderateScale(20),
        marginTop: moderateScale(-25)
    },
    countryCode: {
        height: moderateScale(35),
        flex: 0.25,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        borderColor: '#91ace1',
        borderBottomWidth: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    }
});


class ChangePasswrd {
    Mobile;
    CountryCode;
    Password;
}