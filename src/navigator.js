import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import MenuDrawer from './components/MenuDrawer';
// Navigators
import { createDrawerNavigator, createAppContainer, createStackNavigator, createSwitchNavigator } from 'react-navigation';

// StackNavigator screens
import HomePage from './HomePage';
import CommodityDetail from './CommodityDetail';
import EditProfile from './subViews/EditProfile';

//Other pages
import LoginRegister from './LoginRegister';
import ForgotPassword from "./ForgotPassword";
import WatchList from './WatchList';
import AboutUs from './AboutUs';
import Portfolio from './Portfolio';
import Settings from './Settings';
import UserPreference from './UserPreference';

import IcexWebView from './subViews/IcexWebView';
import ZoomChart from "./subViews/ZoomChart";
import PortfolioDetails from "./subViews/PortfolioDetails";

const Width = Dimensions.get('window').width;

// Login & ForgotPassword Page Navigation will be handled here

const LoginStack = createStackNavigator({
  LoginRegister: {
    screen: LoginRegister, navigationOptions: {
      header: null,
    }
  },
  ForgotPassword: {
    screen: ForgotPassword, navigationOptions: {
      header: null,
    }
  },
}, {
    initialRouteName: 'LoginRegister',
  });

// HomePage & CommodityDetail Page Navigation will be handled here

const CommodityStack = createStackNavigator({
  HomePage: {
    screen: HomePage, navigationOptions: {
      header: null,
    }
  },
  UserPreferenceFromHome: {
    screen: UserPreference, navigationOptions: {
      header: null,
    }
  },
  CommodityDetail: {
    screen: CommodityDetail, navigationOptions: {
      header: null,
    }
  },
  EditProfile: {
    screen: EditProfile, navigationOptions: {
      header: null,
    }
  },
  ZoomChart: {
    screen: ZoomChart, navigationOptions: {
      header: null,
    }
  },
}, {
    initialRouteName: 'HomePage',
  });

// Watchlist Page & CommodityDetail Page Navigation will be handled here

const WatchListStack = createStackNavigator({
  WatchList: {
    screen: WatchList, navigationOptions: {
      header: null,
    }
  },
  CommodityDetail: {
    screen: CommodityDetail, navigationOptions: {
      header: null,
    }
  },
}, {
    initialRouteName: 'WatchList',
  });

const PortfolioStack = createStackNavigator({
  Portfolio: {
    screen: Portfolio, navigationOptions: {
      header: null,
    }
  },
  PortfolioDetails: {
    screen: PortfolioDetails, navigationOptions: {
      header: null,
    }
  },
}, {
    initialRouteName: 'Portfolio',
  });

// About Us & ICEX Website navigation Stack

const AboutUsStack = createStackNavigator({
  AboutUs: {
    screen: AboutUs, navigationOptions: {
      header: null,
    }
  },
  IcexWebView: {
    screen: IcexWebView, navigationOptions: {
      header: null,
    }
  },
}, {
    initialRouteName: 'AboutUs',
  });

// Navigation between screens from drawer will be handled here

const DrawerConfig = {
  drawerWidth: Width * 0.75,
  contentComponent: ({ navigation }) => {
    return (<MenuDrawer navigation={navigation} />)
  }
}

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: CommodityStack
    },
    Portfolio: {
      screen: PortfolioStack
    },
    WatchList: {
      screen: WatchListStack
    },
    UserPreference: {
      screen: UserPreference
    },
    AboutUs: {
      screen: AboutUsStack
    },
    Settings: {
      screen: Settings
    },
  }, DrawerConfig

)

export const createRootNavigator = (signedIn = false) => {
  const SwitchNavigator = createSwitchNavigator(
    {
      SignedIn: {
        screen: DrawerNavigator
      },
      SignedOut: {
        screen: LoginStack
      }
    },
    {
      initialRouteName: signedIn ? "SignedIn" : "SignedOut"
    }
  );
  return createAppContainer(SwitchNavigator);
};

// export default createAppContainer(DrawerNavigator);

// import React from 'react';
// import { Platform, Dimensions } from 'react-native';
// import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
// import HomePage from './HomePage'
// import CommodityDetail from './CommodityDetail'
// import MenuDrawer from './components/MenuDrawer';

// import LoginRegister from './LoginRegister'

// const Width = Dimensions.get('window').width;

// const DrawerConfig = {
// 	drawerWidth: Width*0.83,
// 	contentComponent: ({ navigation }) => {
// 		return(<MenuDrawer navigation={navigation} />)
// 	}
// }

// const Stack = createStackNavigator({
//   HomePage: { screen: HomePage },
//   CommodityDetail: { screen: CommodityDetail },
// }, {
//   initialRouteName: 'HomePage',
// })

// const DrawerNavigator = createDrawerNavigator(
//     {
//         Home: {
//             screen: Stack
//         },
//         Link: {
//             screen: LoginRegister
//         }
//     }, DrawerConfig

// )

// export default createAppContainer(DrawerNavigator);