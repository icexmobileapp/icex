import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, Dimensions, UIManager, LayoutAnimation, AsyncStorage, Alert, BackHandler, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { TabView, SceneMap } from 'react-native-tab-view';
import { connect } from 'react-redux';
import LoginView from './subViews/LoginView';
import RegisterView from './subViews/RegisterView';
import Loader from "./components/Loader";
import { CryptoJS } from "./assets/js/aes";
import { loginUser } from "./actions";
import common from "./common";
const DeviceWidth = Dimensions.get('window').width;
const ViewWidth = DeviceWidth - scale(20) - scale(20);

class LoginRegister extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  state = {
    showLoginScreen: false,
    loading: false,
    index: 0,
    routes: [
      { key: 'login', title: 'LOGIN', navigation: this.props.navigation },
      { key: 'register', title: 'REGISTER', navigation: this.props.navigation },
    ],
  };

  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  componentWillUpdate() {
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    LayoutAnimation.spring();
  }

  componentWillMount() {
    this.retrieveData();
    this.retrieveCredential();
  }

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps LoginReg")
    if (this.props.errorLogin == '' && nextProps.errorLogin != '') {
      this.setState({
        showLoginScreen: true,
        loading: false
      })
    } else
      if (this.props.user == null && nextProps.user != null) {

        this.props.navigation.navigate("SignedIn");
      }
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  componentWillUnmount = () => {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.onBackButtonPressAndroid);
    console.log('componentWillUnmount LoginRegister');
  }

  handleExit() {
    BackHandler.exitApp()
  }

  onBackButtonPressAndroid = () => {
    Alert.alert("Exit", "Are you sure you want to exit?", [{ text: "Cancel", onPress: () => { }, style: "cancel" }, { text: "Exit", onPress: () => this.handleExit() }], { cancelable: false });
    return true;
  };

  retrieveCredential = async () => {
    try {
      const value = await AsyncStorage.getItem('loginCredential');

      console.log(value);

      if (value != null && value != undefined && String(value).trim() != '') {
        console.log(value);
        let decrypted = CryptoJS.AES.decrypt(value, 'aNdRgUkXp2s5v8y/');
        // console.log("decrypted", decrypted)
        console.log('loginCredential', decrypted.toString(CryptoJS.enc.Utf8));
        let temp = new obj();
        temp = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
        console.log('temp', temp)
        let req = {};

        Object.assign(req, temp);
        req.Device = '2';
        req.Platform = Platform.OS === 'ios' ? 2 : 1;

        console.log((JSON.stringify(req)));

        let serviceRequest = common.encryptData(JSON.stringify(req))

        let serviceBody = { request: serviceRequest };
        this.props.loginUser(serviceBody);
      } else {
        this.setState({
          showLoginScreen: true
        })
      }
    } catch (error) {
      console.log('error', error);
    }
  }

  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('refreshTime');

      console.log(value);
      if (value == null) {
        AsyncStorage.setItem("refreshTime", "5 SEC");
      }
    } catch (error) {
      AsyncStorage.setItem("refreshTime", "5 SEC");
    }
  };

  getLoader() {
    if (this.props.loading || this.state.loading) {
      return (
        <Loader />
      );
    }
    return null;
  }

  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => {
    // const inputRange = props.navigationState.routes.map((x, i) => i);
    const selectedTabAlign = this.state.index == 0 ? 'flex-start' : 'flex-end';

    // console.log(props);
    // console.log(inputRange)

    return (
      <View style={{ paddingTop: 20, flexDirection: 'column' }}>
        <View style={styles.tabBar}>
          {props.navigationState.routes.map((route, i) => {
            const color = this.state.index == i ? '#ffffff' : '#c9c9c9';
            const align = i == 0 ? 'flex-start' : 'flex-end';

            return (
              <TouchableOpacity
                key={route.key}
                style={styles.tabItem}
                onPress={() => this.setState({ index: i })}>
                <Text style={{ color, fontSize: moderateScale(17), alignSelf: align, paddingLeft: scale(20), paddingRight: scale(20) }}>{route.title}</Text>
                {/* <View style={{height:4,alignSelf:align ,width:ViewWidth/3, backgroundColor:'#ffffff'}}></View> */}
              </TouchableOpacity>

            );
          })}
        </View>
        <View style={{ height: 3, alignSelf: selectedTabAlign, marginLeft: scale(20), marginRight: scale(20), width: ViewWidth / 3, backgroundColor: '#ffffff' }}></View>
        <View style={{ height: 2, marginLeft: scale(20), width: ViewWidth, backgroundColor: '#ffffff' }}></View>
      </View>
    );
  };

  _renderScene = SceneMap({
    login: LoginView,
    register: RegisterView,
  });

  loginScreen() {
    const selectedTab = this.state.index == 0 ? 'Login' : 'Register';
    if (this.state.showLoginScreen) {
      return (

        <ScrollView
          keyboardShouldPersistTaps={'handled'}
        >
          <Text style={styles.welcomeText}>Welcome to ICEX,</Text>
          <Text style={styles.sloganText}>Seize the right opportunity</Text>
          <Text style={styles.loginText}>
            {/* To the world of commodities!{"\n"} */}
            {selectedTab} to begin.
      </Text>

          <TabView
            navigationState={this.state}
            renderScene={this._renderScene}
            renderTabBar={this._renderTabBar}
            onIndexChange={this._handleIndexChange}
            swipeEnabled={false}
          />
        </ScrollView>
      )
    } else {
      return null
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <LinearGradient
          colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
          start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
          style={{ flex: 1 }}
        >
          {this.getLoader()}
          {this.loginScreen()}

        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
    flexDirection: 'column'
  },
  welcomeText: {
    fontSize: moderateScale(40),
    fontWeight: '300',
    color: 'white',
    paddingLeft: scale(20),
    paddingTop: verticalScale(52)
  },
  sloganText: {
    fontSize: moderateScale(24),
    fontWeight: '300',
    paddingBottom: verticalScale(23),
    color: 'white',
    paddingLeft: scale(20)
  },
  loginText: {
    fontSize: moderateScale(16),
    fontWeight: 'bold',
    color: 'white',
    paddingLeft: scale(20)
  }
});

const mapStateToProps = state => {
  const { errorLogin, loading, user } = state.auth
  return { errorLogin, loading, user }
}


export default connect(mapStateToProps, { loginUser })(LoginRegister);
class obj {
  Mobile;
  Password;

  constructor() { }
}
class UserLogin {
  Mobile;
  Password;
  Device;
  Email;
  Platform;

  constructor() { }
}
