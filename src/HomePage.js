import React, { Component } from 'react';
import { Alert, BackHandler, StyleSheet, Text, View, TouchableHighlight, FlatList, Dimensions, UIManager, LayoutAnimation, AsyncStorage } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { withNavigationFocus } from "react-navigation";
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux';
import { getCommodityData, CanCallService, getWatchListArr } from './actions';
import Header from './components/Header';
import common from './common';
import Loader from "./components/Loader";

class HomePage extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this.callServiceInterval;
    this.state =
      {
        loaderPref: false,
        loading: false,
        refreshTime: 5000,
        refreshTimeText: '5 SEC'
      };

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
    if (this.props.user.isNewUser || !this.props.user.IsPreferenceSet) {
      this.setState({
        loaderPref: true,
      });
      setTimeout(() => {
        this.props.navigation.navigate('UserPreferenceFromHome', { fromHomePage: true });
      }, 1000);
      setTimeout(() => {
        this.setState({
          loaderPref: false,
        });
      }, 1200);
    }
  }

  handleExit() {
    BackHandler.exitApp()
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.closeDrawer();
    Alert.alert("Exit", "Are you sure you want to exit?", [{ text: "Cancel", onPress: () => { }, style: "cancel" }, { text: "Exit", onPress: () => this.handleExit() }], { cancelable: false });
    return true;
  };

  componentWillMount = () => {
    this.setRefreshTime();

    this.props.CanCallService(true);
    // console.log(JSON.parse(JSON.stringify(new Date())));

    this.props.getCommodityData();
    this.getWatchListData();
    setTimeout(() => {
      console.log('this.state.refreshTime:' + this.state.refreshTime)
      this.callServiceInterval = setInterval(() => {
        this.getCommodityData()
      }, this.state.refreshTime);
    }, 50);
  }

  componentWillUnmount = () => {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.onBackButtonPressAndroid);
    clearInterval(this.callServiceInterval);
    console.log('componentWillUnmount Homepage');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('componentWillReceiveProps Homepage');

    if (nextProps.isRefreshTimeChanged) {
      AsyncStorage.getItem("refreshTime").then((value) => {

        if (value != null && value != undefined) {

          if (this.state.refreshTimeText != value) {
            this.setState({
              refreshTimeText: value
            });
            this.setRefreshTime();

            setTimeout(() => {
              clearInterval(this.callServiceInterval);
              this.callServiceInterval = setInterval(() => {
                this.getCommodityData()
              }, this.state.refreshTime);
            }, 100);
          }
        }
      }).done();

    } else {
      this.setState({
        loading: false
      });
    }

  }

  componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.props.CanCallService(this.props.isFocused);
    } else if (this.props.isFocused) {
      this.props.CanCallService(this.props.isFocused);
    }
  }

  getWatchListData() {
    const req = new GetWatchlist();
    req.UserId = this.props.user.Id;

    console.log((JSON.stringify(req)));

    var serviceRequest = common.encryptData(JSON.stringify(req))
    this.setState({
      loading: true,

    });
    var serviceBody = { request: serviceRequest };
    console.log(serviceBody);
    common.callService('GetWatchlist', serviceBody)
      .then(GetWatchlist => {
        console.log('GetWatchlist', GetWatchlist);
        if (GetWatchlist.ReturnCode == '1') {
          this.setState({
            loading: false,
          });
          var watchArr = [];
          if (GetWatchlist.Data != null && GetWatchlist.Data.length > 0) {
            for (var i = 0; i < GetWatchlist.Data.length; i++) {
              const obj = {
                CommodityId: GetWatchlist.Data[i].CommodityId,
                ContractCode: GetWatchlist.Data[i].ContractCode
              }
              watchArr.push(obj);
              if (i == GetWatchlist.Data.length - 1) {
                this.props.getWatchListArr(watchArr);
              }
            }
          } else if (GetWatchlist.Data.length == 0) {
            var watchArr = [];
            this.props.getWatchListArr(watchArr);
          }
        } else {
          this.setState({
            loading: false,
          });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({
          loading: false,
        });
      });

  }

  setRefreshTime() {
    AsyncStorage.getItem("refreshTime").then((value) => {
      this.setState({
        refreshTimeText: value
      });
      if (value != null && value != undefined) {
        switch (value) {
          case '5 SEC': this.setState({ refreshTime: 5000 });
            break;
          case '10 SEC': this.setState({ refreshTime: 10000 });
            break;
          case '15 SEC': this.setState({ refreshTime: 15000 });
            break;
          case '20 SEC': this.setState({ refreshTime: 20000 });
            break;
          case '25 SEC': this.setState({ refreshTime: 25000 });
            break;
          case '30 SEC': this.setState({ refreshTime: 30000 });
            break;

          default: this.setState({ refreshTime: 15000 });
            break;
        }
      }
    }).done();
  }

  getCommodityData() {
    if (this.props.canLoadHomeData)
      this.props.getCommodityData();
  }

  canShowLTP(LastTradedTime) {
    let lttDate = new Date(LastTradedTime.substr(0, 10));
    lttDate.setHours(0, 0, 0, 0);
    let currDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    if (lttDate.getTime() !== currDate.getTime()) {
      return false;
    } else {
      return true;
    }
  }

  renderItem(commodity) {
    // console.log(commodity.item);
    const { Underlying, ExpiryDate, LastTradedPrice, LastTradedTime, PreviousClosePrice, NetPriceChangeFromClosingPrice, NetPercentChangeFromClosingPrice } = commodity.item[0];
    let background = '#4d4d4d';

    let showLTPDetail = false;

    if (LastTradedTime != null && LastTradedTime != undefined) {
      showLTPDetail = this.canShowLTP(LastTradedTime);
    }
    if (showLTPDetail) {
      if (NetPriceChangeFromClosingPrice < 0) {
        background = '#de0808';
      } else if (NetPriceChangeFromClosingPrice > 0) {
        background = '#01800f';
      }
    }

    return (
      <TouchableHighlight
        style={[styles.items, { flexDirection: 'row' }]}
        onPress={() => { this.props.navigation.navigate('CommodityDetail', { index: commodity.index, commodityName: Underlying }) }}
      >
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={[styles.headerTabs, { paddingLeft: 10, paddingRight: 8 }]}>
            <Text style={styles.dataText}>{Underlying}</Text>
            <Text style={styles.itemDateText}>{common.expiryDateToDisplay(ExpiryDate)}</Text>
          </View>
          <View style={[styles.headerTabs, styles.priceTab, { borderColor: '#2e2e2e' }]}>
            {showLTPDetail ?
              <Text style={styles.dataText}>{common.addDecimals(LastTradedPrice)}</Text> :
              <Text style={styles.dataText}>{common.addDecimals(PreviousClosePrice)}</Text>
            }

          </View>
          <View style={[styles.headerTabs, styles.changeTab]}>
            <View style={[styles.changeItemContainer, { backgroundColor: background }]}>
              {showLTPDetail ?
                <Text style={styles.changeItemText}>{common.addDecimals(NetPriceChangeFromClosingPrice)}</Text> :
                <Text style={styles.changeItemText}>0.00</Text>
              }
              {showLTPDetail ?
                <Text style={styles.changeItemText}>{common.addDecimals(NetPercentChangeFromClosingPrice)}%</Text> :
                <Text style={styles.changeItemText}>0.00%</Text>
              }
            </View>
          </View>
        </View>
      </TouchableHighlight>

    );
  }

  renderList() {
    if (this.state.loaderPref) {
      return null
    } else if (this.props.errorCommodityData.trim() == '') {
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={[styles.headerTabs, { paddingLeft: 10, paddingRight: 8 }]}>
              <Text style={styles.headerText}>Commodity{"\n"}Name</Text>
            </View>
            <View style={[styles.headerTabs, styles.priceTab]}>
              <Text style={styles.headerText}>LTP/CP</Text>
            </View>
            <View style={[styles.headerTabs, styles.changeTab]}>
              <Text style={styles.headerText}>Change</Text>
            </View>
          </View>

          <FlatList
            data={this.props.commodityData}
            renderItem={(commodity) => this.renderItem(commodity)}
            keyExtractor={(index) => JSON.stringify(index)}
            extraData={this.state}
            removeClippedSubviews={false}
          />
        </View>
      );
    }
    return (
      <View
        style={{
          flex: 1, backgroundColor: '#303132', padding: moderateScale(20),
          alignItems: 'center', justifyContent: 'center'
        }}
      >
        <Icon name='chain-broken'
          size={moderateScale(35)}
          style={{ color: 'white' }}
        />
        <Text style={{ fontSize: moderateScale(20), color: 'white', marginTop: moderateScale(20) }}>
          {this.props.errorCommodityData}
        </Text>
      </View>

    );
  }

  render() {

    return (
      <View style={styles.container}>
        <Header navigation={this.props.navigation} />
        <Loader loading={this.state.loading || this.state.loaderPref} />
        <LinearGradient
          colors={['#303132', '#171717']}
          start={{ x: 0.5, y: 0.5 }} end={{ x: 1.0, y: 1.0 }}
          style={{ flex: 1 }}
        >

          {this.renderList()}

        </LinearGradient>
      </View>
    );
  }

}

const mapStateToProps = state => {
  const { canLoadHomeData, commodityData, errorCommodityData, isRefreshTimeChanged } = state.commDetails;
  const { user, } = state.auth;
  return { canLoadHomeData, commodityData, errorCommodityData, isRefreshTimeChanged, user }
}

export default connect(mapStateToProps, { getCommodityData, CanCallService, getWatchListArr })(withNavigationFocus(HomePage));

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  header: {
    // flex: 1,
    flexDirection: 'row',
    height: moderateScale(45),
    backgroundColor: '#4d4d4d',
    marginLeft: scale(16),
    marginRight: scale(16),
    marginTop: verticalScale(15),
    marginBottom: verticalScale(10),
    borderRadius: moderateScale(5),
    borderWidth: 1,
    borderColor: '#59595a'
  },
  headerTabs: {
    justifyContent: 'center',
    flex: 1
  },
  priceTab: {
    alignItems: 'flex-end',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    paddingRight: scale(10),
    borderColor: '#59595a'
  },
  changeTab: {
    alignItems: 'flex-end',
    paddingRight: scale(10)
  },
  headerText: {
    color: 'white',
    fontSize: moderateScale(14),
    fontWeight: '500'
  },
  dataText: {
    color: 'white',
    fontSize: moderateScale(12),
    fontWeight: '500'
  },

  items: {
    flex: 1,
    flexDirection: 'row',
    height: moderateScale(70),
    backgroundColor: '#171717',
    marginLeft: scale(16),
    marginRight: scale(16),
    marginBottom: moderateScale(8),
    borderRadius: moderateScale(5),
  },
  itemDateText: {
    fontSize: moderateScale(14),
    color: '#9a9a9a'
  },
  changeItemContainer: {
    flex: 1,
    width: '80%',
    marginTop: verticalScale(10),
    marginBottom: verticalScale(10),
    borderRadius: moderateScale(5),
    justifyContent: 'center'
  },
  changeItemText: {
    alignSelf: 'flex-end',
    paddingRight: 10,
    color: 'white',
    fontSize: moderateScale(12),
    fontWeight: '500',
  }
});
class GetWatchlist {
  UserId;
  constructor() {

  }
}
