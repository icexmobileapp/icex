import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableHighlight, ViewPagerAndroid, UIManager, LayoutAnimation } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import { withNavigationFocus } from "react-navigation";
import { CanCallService } from './actions';
import Header from './components/Header';
import Future from '../src/subViews/Future';
import MarketDepth from '../src/subViews/MarketDepth';
import Spot from '../src/subViews/Spot';
import Blog from '../src/subViews/Blog';
import Loader from "./components/Loader";

class CommodityDetail extends Component {

    state = {
        selectedTab: 'FUTURE',
        isBlogCalled: false,
        isSpotCalled: false,
        isMarketDepthCalled: false,
        selectedCommodity: this.props.navigation.getParam('commodityName', this.props.commodityList[0].value),
        selectedCommodityId: this.props.navigation.getParam('index', 0),
        selectedExpiry: this.props.navigation.getParam('selectedExpIndex', null)
    };

    // componentWillUpdate() {

    //     UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    //     LayoutAnimation.easeInEaseOut();

    // }
    // componentDidMount() {
    //     var ws = new WebSocket('ws://192.168.0.61:15000');
    //     ws.onopen = () => {
    //         // connection opened
    //         console.log('onopen');
    //         ws.send('something'); // send a message
    //       };

    //       ws.onmessage = (e) => {
    //         // a message was received
    //         console.log('onmessage');
    //         console.log(e.data);
    //       };

    //       ws.onerror = (e) => {
    //         // an error occurred
    //         console.log('onerror');
    //         console.log(e.message);
    //       };

    //       ws.onclose = (e) => {
    //         // connection closed
    //         console.log('onclose');
    //         console.log(e.code, e.reason);
    //       };
    // }
    componentDidMount() {
        console.log('componentDidMounts CommodityDetail')
        setTimeout(() => {
            this.props.CanCallService(true);
        }, 200);

    }

    componentDidUpdate(prevProps) {

        if (prevProps.isFocused !== this.props.isFocused) {
            console.log('componentDidUpdate CommodityDetail', this.props.isFocused)
            this.props.CanCallService(this.props.isFocused);
        }
    }

    onCommodityChange(commodity) {
        // this.props.commodityList.find((comm, index) => {
        //     var x = String(comm.value) == String(commodity);
        //     if (x) {
        //         this.setState({
        //             selectedCommodityId: index
        //         })
        //     }
        // });
        this.setState({
            selectedCommodityId: this.props.commodityList.findIndex(comm =>
                String(comm.value) == String(commodity))
        })

        this.setState({
            selectedCommodity: commodity
        })
    }

    tabChanged(index) {
        this.viewpager.setPage(index);
        this.pageScrolled(index)
    }

    pageScrolled(index) {
        switch (index) {
            case 0:
                this.setState({
                    selectedTab: 'FUTURE'
                });
                this.props.CanCallService(true);
                break;
            case 1:
                this.setState({
                    selectedTab: 'MARKET'
                });
                this.props.CanCallService(true);
                if (!this.state.isMarketDepthCalled) {
                    this.setState({
                        isMarketDepthCalled: true
                    });
                }
                break;
            case 2:
                this.setState({
                    selectedTab: 'SPOT'
                });
                this.props.CanCallService(false);
                if (!this.state.isSpotCalled) {
                    this.setState({
                        isSpotCalled: true
                    });
                }
                break;
            case 3:
                this.setState({
                    selectedTab: 'BLOG'
                });
                this.props.CanCallService(false);
                if (!this.state.isBlogCalled) {
                    this.setState({
                        isBlogCalled: true
                    });
                }
                break;

            default:
                break;
        }

    }

    // getSectionDetails() {
    //     if (this.state.selectedTab == 'FUTURE') {
    //         return <Future />
    //     }
    //     else if (this.state.selectedTab == 'MARKET') {
    //         return <MarketDepth />
    //     }
    //     else if (this.state.selectedTab == 'SPOT') {
    //         return <Spot />
    //     }
    //     else if (this.state.selectedTab == 'BLOG') {
    //         return <Blog />
    //     }
    // }

    render() {
        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation} backBtn={true} />
                <LinearGradient
                    colors={['#252525', '#171717']}
                    // locations={[0,0.5,0.9]}
                    start={{ x: 0.5, y: 0.5 }} end={{ x: 1.0, y: 1.0 }}
                    style={{ flex: 1, }}
                >

                    {/* <ScrollView
                        keyboardShouldPersistTaps={'handled'}
                    > */}
                    <View style={{ marginLeft: scale(20), marginTop: -20 }}>
                        <Dropdown
                            // ref={this.codeRef}
                            value={this.state.selectedCommodity}
                            textColor={'white'}
                            onChangeText={(commodity) => this.onCommodityChange(commodity)}
                            containerStyle={{ height: moderateScale(80), width: moderateScale(220), }}
                            data={this.props.commodityList}
                            // propsExtractor={({ props }, index) => props}
                            fontSize={moderateScale(23)}
                            itemTextStyle={{ fontSize: moderateScale(17), borderBottomColor: '#202020', borderBottomWidth: 0.5, paddingBottom: 5, paddingTop: -10 }}
                            pickerStyle={{ backgroundColor: '#000000', borderRadius: moderateScale(5) }}
                            itemColor={'#727272'}
                            itemCount={6}
                            itemPadding={5}
                            IconDetails={{ name: 'caret-down', size: moderateScale(30), color: '#6f73ff' }}
                            dropdownPosition={-7.5}
                            dropdownMargins={{ min: 15, max: 15 }}
                        />
                    </View>

                    <View style={[styles.header, { flexDirection: 'row' }]}>
                        <TouchableHighlight
                            style={[styles.headerTabs, this.state.selectedTab == 'FUTURE' ? styles.selectedFutureTab : styles.futureTab]}
                            onPress={() => this.tabChanged(0)}
                        >
                            <View >
                                <Text style={[styles.tabHeaderFont, this.state.selectedTab == 'FUTURE' ? { color: '#ffff' } : { color: '#838383' }]}>FUTURE</Text>
                            </View>

                        </TouchableHighlight>


                        <TouchableHighlight
                            style={[styles.headerTabs, this.state.selectedTab == 'MARKET' ? styles.selectedMarketTab : styles.marketTab]}
                            onPress={() => this.tabChanged(1)}
                        >
                            <View >
                                <Text style={[styles.tabHeaderFont, this.state.selectedTab == 'MARKET' ? { color: '#ffff' } : { color: '#838383' }]}>MARKET DEPTH</Text>
                            </View>
                        </TouchableHighlight>


                        <TouchableHighlight
                            style={[styles.headerTabs, this.state.selectedTab == 'SPOT' ? styles.selectedSpotTab : styles.spotBlogTab]}
                            onPress={() => this.tabChanged(2)}
                        >
                            <View >
                                <Text style={[styles.tabHeaderFont, this.state.selectedTab == 'SPOT' ? { color: '#ffff' } : { color: '#838383' }]}>SPOT</Text>
                            </View>
                        </TouchableHighlight>


                        <TouchableHighlight
                            style={[styles.headerTabs, { borderTopRightRadius: moderateScale(5), borderBottomRightRadius: moderateScale(5) }, this.state.selectedTab == 'BLOG' ? styles.selectedBlogTab : styles.spotBlogTab]}
                            onPress={() => this.tabChanged(3)}
                        >
                            <View >
                                <Text style={[styles.tabHeaderFont, this.state.selectedTab == 'BLOG' ? { color: '#ffff' } : { color: '#838383' }]}>BLOG</Text>
                            </View>
                        </TouchableHighlight>
                    </View>

                    <ViewPagerAndroid
                        style={{ flex: 1 }}
                        initialPage={0}
                        ref={(viewpager) => { this.viewpager = viewpager }}
                        onPageSelected={(event) => this.pageScrolled(event.nativeEvent.position)}>
                        <View key="1">
                            <Future
                                navigation={this.props.navigation}
                                selectedTab={this.state.selectedTab}
                                selectedCommodity={this.state.selectedCommodity}
                                selectedCommodityId={this.state.selectedCommodityId}
                                selectedExpiry={this.state.selectedExpiry}
                            />
                        </View>
                        <View key="2">
                            <MarketDepth
                                isCalled={this.state.isMarketDepthCalled}
                                selectedCommodity={this.state.selectedCommodity}
                                selectedCommodityId={this.state.selectedCommodityId}
                            />
                        </View>
                        <View key="3">
                            <Spot
                                isCalled={this.state.isSpotCalled}
                                selectedCommodity={this.state.selectedCommodity}
                                selectedTab={this.state.selectedTab}
                            />
                        </View>
                        <View key="4">
                            <Blog
                                selectedCommodity={this.state.selectedCommodity}
                                isCalled={this.state.isBlogCalled}
                                selectedTab={this.state.selectedTab}
                            />
                        </View>
                    </ViewPagerAndroid>

                    {/* {this.getSectionDetails()} */}

                    {/* </ScrollView> */}

                </LinearGradient>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    header: {
        // flex: 1,
        flexDirection: 'row',
        height: verticalScale(45),

        marginLeft: scale(15),
        marginRight: scale(15),
        // marginTop: verticalScale(15),
        marginBottom: verticalScale(25),
        borderRadius: moderateScale(5),
        borderWidth: 1,
        borderColor: '#59595a'
    },
    headerTabs: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    tabHeaderFont: {
        fontSize: moderateScale(13),
    },
    futureTab: {
        flex: 1,
        backgroundColor: '#252525',
        borderTopLeftRadius: moderateScale(5),
        borderBottomLeftRadius: moderateScale(5),
    },
    selectedFutureTab: {
        flex: 1,
        backgroundColor: '#000',
        borderTopLeftRadius: moderateScale(5),
        borderBottomLeftRadius: moderateScale(5),
        borderColor: '#4a4eda',
        borderBottomWidth: verticalScale(2)
    },
    marketTab: {
        borderColor: '#59595a',
        backgroundColor: '#252525',
        borderLeftWidth: 1,
        flex: 1.7
    },
    selectedMarketTab: {
        borderBottomColor: '#4a4eda',
        borderBottomWidth: verticalScale(2),
        backgroundColor: '#000',
        borderLeftWidth: 1,
        borderLeftColor: '#59595a',
        flex: 1.7
    },
    spotBlogTab: {
        borderColor: '#59595a',
        backgroundColor: '#252525',
        borderLeftWidth: 1,
        flex: 0.9
    },
    selectedSpotTab: {
        backgroundColor: '#000',
        borderBottomColor: '#4a4eda',
        borderBottomWidth: verticalScale(2),
        borderLeftColor: '#59595a',
        borderLeftWidth: 1,
        flex: 0.9
    },
    selectedBlogTab: {
        backgroundColor: '#000',
        borderBottomColor: '#4a4eda',
        borderBottomWidth: verticalScale(2),
        borderLeftColor: '#59595a',
        borderLeftWidth: 1,
        flex: 0.9,
        borderTopRightRadius: moderateScale(5),
        borderBottomRightRadius: moderateScale(5),
    }

});

const mapStateToProps = state => {
    const { loading, commodityList } = state.commDetails;
    return { loading, commodityList }
}

export default connect(mapStateToProps, { CanCallService })(withNavigationFocus(CommodityDetail));