/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { StatusBar, StyleSheet, View, Linking, Dimensions, Text, TouchableOpacity, Platform } from 'react-native';
import GlobalFont from 'react-native-global-font';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import LinearGradient from 'react-native-linear-gradient';
import Overlay from 'react-native-modal-overlay';
import ReduxThunk from "redux-thunk";
import { Root } from "native-base";
import SplashScreen from "react-native-splash-screen";
import Hyperlink from 'react-native-hyperlink';
import { createRootNavigator } from './src/navigator';
import reducers from './src/reducers';
import OfflineNotice from "./src/components/OfflineNotice";
import common from './src/common';

const linkify = require('linkify-it')();

const { height, width } = Dimensions.get('window');

class App extends Component {

  state = {
    signedIn: false,
    isChecked: false,
    showUpdateModal: false,
    forceUpdate: false,
    messageToDisplay: '',
  };

  MobileVersionDetails = {};

  componentWillMount() {
    this.callCheckMobileVersion();
  }

  componentDidMount() {
    let fontName = 'Lato';
    GlobalFont.applyGlobal(fontName);
    setTimeout(() => {
      SplashScreen.hide();
    }, 2000)
  }

  callCheckMobileVersion() {

    let Id = Platform.OS === 'ios' ? 2 : 1;
    let req = { Id };
    console.log('req:', req);

    const serviceBody = { request: common.encryptData(JSON.stringify(req)) };
    common.callService('MobileVersion_v2', serviceBody)
      .then(MobileVersion => {
        SplashScreen.hide();
        if (MobileVersion.ReturnCode == '1') {
          this.MobileVersionDetails = MobileVersion.Data;
          console.log(this.MobileVersionDetails);

          if ('MessageOnAppStart' in this.MobileVersionDetails) {
            if (this.MobileVersionDetails.MessageOnAppStart.trim() != '' && this.MobileVersionDetails.MessageOnAppStart != null) {
              this.setState({
                showUpdateModal: true,
                messageToDisplay: this.MobileVersionDetails.MessageOnAppStart,
              });
            } else {
              this.checkVersion();
            }
          } else {
            this.checkVersion();
          }
        }
      })
      .catch(err => {
        SplashScreen.hide();
        this.setState({
          isChecked: true
        });
        console.log(err);
      })
  }

  checkVersion() {
    if (Number(this.MobileVersionDetails.AppVersion) > Number(common.appVersion)) {
      if (this.MobileVersionDetails.ForceUpdate || this.MobileVersionDetails.ForceUpdate == 'true') {
        this.setState({
          showUpdateModal: true,
          forceUpdate: true,
        });
      } else {
        this.setState({
          showUpdateModal: true,
          forceUpdate: false,
        });
      }
    } else {
      this.setState({
        isChecked: true
      });
    }
  }

  renderUpdateBox() {
    return (
      <>
        <View style={{ marginTop: moderateScale(20) }}>
          <Text style={{ fontSize: moderateScale(16), color: 'black', alignContent: 'flex-start' }}>Update App</Text>
        </View>

        <View style={{ marginTop: moderateScale(20) }}>
          <Text
            style={{ fontSize: moderateScale(13), color: 'black', alignContent: 'flex-start' }}
          > New version of app is availaible</Text>
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: moderateScale(15) }}>

          <TouchableOpacity
            style={[styles.buttonStyle, { borderWidth: 1, borderColor: '#4a4eda', borderRadius: moderateScale(5), marginLeft: moderateScale(15) }]}
            onPress={() => Linking.openURL("market://details?id=com.icex")}
          >
            <Text style={{ fontSize: moderateScale(14), color: 'black' }}>Install</Text>
          </TouchableOpacity>

          {this.state.forceUpdate ? null :
            <TouchableOpacity
              style={[styles.buttonStyle, { borderWidth: 1, borderColor: '#4a4eda', borderRadius: moderateScale(5), marginLeft: moderateScale(15) }]}
              onPress={() => {
                this.setState({ showUpdateModal: false, isChecked: true });
              }}
            >
              <Text style={{ fontSize: moderateScale(14), color: 'black' }}> Cancel</Text>
            </TouchableOpacity>
          }

        </View>
      </>
    );
  }

  renderMessageBox() {
    return (
      <>
        <View style={{ marginTop: moderateScale(20) }}>
          <Text style={{ fontSize: moderateScale(16), color: 'black', alignContent: 'flex-start' }}>Announcement</Text>
        </View>

        <View style={{ marginTop: moderateScale(20) }}>
          <Hyperlink
            linkDefault={true}
            linkify={linkify}
            linkStyle={{ color: '#4a4eda', textDecorationLine: 'underline' }}
          >
            <Text
              style={{ fontSize: moderateScale(13), color: 'black', alignContent: 'flex-start' }}
            >{this.state.messageToDisplay}</Text>
          </Hyperlink>
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: moderateScale(15) }}>
          <TouchableOpacity
            style={[styles.buttonStyle, { borderWidth: 1, borderColor: '#4a4eda', borderRadius: moderateScale(5), marginLeft: moderateScale(15) }]}
            onPress={() => {
              this.setState({ messageToDisplay: '', showUpdateModal: false });
              this.checkVersion();
            }}
          >
            <Text style={{ fontSize: moderateScale(14), color: 'black' }}>Proceed</Text>
          </TouchableOpacity>
        </View>
      </>
    );
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    const { signedIn } = this.state;

    if (!this.state.isChecked) {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor="black" barStyle="light-content" />
          <LinearGradient
            colors={['#97f8cc', '#5e7ad6', '#4a4eda']}
            start={{ x: 0.1, y: 0.1 }} end={{ x: 0.8, y: 0.8 }}
            style={{ flex: 1 }}
          >
            <Overlay
              visible={this.state.showUpdateModal}
              closeOnTouchOutside={false}
              containerStyle={{
                backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', paddingRight: 0, paddingTop: 0
              }}
              childrenWrapperStyle={{
                alignSelf: 'center',
                backgroundColor: '#fff',
                justifyContent: 'center',
                width: width > 600 ? 600 : width - scale(40),
                borderRadius: moderateScale(5),
                height: moderateScale(150),
                marginLeft: moderateScale(-20)
              }}
            >
              <View style={{ alignItems: 'center', justifyContent: 'space-between', marginBottom: moderateScale(15) }}>
                {this.state.messageToDisplay == '' ?
                  this.renderUpdateBox()
                  : this.renderMessageBox()
                }
              </View>
            </Overlay>
          </LinearGradient>
        </View>
      );
    }
    const Layout = createRootNavigator(signedIn);
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar backgroundColor="black" barStyle="light-content" />
          <OfflineNotice />
          <Layout />

        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: 'center',
    width: moderateScale(100),
    height: moderateScale(30),
    marginTop: moderateScale(20)
  },
});

export default () =>
  <Root>
    <App />
  </Root>;

